package com.djsisko.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nostra13.universalimageloader.core.assist.ImageSize;

public class ImageAsset {

    @SerializedName("display_type")
    @Expose
    private String displayType;
    @SerializedName("image_type")
    @Expose
    private String imageType;
    @SerializedName("image_size")
    @Expose
    private ImageSize imageSize;

    public String getDisplayType() {
        return displayType;
    }

    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public ImageSize getImageSize() {
        return imageSize;
    }

    public void setImageSize(ImageSize imageSize) {
        this.imageSize = imageSize;
    }
}
