package com.djsisko.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;

public class Song {
    public float audioProgress = 0.0f;
    public int audioProgressSec = 0;
    public String commentUser;
    public String comment;
    String songTitle, songArtist, songDuration, I, songUrl;
    int songId, likeImage, shareImage, commentsImage, optionsImage, smallSongGif, songLikeNo;
    int image, image1, totalCount;
    int totalLikes;
    String date;
    int nowPlaying = 0;
    int favStatus, playListStatus, playlistId, favId;
    String videoLink, socialLink;

    //event
    String eventTitle, eventDescription, eventDate, eventTime;
    String eventLong, eventLat;
    String banner_image;
    String strImage = "http:\\/\\/durisimomobileapps.net\\/djsisko\\/core\\/public\\/storage\\/uploads\\/img3_1513072333.png";
//    name, video_link, image, banner_image, description

    public Song(String songTitle, String videoLink, String I, String banner_image, String eventDescription) {
        this.songTitle = songTitle;
        this.videoLink = videoLink;
        this.I = I;
        this.banner_image = banner_image;
        this.eventDescription = eventDescription;
    }

    public Song(int songId, String I, String songTitle, String songArtist, String songUrl, int likeImage, int songLikeNo, int shareImage, String songDuration, int commentsImage, int optionsImage, int smallSongGif) {
        this.songId = songId;
        this.I = I;
        this.songTitle = songTitle;
        this.songArtist = songArtist;
        this.songUrl = songUrl;
        this.likeImage = likeImage;
        this.songLikeNo = songLikeNo;
        this.shareImage = shareImage;
        this.songDuration = songDuration;
        this.commentsImage = commentsImage;
        this.optionsImage = optionsImage;
        this.smallSongGif = smallSongGif;
    }

    public Song(int songId, String I, String songTitle, String songArtist, String songUrl, String songDurtaion, int songLikeNo) {
        this.songId = songId;
        this.I = I;
        this.songTitle = songTitle;
        this.songArtist = songArtist;
        this.songUrl = songUrl;
        this.songDuration = songDurtaion;
        this.songLikeNo = songLikeNo;
    }

    public Song(int songId, String songTitle, String I, String songUrl) {
        this.songId = songId;
        this.songTitle = songTitle;
        this.I = I;
        this.songUrl = songUrl;
    }

    public Song(int songId, String I, String songTitle, String songArtist, String songUrl, String songDurtaion, int songLikeNo, int totalLikes, int playListStatus, int favStatus, int playlistId, int favId, int nowPlaying) {
        this.songId = songId;
        this.I = I;
        this.songTitle = songTitle;
        this.songArtist = songArtist;
        this.songUrl = songUrl;
        this.songDuration = songDurtaion;
        this.songLikeNo = songLikeNo;
        this.playListStatus = playListStatus;
        this.favStatus = favStatus;
        this.playlistId = playlistId;
        this.favId = favId;
        this.nowPlaying = nowPlaying;
        this.totalLikes = totalLikes;
    }

    public Song(int songId, String I, String songTitle, String songArtist, String songUrl, String songDurtaion, int songLikeNo, int playListStatus, int favStatus, int playlistId, int favId) {
        this.songId = songId;
        this.I = I;
        this.songTitle = songTitle;
        this.songArtist = songArtist;
        this.songUrl = songUrl;
        this.songDuration = songDurtaion;
        this.songLikeNo = songLikeNo;
        this.playListStatus = playListStatus;
        this.favStatus = favStatus;
        this.playlistId = playlistId;
        this.favId = favId;
    }

    public Song() {

    }

    public Song(String songTitle) {
        this.songTitle = songTitle;
    }

    public Song(int songId, String songTitle, int totalCount) {
        this.songId = songId;
        this.songTitle = songTitle;
        this.totalCount = totalCount;
    }

    public Song(int songId, String songTitle) {
        this.songId = songId;
        this.songTitle = songTitle;
    }

    public Song(int id, int image, String artist) {
        this.songId = id;
        this.image = image;
        this.songArtist = artist;
    }

    //comment
    public Song(String commentUser, String comment) {
        this.commentUser = commentUser;
        this.comment = comment;
    }

    //video
    public Song(int songId, String I, String songTitle, String date, String videoLink) {
        this.songId = songId;
        this.I = I;
        this.songTitle = songTitle;
        this.date = date;
        this.videoLink = videoLink;
    }

    //event
    public Song(int songId, String I, String eventTitle, String eventDescription, String eventDate, String eventTime, String eventLong, String eventLat) {
        this.songId = songId;
        this.I = I;
        this.eventTitle = eventTitle;
        this.eventDescription = eventDescription;
        this.eventDate = eventDate;
        this.eventTime = eventTime;
        this.eventLong = eventLong;
        this.eventLat = eventLat;
    }

    //    id,image, title, description, start, end, time, address,longitude,latitude
    //event2
    public Song(int songId, String eventTitle, String eventDescription, String eventDate, String eventTime, String eventLong, String eventLat) {
        this.songId = songId;
        this.I = I;
        this.eventTitle = eventTitle;
        this.eventDescription = eventDescription;
        this.eventDate = eventDate;
        this.eventTime = eventTime;
        this.eventLong = eventLong;
        this.eventLat = eventLat;
    }

    //notification
    public Song(int songId, String I, String songTitle) {
        this.songId = songId;
        this.I = I;
        this.songTitle = songTitle;
    }


    //socialMedia
    public Song(int songId, int image, int image1, String songTitle, String socialLink) {
        this.songId = songId;
        this.image = image;
        this.image1 = image1;
        this.songTitle = songTitle;
        this.socialLink = socialLink;
    }

    public String getBanner_image() {
        return banner_image;
    }

    public int getTotalLikes() {
        return totalLikes;
    }

    public int getNowPlaying() {
        return nowPlaying;
    }

    public void setNowPlaying(int nowPlaying) {
        this.nowPlaying = nowPlaying;
    }

    public int getImage1() {
        return image1;
    }

    public int getFavId() {
        return favId;
    }

    public int getPlaylistId() {
        return playlistId;
    }

    public int getFavStatus() {
        return favStatus;
    }

    public int getPlayListStatus() {
        return playListStatus;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public String getComment() {
        return comment;
    }

    public String getCommentUser() {
        return commentUser;
    }

    public String getDate() {
        return date;
    }

    public String getEventDate() {
        return eventDate;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public String getEventTime() {
        return eventTime;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public String getEventLat() {
        return eventLat;
    }

    public String getEventLong() {
        return eventLong;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public int getImage() {
        return image;
    }

    public String getSongUrl() {
        return songUrl;
    }

    public int getSongId() {
        return songId;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public int getCommentsImage() {
        return commentsImage;
    }

    public int getLikeImage() {
        return likeImage;
    }

    public int getOptionsImage() {
        return optionsImage;
    }

    public String getSongArtist() {
        return songArtist;
    }

    public int getShareImage() {
        return shareImage;
    }

    public String getSongDuration() {
        return songDuration;
    }

    public int getSmallSongGif() {
        return smallSongGif;
    }

    public String getI() {
        return I;
    }

    public int getSongLikeNo() {
        return songLikeNo;
    }

    public String getSocialLink() {
        return socialLink;
    }

    public Bitmap getSmallCover(Context context) {

        // ImageLoader.getInstance().getDiskCache().g
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        Bitmap curThumb = null;
        try {
            Uri uri = Uri.parse("content://media/external/audio/media/" + getSongId() + "/albumart");
            ParcelFileDescriptor pfd = context.getContentResolver().openFileDescriptor(uri, "r");
            if (pfd != null) {
                FileDescriptor fd = pfd.getFileDescriptor();
                curThumb = BitmapFactory.decodeFileDescriptor(fd);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return curThumb;
    }

}
