package com.djsisko.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MessageBoardData {


    @SerializedName("message_board_id")
    @Expose
    private Integer messageBoardId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("user_commnet")
    @Expose
    private String userCommnet;
    @SerializedName("vidoe_id")
    @Expose
    private Integer vidoeId;
    @SerializedName("video_name")
    @Expose
    private String videoName;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("admin_comment")
    @Expose
    private ArrayList<AdminComment> adminComment = null;

    public Integer getMessageBoardId() {
        return messageBoardId;
    }

    public void setMessageBoardId(Integer messageBoardId) {
        this.messageBoardId = messageBoardId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserCommnet() {
        return userCommnet;
    }

    public void setUserCommnet(String userCommnet) {
        this.userCommnet = userCommnet;
    }

    public Integer getVidoeId() {
        return vidoeId;
    }

    public void setVidoeId(Integer vidoeId) {
        this.vidoeId = vidoeId;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public ArrayList<AdminComment> getAdminComment() {
        return adminComment;
    }

    public void setAdminComment(ArrayList<AdminComment> adminComment) {
        this.adminComment = adminComment;
    }
}
