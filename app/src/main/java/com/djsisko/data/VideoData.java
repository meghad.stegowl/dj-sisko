package com.djsisko.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoData {

    @SerializedName("embedded_video_id")
    @Expose
    private String embeddedVideoId;
    @SerializedName("embedded_video_name")
    @Expose
    private String embeddedVideoName;
    @SerializedName("embedded_video_link")
    @Expose
    private String embeddedVideoLink;
    @SerializedName("like_status")
    @Expose
    private String likeStatus;
    @SerializedName("like_count")
    @Expose
    private Integer likeCount;
    @SerializedName("dislike_count")
    @Expose
    private Integer dislikeCount;

    public String  getEmbeddedVideoId() {
        return embeddedVideoId;
    }

    public void setEmbeddedVideoId(String embeddedVideoId) {
        this.embeddedVideoId = embeddedVideoId;
    }

    public String getEmbeddedVideoName() {
        return embeddedVideoName;
    }

    public void setEmbeddedVideoName(String embeddedVideoName) {
        this.embeddedVideoName = embeddedVideoName;
    }

    public String getEmbeddedVideoLink() {
        return embeddedVideoLink;
    }

    public void setEmbeddedVideoLink(String embeddedVideoLink) {
        this.embeddedVideoLink = embeddedVideoLink;
    }

    public String getLikeStatus() {
        return likeStatus;
    }

    public void setLikeStatus(String likeStatus) {
        this.likeStatus = likeStatus;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getDislikeCount() {
        return dislikeCount;
    }

    public void setDislikeCount(Integer dislikeCount) {
        this.dislikeCount = dislikeCount;
    }
}
