package com.djsisko.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetProfileData {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("image_assets")
    @Expose
    private List<ImageAsset> imageAssets = null;
    @SerializedName("data")
    @Expose
    private List<ImageAssetDatra> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ImageAsset> getImageAssets() {
        return imageAssets;
    }

    public void setImageAssets(List<ImageAsset> imageAssets) {
        this.imageAssets = imageAssets;
    }

    public List<ImageAssetDatra> getData() {
        return data;
    }

    public void setData(List<ImageAssetDatra> data) {
        this.data = data;
    }
}
