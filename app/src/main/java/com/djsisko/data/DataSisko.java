package com.djsisko.data;

public class DataSisko {
    int i, i1,i2;
    String s, s1;
    int id;
    String Image, Title;

    public DataSisko(int i, String s) {
        this.i = i;
        this.s = s;
    }

    public DataSisko(int i, String s,int i2) {
        this.i = i;
        this.s = s;
        this.i2 = i2;
    }

    public DataSisko(int id, String image, String title) {
        this.id = id;
        this.Image = image;
        this.Title = title;
    }


    public int getId() {
        return id;
    }

    public String getTitle() {
        return Title;
    }

    public String getImage() {
        return Image;
    }

    public int getI() {
        return i;
    }

    public String getS() {
        return s;
    }

    public int getI1() {
        return i1;
    }

    public String getS1() {
        return s1;
    }
}
