package com.djsisko.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdminComment {

    @SerializedName("admin_comment_id")
    @Expose
    private Integer adminCommentId;
    @SerializedName("message_board_id")
    @Expose
    private Integer messageBoardId;
    @SerializedName("admin_commnet")
    @Expose
    private String adminCommnet;
    @SerializedName("time")
    @Expose
    private String time;

    public Integer getAdminCommentId() {
        return adminCommentId;
    }

    public void setAdminCommentId(Integer adminCommentId) {
        this.adminCommentId = adminCommentId;
    }

    public Integer getMessageBoardId() {
        return messageBoardId;
    }

    public void setMessageBoardId(Integer messageBoardId) {
        this.messageBoardId = messageBoardId;
    }

    public String getAdminCommnet() {
        return adminCommnet;
    }

    public void setAdminCommnet(String adminCommnet) {
        this.adminCommnet = adminCommnet;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
