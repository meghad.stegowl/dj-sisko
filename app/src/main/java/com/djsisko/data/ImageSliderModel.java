package com.djsisko.data;

public class ImageSliderModel {
    String sponsor_slide_name;
    String sponsor_slider_image;
    String sponsor_slider_link;


    public ImageSliderModel(String sponsor_slide_name, String sponsor_slider_image, String sponsor_slider_link) {
        this.sponsor_slide_name=sponsor_slide_name;
        this.sponsor_slider_image=sponsor_slider_image;
        this.sponsor_slider_link=sponsor_slider_link;
    }

    public String getSponsor_slide_name() {
        return sponsor_slide_name;
    }

    public void setSponsor_slide_name(String sponsor_slide_name) {
        this.sponsor_slide_name = sponsor_slide_name;
    }

    public String getSponsor_slider_image() {
        return sponsor_slider_image;
    }

    public void setSponsor_slider_image(String sponsor_slider_image) {
        this.sponsor_slider_image = sponsor_slider_image;
    }

    public String getSponsor_slider_link() {
        return sponsor_slider_link;
    }

    public void setSponsor_slider_link(String sponsor_slider_link) {
        this.sponsor_slider_link = sponsor_slider_link;
    }


}

