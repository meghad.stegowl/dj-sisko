package com.djsisko.adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.djsisko.R;
import com.djsisko.data.Song;
import java.util.ArrayList;

public class MySliderAdapter extends PagerAdapter {
    private ArrayList<Song> images;
    private LayoutInflater inflater;
    private Context context;

    public MySliderAdapter(Context context, ArrayList<Song> images) {
        this.context = context;
        this.images=images;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View v = LayoutInflater.from(context).inflate(R.layout.slide,view, false);
        ImageView myImage = (ImageView) v.findViewById(R.id.image);
        Song currentData = images.get(position);
        Glide.with(context)
                .load(currentData.getSongTitle())
                .placeholder(R.drawable.big1logo)
                .error(R.drawable.big1logo)
                .into(myImage);
        view.addView(v, 0);
        return v;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}
