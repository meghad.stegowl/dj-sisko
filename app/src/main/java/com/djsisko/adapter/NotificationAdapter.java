package com.djsisko.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.djsisko.R;
import com.djsisko.data.Song;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationHolder> {
    Context context;
    ArrayList<Song> data = new ArrayList<>();

    public NotificationAdapter(Context context, ArrayList<Song> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public NotificationAdapter.NotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_list_item, parent, false);
        return new NotificationAdapter.NotificationHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.NotificationHolder holder, int position) {
        Song currentData = data.get(position);
        int id = currentData.getSongId();
        String name = currentData.getI();
        String date = currentData.getSongTitle();
        holder.name.setText(currentData.getI());
        holder.date.setText(currentData.getSongTitle());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class NotificationHolder extends RecyclerView.ViewHolder {
        TextView name, date;
        LinearLayout notification_list;

        public NotificationHolder(View v) {
            super(v);
            name = v.findViewById(R.id.name);
            date = v.findViewById(R.id.date);
            notification_list = v.findViewById(R.id.notification_list);
        }
    }
}
