package com.djsisko.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.djsisko.R;
import com.djsisko.data.AdminComment;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterAdminComment extends RecyclerView.Adapter<AdapterAdminComment.DjDataHolder> {

    Context context;
    ArrayList<AdminComment> embeddedVideoData = new ArrayList<>();

    public AdapterAdminComment(Context context, ArrayList<AdminComment> embeddedVideoData) {
        this.context = context;
        this.embeddedVideoData = embeddedVideoData;
    }

    @Override
    public AdapterAdminComment.DjDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_admin_comment, parent, false);
        return new AdapterAdminComment.DjDataHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterAdminComment.DjDataHolder holder, int position) {
        holder.tv_title.setText(embeddedVideoData.get(position).getAdminCommnet());
        holder.tv_time.setText(embeddedVideoData.get(position).getTime());
    }

    @Override
    public int getItemCount() {
        return embeddedVideoData.size();
    }

    public class DjDataHolder extends RecyclerView.ViewHolder {
        CircleImageView iv_image;
        TextView tv_title, tv_time;
        RecyclerView rv_admin_comment;

        public DjDataHolder(View v) {
            super(v);
            iv_image = v.findViewById(R.id.iv_image);
            tv_title = v.findViewById(R.id.tv_title);
            tv_time = v.findViewById(R.id.tv_time);
            rv_admin_comment = v.findViewById(R.id.rv_admin_comment);
        }
    }
}

