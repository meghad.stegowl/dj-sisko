package com.djsisko.adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.djsisko.R;

import java.util.ArrayList;

/**
 * Created by sachin on 11-12-2017.
 */

public class MyAdapter extends PagerAdapter {
    private ArrayList<Integer> images;
    private LayoutInflater inflater;
    private Context context;

    public MyAdapter(Context context, ArrayList<Integer> images) {
        this.context = context;
        this.images=images;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
//        View myImageLayout = inflater.inflate(R.layout.slide, view, false);
        View v = LayoutInflater.from(context).inflate(R.layout.slide,view, false);
        ImageView myImage = (ImageView) v.findViewById(R.id.image);
//        Song currentData = images.get(position);
        myImage.setImageResource(images.get(position));
//        Picasso.with(context).load(currentData.getSongTitle()).into(myImage);
        view.addView(v, 0);
        return v;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}
