package com.djsisko.adapter;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.djsisko.fragment.ImageFragment;
import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.data.Song;
import java.util.ArrayList;

public class ImageGalleryDataAdapter extends RecyclerView.Adapter<ImageGalleryDataAdapter.ImageGalleryDataHolder> {

    Context context;
    ArrayList<Song> dataModules = new ArrayList<>();
    LinearLayout customSlider;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    String tag;

    public ImageGalleryDataAdapter(Context context, ArrayList<Song> dataModules,String tag) {
        this.context = context;
        this.dataModules = dataModules;
        this.tag = tag;
    }

    public class ImageGalleryDataHolder extends RecyclerView.ViewHolder {
        ImageView logo;
        TextView text;
        RelativeLayout ImageListItem;

        public ImageGalleryDataHolder(View v) {
            super(v);
            logo = (ImageView) v.findViewById(R.id.logo);
            text = (TextView) v.findViewById(R.id.name);
            text.setSelected(true);
            ImageListItem = (RelativeLayout)v.findViewById(R.id.imageListItem);
        }
    }

    @Override
    public ImageGalleryDataAdapter.ImageGalleryDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.image_gallery_list_item, parent, false);
        return new ImageGalleryDataAdapter.ImageGalleryDataHolder(v);
    }

    @Override
    public void onBindViewHolder(ImageGalleryDataAdapter.ImageGalleryDataHolder holder, int position) {
        try {
            final Song currentData = dataModules.get(position);
            final int id = currentData.getSongId();
            String image = currentData.getI();
            final String name = currentData.getSongTitle();
            Glide.with(context)
                    .load(currentData.getI())
                    .placeholder(R.drawable.big1logo)
                    .error(R.drawable.big1logo)
                    .into(holder.logo);
            holder.text.setText(name);

            holder.ImageListItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Toast.makeText(context, currentData.getSongTitle(), Toast.LENGTH_SHORT).show();
                    frag = new ImageFragment(context,id,name,tag);
                    fm = ((MainActivity) context).getSupportFragmentManager();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.container, frag);
                    ft.addToBackStack(null);
                    ft.commit();

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        try {
            return dataModules.size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


}