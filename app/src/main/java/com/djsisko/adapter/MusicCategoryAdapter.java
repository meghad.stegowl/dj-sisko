package com.djsisko.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.djsisko.activities.MainActivity;
import com.djsisko.fragment.MusicCategoryInner;
import com.djsisko.R;
import com.djsisko.data.DataSisko;
import java.util.ArrayList;

/**
 * Created by sachin on 02-12-2017.
 */

public class MusicCategoryAdapter extends RecyclerView.Adapter<MusicCategoryAdapter.MusicCategoryHolder> {

    Context context;
    ArrayList<DataSisko> data = new ArrayList<>();
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    String tag;
    MediaPlayer radioPlayer;

    public MusicCategoryAdapter(Context context, ArrayList<DataSisko> data, String tag,MediaPlayer radioPlayer)
    {
       this.context = context;
       this.data = data;
       this.tag = tag;
       this.radioPlayer = radioPlayer;
    }

    public class MusicCategoryHolder extends RecyclerView.ViewHolder
    {
        ImageView CategoryImage,categoryButton;
        TextView titleText;
        LinearLayout musicCategoeyList;
        public MusicCategoryHolder(View v) {
            super(v);
            CategoryImage = v.findViewById(R.id.categoryImage);
            categoryButton = v.findViewById(R.id.categoryButton);
            titleText = v.findViewById(R.id.title);
            titleText.setSelected(true);
            musicCategoeyList = v.findViewById(R.id.musicCategoeyList);
        }
    }

    @Override
    public MusicCategoryAdapter.MusicCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.music_category_list_item, parent, false);
        return new MusicCategoryHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MusicCategoryAdapter.MusicCategoryHolder holder, int position) {
       final DataSisko currentData = data.get(position);
        final int categoryId = currentData.getId();
        final String categoryImage = currentData.getImage();
        final String categoryTitle = currentData.getTitle();
//        Picasso.with(context).load(categoryImage).error(R.drawable.list).into(holder.CategoryImage);
        Glide.with(context)
                .load(categoryImage)
                .placeholder(R.drawable.ic_list_new)
                .error(R.drawable.ic_list_new)
                .into(holder.CategoryImage);
        holder.titleText.setText(categoryTitle);

        holder.musicCategoeyList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new MusicCategoryInner(context,categoryId,categoryTitle,tag,radioPlayer);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(null);
                ft.commitAllowingStateLoss();
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
