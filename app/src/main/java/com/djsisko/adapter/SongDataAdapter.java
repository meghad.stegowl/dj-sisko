package com.djsisko.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.djsisko.data.BufferData;
import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.data.PojoSongForPlayer;
import com.djsisko.data.Song;
import com.djsisko.manager.MediaController;
import com.djsisko.phonemidea.CheckNetwork;
import com.djsisko.phonemidea.PhoneMediaControl;
import com.djsisko.phonemidea.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

public class SongDataAdapter extends RecyclerView.Adapter<SongDataAdapter.SongDataHolder> {

    ArrayList<Song> dataModules = new ArrayList<>();
    ArrayList<Song> d = new ArrayList<>();
    PopupWindow pw1, pw2, pw3, pw4;
    int favStatus, playListStatus;
    RecyclerView list;
    private TextView playListText, favText, removePlayList, removeFavourites;
    RecyclerView.LayoutManager layoutManager;
    ExistingPlayListViewAdapter dataAdapter;
    private Context context;
    private String androidId;
    Boolean isConnected;
    RecyclerView commentsRecycler;
    RecyclerView.LayoutManager layoutManager2;
    ArrayList<Song> comments = new ArrayList<>();
    CommentsAdapter commentsAdapter;
    JSONObject listObject;
    GifDrawable drawable1;
    ProgressDialog dialog;
    Handler handler;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    SharedPreferences preferences;
    int nowPlaying = 0;
    MediaPlayer radioPlayer;
    String duration;
    ArrayList<PojoSongForPlayer> sngList;

    public SongDataAdapter(Context c, ArrayList<Song> d, MediaPlayer radioPlayer) {
        this.context = c;
        this.dataModules.clear();
        this.dataModules = d;
        this.radioPlayer = radioPlayer;
        notifyDataSetChanged();
    }

    public class SongDataHolder extends RecyclerView.ViewHolder {

        public TextView songTitle, songArtist, songDuration, songLikeNo;
        public ImageView songImage, likeImage, shareImage, commentsImage, optionImage;
        public TextView songName, duration;
        public GifImageView smallSongGif;
        public RelativeLayout SongList;

        public SongDataHolder(View itemView) {
            super(itemView);
            songImage = itemView.findViewById(R.id.imageIcon);
            songTitle = itemView.findViewById(R.id.songName);
            songTitle.setSelected(true);
            songTitle.setHorizontallyScrolling(true);
            songArtist = itemView.findViewById(R.id.songDetails);
            likeImage = itemView.findViewById(R.id.like);
            shareImage = itemView.findViewById(R.id.share);
            songDuration = itemView.findViewById(R.id.duration);
            songLikeNo = itemView.findViewById(R.id.likeNo);
            commentsImage = itemView.findViewById(R.id.comments);
            optionImage = itemView.findViewById(R.id.options);
            smallSongGif = itemView.findViewById(R.id.equilizer);
            SongList = itemView.findViewById(R.id.songList);

        }
    }

    @Override
    public SongDataAdapter.SongDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.song_list_item, parent, false);
        try {
            sngList = new ArrayList<PojoSongForPlayer>();
            Song myBean;
            for (int i = 0; i < dataModules.size(); i++) {
                myBean = dataModules.get(i);
                duration = myBean.getSongDuration();
                String durInMili = String.valueOf(Utils.getInstance().strToMilli(duration));
                PojoSongForPlayer mDetail = new PojoSongForPlayer(myBean.getSongTitle(), durInMili, myBean.getSongUrl(), myBean.getI(), myBean.getSongArtist(), myBean.getSongId());
                sngList.add(mDetail);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new SongDataHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SongDataAdapter.SongDataHolder holder, final int position) {
        preferences = context.getSharedPreferences("MyAndroidId", Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId", "");
        isConnected = preferences.getBoolean("isConnected", false);
        Log.d("SongAdapter", "Android id: " + androidId);
        Log.d("SongAdapter", "Net is connected: " + isConnected);
        final Song currentData = dataModules.get(position);
        final int id = currentData.getSongId();
        final String songUrl = currentData.getSongUrl();
        final int favId = currentData.getFavId();
        final int playlistId = currentData.getPlaylistId();
        favStatus = currentData.getFavStatus();
        playListStatus = currentData.getPlayListStatus();
        duration = currentData.getSongDuration();
//        Picasso.with(context).load(currentData.getI()).error(R.drawable.barkologo).into(holder.songImage);
        Glide.with(context)
                .load(currentData.getI())
                .placeholder(R.drawable.barkologo)
                .error(R.drawable.barkologo)
                .into(holder.songImage);
        int likeNos = currentData.getSongLikeNo();
        int totalLikes = currentData.getTotalLikes();
        nowPlaying = currentData.getNowPlaying();
        holder.songLikeNo.setText(String.valueOf(totalLikes));
        holder.songTitle.setText(currentData.getSongTitle());
        holder.songArtist.setText(currentData.getSongArtist());
        holder.songDuration.setText(currentData.getSongDuration());
//        holder.songTitle.setSelected(true);
//        holder.songTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
//        holder.songTitle.setSingleLine(true);
        holder.songArtist.setSelected(true);
        holder.songArtist.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.songArtist.setSingleLine(true);
        if (likeNos == 0) {
            holder.likeImage.setImageResource(R.drawable.like);
        } else if (likeNos == 1) {
            holder.likeImage.setImageResource(R.drawable.fav_green);
        }
        if (nowPlaying == 1) {
            holder.smallSongGif.setVisibility(View.VISIBLE);
        } else if (nowPlaying == 0) {
            holder.smallSongGif.setVisibility(View.GONE);
        }
        Log.d("SongAdapter", "NowPlaying is:" + nowPlaying);


        holder.SongList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MediaPlayer radioPlayer2 = BufferData.getInstance().getRadioPlayer();
                if (radioPlayer2 != null) {
                    if (radioPlayer2.isPlaying()) {
                        radioPlayer2.stop();
                        radioPlayer2.release();
                        radioPlayer2 = null;
                        BufferData.getInstance().setRadioPlayer(radioPlayer2);
                        MainActivity.checkRadio();
                    }
                }
                notifyDataSetChanged();
                final PojoSongForPlayer mDetail = sngList.get(position);
                if (holder.smallSongGif.getVisibility() == View.VISIBLE) {
                    MainActivity.setLatinLayout();
                }
                else {
                    if (mDetail != null) {
                        final ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                        if (progressDialog != null) {
                            Utils.getInstance().d("not null");
                            if (!progressDialog.isShowing()) {
                                Utils.getInstance().d("not showing");
                                if (CheckNetwork.isInternetAvailable(context)) {
                                    progressDialog.show();
                                    MainActivity.setLatinLayout();
                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                                }
                                if (MediaController.getInstance().isPlayingAudio(mDetail) && !MediaController.getInstance().isAudioPaused()) {
                                    MediaController.getInstance().pauseAudio(mDetail);
                                } else {
                                    MediaController.getInstance().setPlaylist(sngList, mDetail, PhoneMediaControl.SonLoadFor.All.ordinal(), -1);
                                }
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (progressDialog != null) {
                                            if (progressDialog.isShowing()) {
                                                new Handler().postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        String duration1 = duration;
                                                        String strTime = duration1.substring(duration1.length() - 2);
                                                        int sec1 = mDetail.audioProgressSec % 60;
                                                        int sec = Integer.parseInt(strTime);
                                                        if (sec > 1 && sec1 > 1) {
                                                            progressDialog.dismiss();
                                                            MainActivity.setLatinLayout();
                                                        }
                                                    }
                                                }, 500);
                                            }
                                        }
                                    }
                                }, 500);
                            }

                        }
                    }
                }

            }
        });

        holder.likeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject object = new JSONObject();
                try {
                    object.put("u_id", androidId);
                    object.put("song_id", id);

                    Log.d("mytag", "Json Object : " + object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (CheckNetwork.isInternetAvailable(context)) //returns true if internet available
                {
                    new songLIkePostAsyncTask(object, holder.songLikeNo, holder.likeImage).execute();
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.shareImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                String songName = "Song Name: " + currentData.getSongTitle() + "   ";
                String songUrl = "\nSong Url: " + currentData.getSongUrl() + "   ";
                String songArtist = "\nSong Artist: " + currentData.getSongArtist() + "   ";
                String appName = "\nBy : Dj Sisko";
                String shareBody = ("Song Name: " + currentData.getSongTitle() + "\n\n" + "Song Url: " + currentData.getSongUrl() + "\n\n" + "Artist Name: " + currentData.getSongArtist() + "\n\n" + "App Name: " + "By Dj Sisko");
                share.putExtra(Intent.EXTRA_SUBJECT, "Song");
                share.putExtra(Intent.EXTRA_TEXT, shareBody);
                ((MainActivity) context).startActivity(Intent.createChooser(share, "Share song!"));
            }
        });

        holder.optionImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(context,"options Clicked",Toast.LENGTH_SHORT).show();
                try {
                    popupWindow(view, id, playListStatus, favStatus, playlistId, favId, position);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        holder.commentsImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(context,"Comments Clicked",Toast.LENGTH_SHORT).show();
                commentsView(id, position);
            }
        });

    }


    public void removePosition(int position) {
        dataModules.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, dataModules.size());
    }

    @Override
    public int getItemCount() {
        return dataModules.size();
    }


    private class CommentsAdapter extends RecyclerView.Adapter<SongDataAdapter.CommentsAdapter.CommentsHolder> {

        Context context;
        ArrayList<Song> dataComments = new ArrayList<>();

        public CommentsAdapter(Context context, ArrayList<Song> dataComments) {
            this.context = context;
            this.dataComments = dataComments;
        }

        @Override
        public CommentsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.comments_list_item, parent, false);
            return new SongDataAdapter.CommentsAdapter.CommentsHolder(v);
        }

        @Override
        public void onBindViewHolder(SongDataAdapter.CommentsAdapter.CommentsHolder holder, int position) {
            Song currentData = dataComments.get(position);
            String user = currentData.getCommentUser();
            String comment = currentData.getComment();
            holder.commentUser.setText(user);
            holder.comments.setText(comment);
        }

        @Override
        public int getItemCount() {
            return dataComments.size();
        }

        public class CommentsHolder extends RecyclerView.ViewHolder {
            TextView commentUser, comments;

            public CommentsHolder(View itemView) {
                super(itemView);
                commentUser = itemView.findViewById(R.id.userName);
                comments = itemView.findViewById(R.id.commentsText);
            }
        }
    }

    private void commentsView(final int id, int position) {
        final Dialog commentsDialog = new Dialog(this.context, R.style.AlertDialog);
        View view = ((MainActivity) context).getLayoutInflater().inflate(R.layout.commentsdialog, null);
        commentsDialog.setContentView(view);
        commentsDialog.show();
        ImageView backButtonComments = view.findViewById(R.id.backButtonComments);
        TextView menuListHeading = view.findViewById(R.id.menuListHeading);
        menuListHeading.setText("Comments");
        final EditText editComments = view.findViewById(R.id.editTextComments);
        final Button sendButton = view.findViewById(R.id.sendButtonComments);
        backButtonComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commentsDialog.dismiss();
            }
        });
        commentsRecycler = view.findViewById(R.id.commentsRecycler);
        layoutManager2 = new LinearLayoutManager((MainActivity) context);
        commentsRecycler.setLayoutManager(layoutManager2);
        comments.clear();
//        commentsAdapter = new CommentsAdapter(context,comments);
//        commentsRecycler.setAdapter(commentsAdapter);
        listObject = new JSONObject();
        try {
            listObject.put("song_id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new listCommentsAsyncvTask(listObject).execute();
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!editComments.getText().toString().isEmpty()) {
                    String text = editComments.getText().toString();
                    JSONObject object = new JSONObject();
                    try {
                        Log.d("Song id inside object:", "" + id);
                        Log.d("Comment inside object:", text);
                        object.put("u_id", androidId);
                        object.put("song_id", id);
                        object.put("comment", text);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    new addCommentAsyncTask(object).execute();
                    editComments.getText().clear();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editComments.getWindowToken(), 0);
                    editComments.clearFocus();
                    commentsRecycler.findFocus();
                } else {
                    editComments.requestFocus();
                    editComments.setError("Please enter comment");
                }

            }
        });
    }

    private void popupWindow(View view, final int id, int playListStatus, int favStatus, final int playlistId, final int favId, final int position) throws JSONException {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popupwindow1, (ViewGroup) view.findViewById(R.id.popupwindow1));
        playListText = (TextView) layout.findViewById(R.id.playlistPopUp);
        removePlayList = (TextView) layout.findViewById(R.id.removePlaylistPopUp);
        removeFavourites = (TextView) layout.findViewById(R.id.removeFavouritesPopUp);
        favText = (TextView) layout.findViewById(R.id.favouritesPopUp);
        pw1 = new PopupWindow(layout, RecyclerView.LayoutParams.MATCH_PARENT, 300, true);
        pw1.showAtLocation(layout, Gravity.CENTER, 0, 0);
        if (playListStatus == 1) {
            playListText.setVisibility(View.GONE);
            removePlayList.setVisibility(View.VISIBLE);
        }
        if (favStatus == 1) {
            favText.setVisibility(View.GONE);
            removeFavourites.setVisibility(View.VISIBLE);
        }
        removePlayList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(context,"Remove From Playlist Clicked",Toast.LENGTH_SHORT).show();
                if (playlistId != 0) {
                    JSONObject object = new JSONObject();
                    try {
                        Log.d("SongDataAdapter", "sip_id here:" + playlistId);
                        object.put("sip_id", playlistId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (CheckNetwork.isInternetAvailable(context)) {
                        new RemoveSongPlayListAsyncTask(object).execute();
                        removePosition(position);
                    } else {
                        Toast.makeText(context,"No Internet Conncetion",Toast.LENGTH_SHORT).show();
                    }


                }
            }
        });

        removeFavourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(context,"Remove From Favourites Clicked",Toast.LENGTH_SHORT).show();
                if (favId != 0) {
                    JSONObject object = new JSONObject();
                    try {
                        Log.d("SongDataAdapter", "fav_id here:" + favId);
                        object.put("fav_id", favId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (CheckNetwork.isInternetAvailable(context)) {
                        new RemoveSongFavAsyncTask(object).execute();
                        removePosition(position);
                    } else {
                        Toast.makeText(context,"No Internet Conncetion",Toast.LENGTH_SHORT).show();
                    }


                }
            }
        });
        playListText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, "PlayList Options Clicked", Toast.LENGTH_SHORT).show();
                pw1.dismiss();
                playlistPopUp(v, id);
            }
        });

        favText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject object = new JSONObject();
                try {
                    object.put("u_id", androidId);
                    object.put("song_id", id);
                    Log.d("Song Id in Add song Fav", "" + id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (CheckNetwork.isInternetAvailable(context)) //returns true if internet available
                {
                    new AddFavSongAsyncTask(object).execute();
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void playlistPopUp(View view, final int id) {
        View layout = LayoutInflater.from(context).inflate(R.layout.popupwindow2, (ViewGroup) view.findViewById(R.id.popupWindow2));
        TextView existingPlayList = (TextView) layout.findViewById(R.id.existingPlayList);
        TextView newPlayListText = (TextView) layout.findViewById(R.id.newPlaylist);
        pw2 = new PopupWindow(layout, RecyclerView.LayoutParams.MATCH_PARENT, 300, true);
        pw2.showAtLocation(layout, Gravity.CENTER, 0, 0);
        newPlayListText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pw2.dismiss();
                if (CheckNetwork.isInternetAvailable(context)) //returns true if internet available
                {
                    newPlayListPopUp(v, id);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }


            }
        });
        existingPlayList.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                pw2.dismiss();
                d = new ArrayList<>();
                if (CheckNetwork.isInternetAvailable(context)) //returns true if internet available
                {
                    createNewPopUp(v, id);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void newPlayListPopUp(View view, final int id) {
        View layout = LayoutInflater.from(context).inflate(R.layout.popupwindow3, (ViewGroup)
                view.findViewById(R.id.popupwindow3));
        pw3 = new PopupWindow(layout, RecyclerView.LayoutParams.MATCH_PARENT, 350, true);
        pw3.showAtLocation(layout, Gravity.CENTER, 0, 0);
        final EditText addPlayListName = (EditText) layout.findViewById(R.id.newPlayListName);
        Button createBtn = (Button) layout.findViewById(R.id.createBtn);
        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!addPlayListName.getText().toString().equals("")) {
                    String name = addPlayListName.getText().toString();
                    JSONObject object = new JSONObject();
                    try {
                        object.put("u_id", androidId);
                        object.put("name", name);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    new NewPlayListAsyncTask(object).execute();
                    Toast.makeText(context, "Playlist successfully created", Toast.LENGTH_LONG).show();
                    pw3.dismiss();
                    // pw4.dismiss();
                    createNewPopUp(v, id);
                } else {
                    addPlayListName.setError("Please Enter Playlist Name");
                    addPlayListName.requestFocus();
                }
            }
        });
    }

    private void createNewPopUp(View view, int id) {
        View layout = LayoutInflater.from(context).inflate(R.layout.popupwindow4, (ViewGroup) view.findViewById(R.id.popupwindow4));
        pw4 = new PopupWindow(layout, RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT, true);
        pw4.showAtLocation(layout, Gravity.CENTER, 0, 0);
        list = (RecyclerView) layout.findViewById(R.id.newplaylistRecycler);
        layoutManager = new LinearLayoutManager(context);
        list.setLayoutManager(layoutManager);
        JSONObject object = new JSONObject();
        try {
            object.put("u_id", androidId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        d.clear();
        if (CheckNetwork.isInternetAvailable(context)) //returns true if internet available
        {
            new existingPlayListAsyncTask(object, id).execute();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

       // pw4.dismiss();
    }

    //http://durisimomobileapps.net/djsisko/api/playlist/new
    private class NewPlayListAsyncTask extends AsyncTask<String, Void, String> {
        JSONObject object;

        private NewPlayListAsyncTask(JSONObject object) {
            this.object = object;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/playlist/new";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(object));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.d("New Playlist Result is:", result);
                return result;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                Log.d("New Playlist Response", s);
                JSONObject jsonObject = new JSONObject(s);
                int status = jsonObject.getInt("status");
                Log.d("New PlayList status", "" + status);
                int playlistId = jsonObject.getInt("playlist_id");
                Log.d("New PlayList Id is:", "" + playlistId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    //http://durisimomobileapps.net/djsisko/api/playlist/user
    private class existingPlayListAsyncTask extends AsyncTask<String, Void, String>
    {
        JSONObject object;
        int id;

        private existingPlayListAsyncTask(JSONObject object, int id) {
            this.object = object;
            this.id = id;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/playlist/user";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(object));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.d("ExistingPLaylist Result", result);
                return result;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            Log.d("ExistingPlayList s is", s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                int status = jsonObject.getInt("status");
                if(status == 1) {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    if (jsonArray.length() == 0) {
                        Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
                    }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject playListIdJson = jsonArray.getJSONObject(i);
                        int playListId = playListIdJson.getInt("playlist_id");
                        Log.d("ExistingPlayList id:", "" + playListId);
                        JSONObject playListNameJson = jsonArray.getJSONObject(i);
                        String playListName = playListNameJson.getString("name");
                        Log.d("ExistingPlayList name:", playListName);
                        if (status == 0)
                        {
                            Toast.makeText(context, "No Playlist Found", Toast.LENGTH_SHORT).show();
                        } else {
                            d.add(new Song(playListId, playListName));
                            dataAdapter = new ExistingPlayListViewAdapter(context, d, id, pw4);
                            list.setAdapter(dataAdapter);
                        }
                    }
                }else{
                    //  pw3.dismiss();
                    Toast.makeText(context, "Playlist Not Found", Toast.LENGTH_LONG).show();
                    Log.d("TAG", "Playlist Not Found");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    //   http://durisimomobileapps.net/djsisko/api/addlike
    public class songLIkePostAsyncTask extends AsyncTask<String, Void, String>
    {
        private JSONObject jsonObject;
        TextView likeStatus;
        ImageView likeImage;

        public songLIkePostAsyncTask(JSONObject object, TextView likeStatus, ImageView likeImage)
        {
            this.jsonObject = object;
            this.likeStatus = likeStatus;
            this.likeImage = likeImage;
        }

        @Override
        protected String doInBackground(String... params)
        {
            try
            {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/addlike";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(jsonObject));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.d("Result is:", result);
                return result;
            }
            catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject dataObject = jsonObject.getJSONObject("data");
                int likes = dataObject.getInt("like_status");
                Log.d("Likes are:", "" + likes);
                int totalLikes = dataObject.getInt("total_like");
                Log.d("Total likes are", "" + totalLikes);
                if (likes == 1) {
                    Log.d("SongAdapter", "Likes 1 updated");
                    likeImage.setImageResource(R.drawable.fav_green);
                    likeStatus.setText(String.valueOf(totalLikes));
                } else if (likes == 0) {
                    Log.d("SongAdapter", "Likes 0 updated");
                    likeImage.setImageResource(R.drawable.like);
                    likeStatus.setText(String.valueOf(totalLikes));
                }
//                Toast.makeText(context,"Likes :" +totalLikes,Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                e.printStackTrace();
            }
            // txtString.setText(s);
            System.out.println(s);
        }
    }

    //    http://durisimomobileapps.net/djsisko/api/comments/add
    private class addCommentAsyncTask extends AsyncTask<String, Void, String> {
        JSONObject jsonObject;

        private addCommentAsyncTask(JSONObject jsonObject) {
            this.jsonObject = jsonObject;
        }


        @Override
        protected String doInBackground(String... strings) {
            try {
                MediaType JSON = MediaType.parse("application/json;charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/comments/add";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(jsonObject));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.d("Add Comment json ", jsonObject.toString());
                Log.d("Add Comment json result", result);
                return result;
            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("Comment Response is", s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                int status = jsonObject.getInt("status");
                Log.d("Add Comment Status is:", "" + status);
                if (status == 1) {
                    new listCommentsAsyncvTask(listObject).execute();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    //    http://durisimomobileapps.net/djsisko/api/comments
    private class listCommentsAsyncvTask extends AsyncTask<String, Void, String> {
        JSONObject object;

        private listCommentsAsyncvTask(JSONObject object) {
            this.object = object;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/comments";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(object));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.d("Comment List Result is:", result);
                return result;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                Log.d("Comments list Response", s);
                JSONObject jsonObject = new JSONObject(s);
                int status = jsonObject.getInt("status");
                if (status == 0) {
                    Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                }
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                if (jsonArray.length() == 0) {
                    Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
                }
                if (jsonArray.length() == 0) {
                    Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
                }
                comments.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject userJson = jsonArray.getJSONObject(i);
                    String user = userJson.getString("user");
                    Log.d("User is:", user);
                    JSONObject songIdJson = jsonArray.getJSONObject(i);
                    int songId = songIdJson.getInt("song_id");
                    Log.d("Comments List", "Song Id is:" + songId);
                    JSONObject commentsJson = jsonArray.getJSONObject(i);
                    String comment = commentsJson.getString("comment");
                    Log.d("Comment List", "Comment is:" + comment);
                    Song currentData = new Song();
                    currentData.commentUser = user;
                    currentData.comment = comment;
                    comments.add(new Song(user, comment));

                    try {
                        commentsAdapter = new CommentsAdapter(context, comments);
                        commentsRecycler.setAdapter(commentsAdapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println(s);
        }
    }



    //http://durisimomobileapps.net/djsisko/api/favs/add
    private class AddFavSongAsyncTask extends AsyncTask<String, Void, String> {
        JSONObject object;

        private AddFavSongAsyncTask(JSONObject object) {
            this.object = object;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/favs/add";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(object));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.d("Add Fav Song Result", result);
                return result;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s == null) {
                Log.d("mytag", "Bookings Response is null");
            }else {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    int status = jsonObject.getInt("status");
                    Log.d("Add Fav song status:", "" + status);
                    String msg = jsonObject.getString("msg");
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    pw1.dismiss();
                    Log.d("Add Fav Song Message", msg);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //http://durisimomobileapps.net/djsisko/api/playlist/remove -> sip_id
    private class RemoveSongPlayListAsyncTask extends AsyncTask<String, Void, String> {
        JSONObject object;


        private RemoveSongPlayListAsyncTask(JSONObject object) {
            this.object = object;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/playlist/remove";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(object));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.d("Remove Song PlayList", result);
                return result;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("Remove song from pList", s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                int status = jsonObject.getInt("status");
                Log.d("RemoveSongPList status:", "" + status);
                String msg = jsonObject.getString("msg");
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                Log.d("RemoveSongPList Message", msg);
                if (status == 1) {
                    pw1.dismiss();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    //http://durisimomobileapps.net/djsisko/api/favs/remove -> fav_id
    private class RemoveSongFavAsyncTask extends AsyncTask<String, Void, String> {
        JSONObject object;

        private RemoveSongFavAsyncTask(JSONObject object) {
            this.object = object;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/favs/remove";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(object));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.d("Remove Song Favourite", result);
                return result;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("Remove song from Fav", s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                int status = jsonObject.getInt("status");
                Log.d("RemoveSongFav status:", "" + status);
                String msg = jsonObject.getString("msg");
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                Log.d("RemoveSongFav msg", msg);
                pw1.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}