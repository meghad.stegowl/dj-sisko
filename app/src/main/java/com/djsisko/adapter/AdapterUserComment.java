package com.djsisko.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.djsisko.R;
import com.djsisko.data.AdminComment;
import com.djsisko.data.MessageBoardData;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterUserComment extends RecyclerView.Adapter<AdapterUserComment.DjDataHolder> {

    Context context;
//    ArrayList<EmbeddedVideo> embeddedVideos = new ArrayList<>();
    ArrayList<MessageBoardData> embeddedVideos = new ArrayList<>();
    ArrayList<AdminComment> adminComments = new ArrayList<>();
    RecyclerView.LayoutManager layoutManager;

    public AdapterUserComment(Context context, ArrayList<MessageBoardData> embeddedVideos) {
        this.context = context;
        this.embeddedVideos = embeddedVideos;
    }

    @Override
    public AdapterUserComment.DjDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_user_comment, parent, false);
        return new AdapterUserComment.DjDataHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterUserComment.DjDataHolder holder, int position) {

        holder.tv_title.setText(embeddedVideos.get(position).getUserCommnet());
        Glide.with(context)
                .load(embeddedVideos.get(position).getUserImage())
                .placeholder(R.drawable.barkologo)
                .error(R.drawable.barkologo)
                .into(holder.iv_image);

        holder.tv_time.setText(embeddedVideos.get(position).getTime());
        holder.tv_user_name.setText(embeddedVideos.get(position).getUserName());

        adminComments = embeddedVideos.get(position).getAdminComment();

        layoutManager = new LinearLayoutManager(context);
        holder.rv_admin_comment.setLayoutManager(layoutManager);
        AdapterAdminComment adapterAdminComment = new AdapterAdminComment(context, adminComments);
        holder.rv_admin_comment.setAdapter(adapterAdminComment);
    }

    @Override
    public int getItemCount() {
        return embeddedVideos.size();
    }

    public void add(ArrayList<MessageBoardData> messageBoardData) {
        int size = embeddedVideos.size();
        embeddedVideos.addAll(messageBoardData);
        notifyItemRangeChanged(size, embeddedVideos.size());

//        embeddedVideos.addAll(messageBoardData);
//        notifyDataSetChanged();
    }

    public void clear() {
        embeddedVideos.clear();
        notifyDataSetChanged();
    }

    public class DjDataHolder extends RecyclerView.ViewHolder {
        CircleImageView iv_image;
        TextView tv_title,tv_time,tv_user_name;
        RecyclerView rv_admin_comment;

        public DjDataHolder(View v) {
            super(v);
            iv_image = v.findViewById(R.id.iv_image);
            tv_title = v.findViewById(R.id.tv_title);
            tv_time = v.findViewById(R.id.tv_time);
            tv_user_name = v.findViewById(R.id.tv_user_name);
            rv_admin_comment = v.findViewById(R.id.rv_admin_comment);
        }
    }
}

