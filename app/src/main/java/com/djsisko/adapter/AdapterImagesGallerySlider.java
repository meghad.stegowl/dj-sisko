package com.djsisko.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.fragment.app.FragmentManager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.djsisko.R;
import com.djsisko.data.Song;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by Android18 on 20-10-2016.
 */
public class AdapterImagesGallerySlider extends RecyclerView.Adapter<AdapterImagesGallerySlider.ViewHolder> {

    private ArrayList<Song> arrayList;
    private FragmentManager fragmentManager;
    private Context context;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;

    private DisplayImageOptions options;

    public AdapterImagesGallerySlider(Context context, ArrayList<Song> arrayList) {
        this.context = context;
        this.drawerLayout = drawerLayout;
        this.toolbar = toolbar;
        this.arrayList = arrayList;
        this.fragmentManager = fragmentManager;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.big1logo)
                .showImageForEmptyUri(R.drawable.big1logo)
                .showImageOnFail(R.drawable.big1logo)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_gallery_image;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_gallery_image = (ImageView) itemView.findViewById(R.id.image);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Song bean = arrayList.get(position);

        final String name = bean.getSongTitle();
        ImageLoader.getInstance().displayImage(name, holder.iv_gallery_image, options);
        holder.iv_gallery_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showImagesPopup();
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    private void showImagesPopup() {
        AppCompatDialog imageDialog = new AppCompatDialog(context);
        imageDialog.setContentView(R.layout.custom_dialog_image_gallery);
        imageDialog.show();
        RecyclerView rv_image_gallery = (RecyclerView) imageDialog.findViewById(R.id.rv_image_gallery);
        rv_image_gallery.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));


    }


}

