package com.djsisko.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.djsisko.fragment.DjFragment;
import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.data.Song;

import java.util.ArrayList;

/**
 * Created by sachin on 02-12-2017.
 */

public class DjDataAdapter extends RecyclerView.Adapter<DjDataAdapter.DjDataHolder> {

    Context context;
    ArrayList<Song> data = new ArrayList<>();
    int featuredDjId;
    Fragment frag;
    FragmentManager fm;
    FragmentTransaction ft;
    String tag;
    MediaPlayer radioPlayer;


    public DjDataAdapter(Context context,ArrayList<Song> data,String tag,MediaPlayer radioPlayer)
    {
        this.context = context;
        this.data = data;
        this.tag = tag;
        this.radioPlayer = radioPlayer;
    }

    public class DjDataHolder extends RecyclerView.ViewHolder
    {
        ImageView firstImage;
        TextView middleText;
        RelativeLayout DjList;
        public DjDataHolder(View v) {
            super(v);
            firstImage = v.findViewById(R.id.firstImage);
            middleText = v.findViewById(R.id.middleText);
            DjList = v.findViewById(R.id.DjList);
        }
    }

    @Override
    public DjDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.dj_listitem, parent, false);
        return new DjDataHolder(view);
    }

    @Override
    public void onBindViewHolder(DjDataHolder holder, int position) {
        Song currentData = data.get(position);
        final int featuredId = currentData.getSongId();
        final String name  = currentData.getSongTitle();
        String image = currentData.getI();
        final String instaUrl = currentData.getSongUrl();
        holder.middleText.setText(name);
//        Picasso.with(context).load(image).error(R.drawable.barkologo).into(holder.firstImage);
        Glide.with(context)
                .load(currentData.getI())
                .placeholder(R.drawable.barkologo)
                .error(R.drawable.barkologo)
                .into(holder.firstImage);
        holder.DjList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    frag = new DjFragment(context,featuredId,name,instaUrl,tag,radioPlayer);
                    fm = ((MainActivity) context).getSupportFragmentManager();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.container,frag);
                    ft.addToBackStack(null);
                    ft.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
