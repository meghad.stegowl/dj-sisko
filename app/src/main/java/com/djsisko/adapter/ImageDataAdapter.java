package com.djsisko.adapter;

import android.content.Context;
import androidx.appcompat.app.AppCompatDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.djsisko.R;
import com.djsisko.data.Song;

import java.util.ArrayList;

/**
 * Created by sachin on 28-11-2017.
 */

public class ImageDataAdapter extends RecyclerView.Adapter<ImageDataAdapter.ImageDataHolder>
        {
            Context context;
            ArrayList<Song> dataModules = new ArrayList<>();
//            Gallery gallery;
            RecyclerViewPositionHelper mRecyclerViewHelper;


            public ImageDataAdapter(Context context, ArrayList<Song> dataModules) {
                this.context = context;
                this.dataModules = dataModules;
            }


            public class ImageDataHolder extends RecyclerView.ViewHolder
            {
                ImageView image;

                public ImageDataHolder(View itemView) {
                    super(itemView);
                    image =(ImageView)itemView.findViewById(R.id.listImage);
                }
            }


            @Override
            public ImageDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View v = LayoutInflater.from(context).inflate(R.layout.image_list_item, parent, false);
                return new ImageDataHolder(v);
            }

            @Override
            public void onBindViewHolder(final ImageDataHolder holder, final int position) {
                final Song currentData = dataModules.get(position);
                int id = currentData.getSongId();
                String image = currentData.getSongTitle();
//                Picasso.with(context).load(image).error(R.drawable.barkologo).into(holder.image);
                Glide.with(context)
                        .load(currentData.getSongTitle())
                        .placeholder(R.drawable.big1logo)
                        .into(holder.image);
                holder.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                            showPopupImage(position);
                    }
                });
            }

            private void showPopupImage(int pos) {

                AppCompatDialog imageDialog = new AppCompatDialog(context);
                imageDialog.setContentView(R.layout.custom_dialog_image_gallery);
                imageDialog.show();
                final RecyclerView rv_image_gallery = (RecyclerView) imageDialog.findViewById(R.id.rv_image_gallery);
                rv_image_gallery.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                final AdapterGalleryNew adapter = new AdapterGalleryNew(context, dataModules);
                rv_image_gallery.setAdapter(adapter);
                new PagerSnapHelper().attachToRecyclerView(rv_image_gallery);
                mRecyclerViewHelper = RecyclerViewPositionHelper.createHelper(rv_image_gallery);
                ImageView iv_right = (ImageView) imageDialog.findViewById(R.id.iv_right);
                ImageView close = (ImageView) imageDialog.findViewById(R.id.close);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imageDialog.dismiss();
                    }
                });
                ImageView iv_left = (ImageView) imageDialog.findViewById(R.id.iv_left);
                rv_image_gallery.scrollToPosition(pos);
                iv_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int totalCount = adapter.getItemCount();
                        int lastPosition = mRecyclerViewHelper.findLastVisibleItemPosition();
                        if (lastPosition != 0) {
                            rv_image_gallery.scrollToPosition(lastPosition - 1);
                        }


                    }

                });
                iv_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int totalCount = adapter.getItemCount();
                        int lastPosition = mRecyclerViewHelper.findLastVisibleItemPosition();
                        if (lastPosition < totalCount) {
                            rv_image_gallery.scrollToPosition(lastPosition + 1);
                        }


                    }

                });
            }

            @Override
            public int getItemCount() {
                return dataModules.size();
            }

}
