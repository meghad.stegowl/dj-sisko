package com.djsisko.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.djsisko.R;
import com.djsisko.data.ImageSliderModel;

import java.util.ArrayList;

public class AdapterViewPager extends PagerAdapter {

    private ArrayList<View> views = new ArrayList<View>();
    private ArrayList<ImageSliderModel> imageArraylist;
    private Context context;

    public AdapterViewPager(ArrayList<ImageSliderModel> imageArraylist, Context context) {
        this.context = context;
        this.imageArraylist = imageArraylist;
    }

    @Override
    public int getCount() {
        return imageArraylist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View rootView1 = inflater.inflate(R.layout.row_image_slider, container, false);
        ImageView iv_image = rootView1.findViewById(R.id.iv_image);

        Glide.with(context)
                .load(imageArraylist.get(position).getSponsor_slider_image())
                .placeholder(R.drawable.slider_placeholder)
                .into(iv_image);

        iv_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setData(Uri.parse(imageArraylist.get(position).getSponsor_slider_link()));
                    context.startActivity(intent);

                } catch (ActivityNotFoundException e) {
                    //TODO smth
                }
            }
        });

        container.addView(rootView1);
        return rootView1;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(Object object) {
        int index = views.indexOf(object);
        if (index == -1)
            return POSITION_NONE;
        else
            return index;
    }
}