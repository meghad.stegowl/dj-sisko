package com.djsisko.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.fragment.Video2Fragment;
import com.djsisko.data.Song;

import java.util.ArrayList;

/**
 * Created by sachin on 11-12-2017.
 */

public class VideoDataAdapter extends RecyclerView.Adapter<VideoDataAdapter.VideoDataHolder> {


    Context context;
    ArrayList<Song> data = new ArrayList<>();
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    String tag;
    MediaPlayer radioPlayer;
    LinearLayout adView;

    public VideoDataAdapter(Context context,ArrayList<Song> data,String tag,MediaPlayer radioPlayer,LinearLayout adView)
    {
        this.context = context;
        this.data = data;
        this.tag = tag;
        this.radioPlayer = radioPlayer;
        this.adView = adView;
    }

    public class VideoDataHolder extends RecyclerView.ViewHolder
    {
        LinearLayout videoList;
        ImageView videoImage;
        TextView videoName,date;
        public VideoDataHolder(View v) {
            super(v);
            videoList = v.findViewById(R.id.videoList);
            videoImage = v.findViewById(R.id.videoImage);
            videoName = v.findViewById(R.id.videoName);
            date = v.findViewById(R.id.date);
        }
    }

    @Override
    public VideoDataAdapter.VideoDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.video_list_item, parent, false);
        return new VideoDataAdapter.VideoDataHolder(v);
    }

    @Override
    public void onBindViewHolder(VideoDataAdapter.VideoDataHolder holder, int position) {
        final Song currentData = data.get(position);
        int id = currentData.getSongId();
        String image = currentData.getI();
        final String videoLink = currentData.getVideoLink();
        final String title = currentData.getSongTitle();
        String date = currentData.getDate();
//        Picasso.with(context).load(image).error(R.drawable.biglogo1).into(holder.videoImage);
        Glide.with(context)
                .load(image)
                .placeholder(R.drawable.big1logo)
                .error(R.drawable.big1logo)
                .into(holder.videoImage);
        holder.videoName.setText(title);
        holder.videoName.setSelected(true);
        holder.date.setText(date);
        holder.videoList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new Video2Fragment(context,title,videoLink,tag,radioPlayer,adView);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
      return data.size();
    }
}
