package com.djsisko.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.djsisko.R;
import com.djsisko.data.sponsors;

import java.util.ArrayList;

/**
 * Created by sachin on 29-12-2017.
 */

public class SponsorImageAdapter extends RecyclerView.Adapter<SponsorImageAdapter.SponsorImageHolder> {

    Context context;
    ArrayList<sponsors> data = new ArrayList<>();

    public SponsorImageAdapter(Context context,ArrayList<sponsors> data)
    {
        this.context = context;
        this.data = data;
    }

    public class SponsorImageHolder extends RecyclerView.ViewHolder {
        ImageView sImage;
        public SponsorImageHolder(View v) {
            super(v);
            sImage = v.findViewById(R.id.sImage);
        }
    }

    @Override
    public SponsorImageAdapter.SponsorImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.sponsor_image_list, parent, false);
        return new SponsorImageAdapter.SponsorImageHolder(v);
    }

    @Override
    public void onBindViewHolder(SponsorImageAdapter.SponsorImageHolder holder, int position) {
        sponsors current = data.get(position);
        String image = current.getLogo();
        Glide.with(context)
                .load(current.getLogo())
                .placeholder(R.drawable.big1logo)
                .error(R.drawable.big1logo)
                .into(holder.sImage);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


}
