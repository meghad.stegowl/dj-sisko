package com.djsisko.adapter;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.djsisko.fragment.Events2Fragment;
import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.data.EventsData;

import java.util.ArrayList;

public class EventsDataAdapter extends RecyclerView.Adapter<EventsDataAdapter.EventsDataHolder> {
    Context context;
    ArrayList<EventsData> dataModules = new ArrayList<>();
    Fragment frag;
    FragmentManager fm;
    FragmentTransaction ft;
    String tag;

    public EventsDataAdapter(Context context, ArrayList<EventsData> dataModules,String tag) {
        this.dataModules = dataModules;
        this.context = context;
        this.tag = tag;
    }

    public class EventsDataHolder extends RecyclerView.ViewHolder {
        ImageView calImage;
        TextView location,place,dateTimeText;
        RelativeLayout eventsList;
        public EventsDataHolder(View v) {
            super(v);
            eventsList = v.findViewById(R.id.eventsList);
            calImage = v.findViewById(R.id.calImage);
            location = v.findViewById(R.id.location);
            place = v.findViewById(R.id.place);
            dateTimeText = v.findViewById(R.id.dateTimeText);
            place.setSelected(true);
        }
    }

    @Override
    public EventsDataAdapter.EventsDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.events_list_item, parent, false);
        return new EventsDataAdapter.EventsDataHolder(v);
    }

    @Override
    public void onBindViewHolder(EventsDataAdapter.EventsDataHolder holder, final int position) {
        try {
            final EventsData currentData = dataModules.get(position);
           int id = currentData.getId();
//            Picasso.with(context).load(currentData.getI()).error(R.drawable.cal12).into(holder.calImage);
           holder.location.setText(currentData.geteTitle());
           holder.place.setText(currentData.geteAddress());
           String dateTime = currentData.geteStart() + "  " + " at " + currentData.getTime();
            holder.dateTimeText.setText(dateTime);
            Glide.with(context)
                    .load(currentData.geteImage())
                    .placeholder(R.drawable.big1logo)
                    .error(R.drawable.big1logo)
                    .into(holder.calImage);
           final String eventLong = currentData.geteLongt();
           final String eventLat = currentData.geteLat();
            Log.d("EventAdapter","Long is "+eventLong);
            Log.d("EventsAdapter","Lat is "+eventLat);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                     frag = new Events2Fragment(context,currentData.geteTitle(),currentData,eventLong,eventLat,tag);
                    fm = ((MainActivity) context).getSupportFragmentManager();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.container, frag);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        try {
            return dataModules.size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


}