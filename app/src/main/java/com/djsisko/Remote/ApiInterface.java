package com.djsisko.Remote;

import android.media.Image;

import com.djsisko.data.GetProfile;
import com.djsisko.data.Logout;
import com.djsisko.data.MessageBoard;
import com.djsisko.data.Video;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("logout")
    Call<Logout> GetLogout(@Query("logout") String logout);

//    @POST("messageboard")
//    @FormUrlEncoded
//    Call<MessageBoard> GetMessageBoard(@Field("video_id") String video_id);

    @POST("messageboard/usercomment")
    @FormUrlEncoded
    Call<MessageBoard> GetMessageFromApi(@Query("page") int page,@Field("video_id") String video_id);


    @POST("embeddedvideo")
    @FormUrlEncoded
    Call<Video> GetVideoApi(@Field("user_id") String user_id);

}
