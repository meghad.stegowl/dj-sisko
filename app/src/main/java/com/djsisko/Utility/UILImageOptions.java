package com.djsisko.Utility;

import android.graphics.Bitmap;

import com.djsisko.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;


public class UILImageOptions {


    public static DisplayImageOptions eventOption() {
        DisplayImageOptions options = null;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.big1logo) // resource or drawable
                .showImageForEmptyUri(R.drawable.big1logo) // resource or drawable
                .showImageOnFail(R.drawable.big1logo) // resource or drawable
                .displayer(new FadeInBitmapDisplayer(1000))
                .cacheInMemory(true) // default
                .cacheOnDisk(true) // default
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .build();
        return options;
    }
}


