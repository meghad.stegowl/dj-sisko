package com.djsisko.Utility;



public class Const {

     public static final String PREFS_LOGIN_USERNAME_KEY = "__USERNAME__" ;
    public static final String PREFS_LOGIN_PASSWORD_KEY = "__PASSWORD__" ;

    public static final String PREFS_FILENAME = "djmusic";
    public static final String SHUFFEL = "isdhuffel";
    public static final String REPEAT = "isRepear";
    public static final String CHECK = "check";
    public static final String CHECK2 = "check2";
    public static final String CHECK3 = "check3";
    public static final String GCM_ID = "gcm";
    public static final String USERID= "user_id";
    public static final String SKIP_USERID= "skip_user_id";
    public static final String PROFILE_IMAGE = "profile_img";
    public static final String DEVICE_TYPE = "Android";
    public static final String NAME = "name";

    public static final String SKIP_STATUS = "skip_status";
    public static final String SKIP_VIDEO_STATUS = "skip_video_status";
    public static final String CHECKBOX_STATUS = "checkbox_status";
    public static final String LOGIN_USERNAME = "userName";
    public static final String LOGIN_PASSWORD = "password";
    public static final String VIDEO_ID = "video_id";
    public static final String VIDEO_LINK = "embedded_video_link";

    public static final String HOST_SISKO = "http://durisimomobileapps.net/djsisko/api/";


    public static final String FORGOT_PASSWORD = HOST_SISKO+"forgotpassword";
    public static final String SIGNUP = HOST_SISKO+"user/signup";
    public static final String CHANGE_PASSWORD = HOST_SISKO+"changepassword";
    public static final String PRIVACY_POLICY = HOST_SISKO+"privacy";
    public static final String TERMS = HOST_SISKO+"terms";
    public static final String LOGIN = HOST_SISKO+"appusers/login";
    public static final String LOGOUT = HOST_SISKO+"logout";
    public static final String GETPROFILE = HOST_SISKO+"profile";
    public static final String VIDEO_lIKE = HOST_SISKO+"video/like";
    public static final String SLIDER = HOST_SISKO+"sponsorslider";
    public static final String ADDCOMMENT = HOST_SISKO+"messageboard/addcomment";
    public static final String PROFILEUPDATE = HOST_SISKO+"profile/update";

    public static final String UPLOAD_IMAGE = "http://durisimomobileapps.net/djsisko/api/profile/upload";
//    public static final String UPLOAD_IMAGE ="http://durisimomobileapps.net/soundview/api/Profile/updateimage";
//    public static final String UPLOAD_IMAGE = "https://server.albuquerquecourtesyandparking.com/api/mobile/uploadImage";



}
