/*
 * This is the source code of DMPLayer for Android v. 1.0.0.
 * You should have received a copy of the license in this archive (see LICENSE).
 * Copyright @Dibakar_Mistry, 2015.
 */
package com.djsisko.manager;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.djsisko.activities.MainActivity;
import com.djsisko.data.BufferData;
import com.djsisko.phonemidea.CheckNetwork;

import static com.djsisko.activities.MainActivity.liveRadioButton;
import static com.djsisko.activities.MainActivity.liveRadioButtonon1;
import static com.djsisko.activities.MainActivity.pauseRadio;
import static com.djsisko.activities.MainActivity.playRadio;


public class MusicPlayerReceiver extends BroadcastReceiver {

    ProgressDialog pd;

    @SuppressLint("LongLogTag")
    @Override
    public void onReceive(Context context, Intent intent) {
//		if (TelephonyManager.EXTRA_STATE_RINGING.equals(intent.getStringExtra(TelephonyManager.EXTRA_STATE))){
//			if (MediaController.getInstance().isPlayingAudio(MediaController.getInstance().getPlayingSongDetail())
//					&& !MediaController.getInstance().isAudioPaused()) {
//				MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
//			}
//
//		}
        int status = CheckNetwork.getConnectivityStatusString(context);
        Log.e("Sulod sa network reciever", "Sulod sa network reciever");
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            if (status == CheckNetwork.NETWORK_STATUS_NOT_CONNECTED) {
                Log.d("urtag", "not connected to internet");
                if (MediaController.getInstance().getPlayingSongDetail() != null && !MediaController.getInstance().isAudioPaused()) {
                    MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                }
                else if(MainActivity.radioMusic!= null && MainActivity.radioMusic.isPlaying())
                {
                    MainActivity.radioMusic.pause();
                    playRadio.setVisibility(View.VISIBLE);
                    liveRadioButton.setVisibility(View.VISIBLE);
                    pauseRadio.setVisibility(View.GONE);
                    liveRadioButtonon1.setVisibility(View.GONE);
                }
                Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
            }
        }
//                new ResumeForceExitPause(context).execute();
            if (intent.getAction().equals(Intent.ACTION_MEDIA_BUTTON)) {
                Log.d("urtag", "my receiver called");
                if (intent.getExtras() == null)
                {
                    return;
                }
                KeyEvent keyEvent = (KeyEvent) intent.getExtras().get(Intent.EXTRA_KEY_EVENT);
                if (keyEvent == null) {
                    return;
                }
                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
                    return;
                switch (keyEvent.getKeyCode()) {
                    case KeyEvent.KEYCODE_HEADSETHOOK:

                    case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:

                        if (MediaController.getInstance().isAudioPaused()) {
                            MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
//                        HomeActivity.setplayPause();
                        } else {
                            MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
//                        HomeActivity.setplayPause();
                        }
                        break;
                    case KeyEvent.KEYCODE_MEDIA_PLAY:
                        MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                        break;
                    case KeyEvent.KEYCODE_MEDIA_PAUSE:
                        MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                        break;
                    case KeyEvent.KEYCODE_MEDIA_STOP:
                        break;
                    case KeyEvent.KEYCODE_MEDIA_NEXT:
                        MediaController.getInstance().playNextSong();
                        break;
                    case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                        MediaController.getInstance().playPreviousSong();
                        break;
                }
            }

        else {
            if (intent.getAction().equals(MusicPlayerService.NOTIFY_PLAY))
            {
                MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                MediaPlayer audioplayer = BufferData.getInstance().getRadioPlayer();
                if (audioplayer != null)
                {
                    if (audioplayer.isPlaying())
                    {
//                        HomeActivity.setplayPause();
//                        HomeActivity.iv_homelayout_gif.
                        audioplayer.stop();
                        audioplayer.release();
                        audioplayer = null;
                        BufferData.getInstance().setRadioPlayer(audioplayer);
                    }
                }
//                HomeActivity.checkRadioButton();
//                HomeActivity.gifStartStop();
            } else if (intent.getAction().equals(MusicPlayerService.NOTIFY_PAUSE)) {
                MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
//                HomeActivity.gifStartStop();
            } else if (intent.getAction().equals(android.media.AudioManager.ACTION_AUDIO_BECOMING_NOISY)) {
                MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
            } else if (intent.getAction().equals(MusicPlayerService.NOTIFY_NEXT)) {
                MediaController.getInstance().playNextSong();
            } else if (intent.getAction().equals(MusicPlayerService.NOTIFY_CLOSE)) {

                MediaController.getInstance().cleanupPlayer(context, true, true);
                pd = BufferData.getInstance().getUniversalProgressLoader();
                if(MediaController.getInstance().getPlayingSongDetail()!=null)
                {
                    if(pd != null && pd.isShowing())
                    {
                        pd.dismiss();
                    }
                }

                MusicPreferance.playlist.clear();
                MusicPreferance.playingSongDetail = null;
                MusicPreferance.saveLastSong(context, null);
                MusicPreferance.shuffledPlaylist.clear();
//                HomeActivity.gifStartStop();
            } else if (intent.getAction().equals(MusicPlayerService.NOTIFY_PREVIOUS)) {
                MediaController.getInstance().playPreviousSong();
            }
        }

    }
}
