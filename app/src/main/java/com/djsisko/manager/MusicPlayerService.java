/*
 * This is the source code of DMPLayer for Android v. 1.0.0.
 * You should have received a copy of the license in this archive (see LICENSE).
 * Copyright @Dibakar_Mistry, 2015.
 */
package com.djsisko.manager;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.RemoteControlClient;
import android.os.Build;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.djsisko.ApplicationDMPlayer;
import com.djsisko.R;
import com.djsisko.activities.MainActivity;
import com.djsisko.data.PojoSongForPlayer;
import com.djsisko.Utility.Const;
import com.djsisko.phonemidea.DMPlayerUtility;
import com.djsisko.phonemidea.Prefs;

import static com.djsisko.activities.MainActivity.liveRadioButton;
import static com.djsisko.activities.MainActivity.liveRadioButtonon1;
import static com.djsisko.activities.MainActivity.pauseRadio;
import static com.djsisko.activities.MainActivity.playRadio;


public class MusicPlayerService extends Service implements NotificationManager.NotificationCenterDelegate {

    public static final String NOTIFY_PREVIOUS = "com.djsisko.previous";
    public static final String NOTIFY_CLOSE = "com.djsisko.close";
    public static final String NOTIFY_PAUSE = "com.djsisko.pause";
    public static final String NOTIFY_PLAY = "com.djsisko.play";
    public static final String NOTIFY_NEXT = "com.djsisko.next";

    //private NotificationManagerCompat notificationManagerCompat;
    private RemoteControlClient remoteControlClient;
    private AudioManager audioManager;
    public static final String NOTIFICATION_CHANNEL_ID = "10003";
    private static boolean ignoreAudioFocus = false;
    boolean isvisible = false;
    private PhoneStateListener phoneStateListener;
    public static Notification notification;
    private NotificationManagerCompat mNotificationManager;
    ProgressDialog mdiloag;
    private static boolean supportBigNotifications = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    private static boolean supportLockScreenControls = Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;

    PojoSongForPlayer mSongDetail;
    private int isPlaying = 0, isRadioPlaying = 0;
    android.app.NotificationManager notificationManager;
    AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener;

    public MusicPlayerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

//        mdiloag = new ProgressDialog(MusicPlayerService.this);
//        mdiloag.setMessage("Loading");
//        mdiloag.setCanceledOnTouchOutside(false);
//        mdiloag.setIndeterminate(true);
//        mdiloag.setCancelable(false);
//        BufferData.getInstance().setnotificationProgressLoader(mdiloag);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioProgressDidChanged);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioPlayStateChanged);
        try {
            phoneStateListener = new PhoneStateListener() {
                @Override
                public void onCallStateChanged(int state, String incomingNumber) {
                    Log.d("urtag","Photestate listner called");
                    if (state == TelephonyManager.CALL_STATE_RINGING)
                    {
                        Log.d("urtag","call state is ringing called");
                        if (MediaController.getInstance().isPlayingAudio(MediaController.getInstance().getPlayingSongDetail())
                                && !MediaController.getInstance().isAudioPaused())
                        {
                            MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                        }
//                        else if(MainActivity.radioMusic!= null)
//                        {
//                            Log.d("urtag", " here out ring - " + isRadioPlaying);
//                            if(MainActivity.radioMusic.isPlaying()) {
//                                isRadioPlaying = 1;
//                                Log.d("urtag", " here in ring - " + isRadioPlaying);
//                                MainActivity.radioMusic.pause();
//                            }
//                        }

                    } else if (state == TelephonyManager.CALL_STATE_IDLE)
                    {
                        Log.d("urtag", " durisimo state is idle");
                        Log.d("urtag", " here out idle - " + isRadioPlaying);

//                        if(isRadioPlaying == 1){
//                            isRadioPlaying = 0;
//                            Log.d("urtag", " here in idle - " + isRadioPlaying);
//                            MainActivity.getInstance().playAudio();
//                        }
//                       if(MediaController.getInstance().getPlayingSongDetail()!= null)
//                       {
//                           if(!MediaController.getInstance().isAudioPaused())
//                           {
//                               MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
//
//                           }
//                       }

                    } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {

                        Log.d("urtag","call state offhook called");
                        if (MediaController.getInstance().getPlayingSongDetail() != null) {
                            if (!MediaController.getInstance().isAudioPaused()) {
                                MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                            }
                        }


                    }
                    super.onCallStateChanged(state, incomingNumber);
                }
            };
            TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            if (mgr != null) {
                mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);

            }
        } catch (Exception e) {
            Log.e("tmessages", e.toString());
        }

        mOnAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
            @Override
            public void onAudioFocusChange(int focusChange)
            {

                if (ignoreAudioFocus) {
                    ignoreAudioFocus = false;
                    return;
                }
                if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT)
                {
                    // Pause
                    // ignoreAudioFocus = true;
                    Log.d("urtag", "AUDIOFOCUS_LOSS_TRANSIENT");
//            if (MediaController.getInstance().isPlayingAudio(MediaController.getInstance().getPlayingSongDetail())
//                    && !MediaController.getInstance().isAudioPaused())
//            {
//                MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
//            }


                    //----------this is for whatsapp mnagement-------------//

                    //-------------my code-------------//
//                    if(MediaController.getInstance().getPlayingSongDetail()!= null)
//                    {
//                        MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
//                    }
                    //-------------my code-------------//

                    if (MediaController.getInstance().getPlayingSongDetail() != null) {
                        if (!MediaController.getInstance().isAudioPaused()) {

                            isPlaying = 1;
                            // islosstransient = 1;
//                            MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                            MediaController.audioPlayer.pause();
                            MediaController.isPaused = true;

                            updateButtons(MediaController.getInstance().getPlayingSongDetail(), notification);
                        }

                    }

                } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                    Log.d("urtag", "focus duck");
                    //ignoreAudioFocus = true;
                    MediaController.getInstance().setduckvolume();
                } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK) {
                    Log.d("urtag", "focus duck gain");
                    ignoreAudioFocus = true;
                    MediaController.getInstance().setnormalvolume();
                } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                    Log.d("urtag", "AUDIOFOCUS_LOSS");

                    //------------------------my code-------------------//
//                    if (MediaController.getInstance().getPlayingSongDetail() != null) {
//                        if (MediaController.getInstance().isAudioPaused() == false) {
//                            MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
//                            ignoreAudioFocus = false;
//                        }
//                    }
                    //------------------------my code-------------------//

                    audioManager.abandonAudioFocus(mOnAudioFocusChangeListener);

                    if (MediaController.getInstance().getPlayingSongDetail() != null) {

                        if (!MediaController.getInstance().isAudioPaused()) {

                            isPlaying = 1;
                            MediaController.audioPlayer.pause();
                            MediaController.isPaused = true;

                            updateButtons(MediaController.getInstance().getPlayingSongDetail(), notification);


                        }
                    }

                    try {
                        if (MainActivity.radioMusic != null) {
                            if (MainActivity.radioMusic.isPlaying()) {
                                playRadio.setVisibility(View.VISIBLE);
                                liveRadioButton.setVisibility(View.VISIBLE);
                                pauseRadio.setVisibility(View.GONE);
                                liveRadioButtonon1.setVisibility(View.GONE);
                                MainActivity.radioMusic.pause();
                            }
                        }
                    } catch(Exception e){
                        e.printStackTrace();
                    }


                } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN)
                {
                    ignoreAudioFocus = false;
                    Log.d("urtag", "AUDIOFOCUS_GAIN");

                    //------------------------my code-------------------//
                    //  MediaController.getInstance().setnormalvolume();

//                    if (MediaController.getInstance().getPlayingSongDetail() != null) {
////                if (MediaController.getInstance().isAudioPaused() == true) {
//                        if (MediaController.getInstance().isAudioPaused()) {
//                            MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
//                            ignoreAudioFocus = false;
//
//                        } else
//                        {
//                            MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
//                            Log.d("urtag", "audio started");
//                            ignoreAudioFocus = false;
//                        }
//                    }

                    //------------------------my code-------------------//

                    if (isPlaying == 1) {
                        isPlaying = 0;
                        MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                    }

                }
            }
        };
        super.onCreate();
    }

    private void updateButtons(PojoSongForPlayer playingSongDetail, Notification notification) {

        if (!MediaController.audioPlayer.isPlaying()) {
            MainActivity.play.setSelected(false);
            MainActivity.play2.setSelected(false);
            this.notification.contentView.setViewVisibility(R.id.player_pause, View.GONE);
            this.notification.contentView.setViewVisibility(R.id.player_play, View.VISIBLE);
            if (supportBigNotifications) {
                this.notification.bigContentView.setViewVisibility(R.id.player_pause, View.GONE);
                this.notification.bigContentView.setViewVisibility(R.id.player_play, View.VISIBLE);
            }
        } else {
            MainActivity.play.setSelected(true);
            MainActivity.play2.setSelected(true);
            this.notification.contentView.setViewVisibility(R.id.player_pause, View.VISIBLE);
            this.notification.contentView.setViewVisibility(R.id.player_play, View.GONE);
            if (supportBigNotifications) {
                this.notification.bigContentView.setViewVisibility(R.id.player_pause, View.VISIBLE);
                this.notification.bigContentView.setViewVisibility(R.id.player_play, View.GONE);
            }
        }
        this.notification.flags |= Notification.FLAG_ONGOING_EVENT;

        notificationManager.notify(5, notification);

    }

    @SuppressLint("NewApi")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String pkg = getPackageName();
        try {
            PojoSongForPlayer messageObject = MediaController.getInstance().getPlayingSongDetail();
            if (messageObject == null) {
                DMPlayerUtility.runOnUIThread(new Runnable() {
                    @Override
                    public void run() {
                        stopSelf();
                    }
                });
                return START_STICKY;
            }
            if (supportLockScreenControls) {
                ComponentName remoteComponentName = new ComponentName(getApplicationContext(), MusicPlayerReceiver.class.getName());
                try {
                    if (remoteControlClient == null) {
                        audioManager.registerMediaButtonEventReceiver(remoteComponentName);
                        Intent mediaButtonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
                        mediaButtonIntent.setComponent(remoteComponentName);
//                        String pkg = getPackageName();
                        PendingIntent mediaPendingIntent = PendingIntent.getBroadcast(ApplicationDMPlayer.applicationContext, 100, mediaButtonIntent.setPackage(pkg), 0);
                        remoteControlClient = new RemoteControlClient(mediaPendingIntent);
                        audioManager.registerRemoteControlClient(remoteControlClient);
                    }
                    remoteControlClient.setTransportControlFlags(RemoteControlClient.FLAG_KEY_MEDIA_PLAY | RemoteControlClient.FLAG_KEY_MEDIA_PAUSE
                            | RemoteControlClient.FLAG_KEY_MEDIA_PLAY_PAUSE | RemoteControlClient.FLAG_KEY_MEDIA_STOP
                            | RemoteControlClient.FLAG_KEY_MEDIA_PREVIOUS | RemoteControlClient.FLAG_KEY_MEDIA_NEXT);
                } catch (Exception e) {
                    Log.e("tmessages", e.toString());
                }
            }

            createNotification(messageObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return START_STICKY;
    }

    @SuppressLint({"NewApi", "WrongConstant"})
    private void createNotification(PojoSongForPlayer mSongDetail) {
//        if (isvisible == true) {
        try {
            String songName = mSongDetail.getS_name();
            String authorName = mSongDetail.getArtist();
            PojoSongForPlayer audioInfo = MediaController.getInstance().getPlayingSongDetail();

            RemoteViews simpleContentView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.player_small_notification);
            RemoteViews expandedView = null;
            if (supportBigNotifications) {
                expandedView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.player_big_notification);
            }

            Intent intent = new Intent(ApplicationDMPlayer.applicationContext, MainActivity.class);
            intent.setAction("openplayer");
            intent.setFlags(32768);
            intent.setPackage(getApplicationContext().getPackageName());
            String pkg = getPackageName();

            PendingIntent contentIntent = PendingIntent.getActivity(ApplicationDMPlayer.applicationContext, 100, intent.setPackage(pkg), 0);
            notificationManager = (android.app.NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = android.app.NotificationManager.IMPORTANCE_LOW;
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
                notificationChannel.setDescription(ApplicationDMPlayer.applicationContext.getResources().getString(R.string.app_name));
                notificationChannel.setShowBadge(true);
                notificationManager.createNotificationChannel(notificationChannel);
//              notificationManager.postNotificationName();
//                notificationManager.;
//                notificationManager.createNotificationChannel(mChannel);
                notification = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID)
                        .setChannelId(NOTIFICATION_CHANNEL_ID).setSmallIcon(R.drawable.logo)
                        .setContentIntent(contentIntent).setContentTitle(songName).build();

            }
            else
                {
                notification = new NotificationCompat.Builder(getApplicationContext())
                        .setSmallIcon(R.drawable.logo)
                        .setContentIntent(contentIntent).setContentTitle(songName).build();

            }


            //   Notification notification = new NotificationCompat.Builder(getApplicationContext()).setSmallIcon(R.mipmap.ic_player)
            //         .setContentIntent(contentIntent).setContentTitle(songName).build();

            notification.contentView = simpleContentView;
            if (supportBigNotifications) {
                notification.bigContentView = expandedView;
            }
            setListeners(simpleContentView);
            if (supportBigNotifications) {
                setListeners(expandedView);
            }
            Bitmap albumArt = audioInfo != null ? audioInfo.getSmallCover(ApplicationDMPlayer.applicationContext) : null;

            if (albumArt != null) {
                notification.contentView.setImageViewBitmap(R.id.player_album_art, albumArt);
                if (supportBigNotifications) {
                    notification.bigContentView.setImageViewBitmap(R.id.player_album_art, albumArt);
                }
            } else {
                notification.contentView.setImageViewResource(R.id.player_album_art, R.drawable.big1logo);
                if (supportBigNotifications) {
                    notification.bigContentView.setImageViewResource(R.id.player_album_art, R.drawable.big1logo);
                }
            }
            notification.contentView.setViewVisibility(R.id.player_progress_bar, View.GONE);
            notification.contentView.setViewVisibility(R.id.player_next, View.VISIBLE);
            notification.contentView.setViewVisibility(R.id.player_previous, View.VISIBLE);
            if (supportBigNotifications) {
                notification.bigContentView.setViewVisibility(R.id.player_next, View.VISIBLE);
                notification.bigContentView.setViewVisibility(R.id.player_previous, View.VISIBLE);
                notification.bigContentView.setViewVisibility(R.id.player_progress_bar, View.GONE);
            }

            if (MediaController.getInstance().isAudioPaused()) {
                notification.contentView.setViewVisibility(R.id.player_pause, View.GONE);
                notification.contentView.setViewVisibility(R.id.player_play, View.VISIBLE);
                if (supportBigNotifications) {
                    notification.bigContentView.setViewVisibility(R.id.player_pause, View.GONE);
                    notification.bigContentView.setViewVisibility(R.id.player_play, View.VISIBLE);
                }
            } else {
                notification.contentView.setViewVisibility(R.id.player_pause, View.VISIBLE);
                notification.contentView.setViewVisibility(R.id.player_play, View.GONE);
                if (supportBigNotifications) {
                    notification.bigContentView.setViewVisibility(R.id.player_pause, View.VISIBLE);
                    notification.bigContentView.setViewVisibility(R.id.player_play, View.GONE);
                }
            }

            notification.contentView.setTextViewText(R.id.player_song_name, songName);
            notification.contentView.setTextViewText(R.id.player_author_name, authorName);
            if (supportBigNotifications) {
                notification.bigContentView.setTextViewText(R.id.player_song_name, songName);
                notification.bigContentView.setTextViewText(R.id.player_author_name, authorName);
            }
            notification.flags |= Notification.FLAG_ONGOING_EVENT;
            startForeground(5, notification);

            if (remoteControlClient != null) {
                RemoteControlClient.MetadataEditor metadataEditor = remoteControlClient.editMetadata(true);
                metadataEditor.putString(MediaMetadataRetriever.METADATA_KEY_ARTIST, authorName);
                metadataEditor.putString(MediaMetadataRetriever.METADATA_KEY_TITLE, songName);
                if (audioInfo != null && audioInfo.getCover(ApplicationDMPlayer.applicationContext) != null) {
                    metadataEditor.putBitmap(RemoteControlClient.MetadataEditor.BITMAP_KEY_ARTWORK,
                            audioInfo.getCover(ApplicationDMPlayer.applicationContext));
                }
                metadataEditor.apply();
                audioManager.requestAudioFocus(mOnAudioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                // isvisible = true;
//                if(MediaController.getInstance().getPlayingSongDetail()!=null)
//                {
//                        MediaController.getInstance().playAudio(mSongDetail);
//                }
                Log.d("urtag", "focus gain");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//        else
//        {
//            notification.notify();
//        }
//    }


    public void setListeners(RemoteViews view)
        {
        try {
            String pkg = getPackageName();

            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, new Intent(NOTIFY_PREVIOUS).setPackage(pkg), PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
            view.setOnClickPendingIntent(R.id.player_previous, pendingIntent);
            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, new Intent(NOTIFY_CLOSE).setPackage(pkg), PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT );
            view.setOnClickPendingIntent(R.id.player_close, pendingIntent);

            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, new Intent(NOTIFY_PAUSE).setPackage(pkg), PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT );
            view.setOnClickPendingIntent(R.id.player_pause, pendingIntent);
            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, new Intent(NOTIFY_NEXT).setPackage(pkg), PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
            view.setOnClickPendingIntent(R.id.player_next, pendingIntent);
            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, new Intent(NOTIFY_PLAY).setPackage(pkg), PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
            view.setOnClickPendingIntent(R.id.player_play, pendingIntent);

            //  -----------SOLUTION OF GETTING DIFFERENT NAME ON NOTIFICATION BAR AND HOME SCREEN WHEN CONTINUOUSLY PRESSING NEXT------------------

//            if(mNotificationManager!= null)
//            {
//                mNotificationManager.cancelAll();
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent)
    {
        Log.d("urtag","on taskremoved called");

        MediaController.getInstance().cleanupPlayer(ApplicationDMPlayer.applicationContext, true, true);
//        MusicPreferance.saveLastSong(ApplicationDMPlayer.applicationContext, null);
        MusicPreferance.playingSongDetail = null;
        MusicPreferance.playlist.clear();
        MusicPreferance.saveLastSong(ApplicationDMPlayer.applicationContext,null);
//        HomeActivity.seek_framlayout.setProgress(0);
////        HomeActivity.seek_framlayout.clearFocus();
////        HomeActivity.seek_framlayout.clearFocus();
//        HomeActivity.seek_framlayout.setProgress(0);
        Prefs.getPrefInstance().setValue(ApplicationDMPlayer.applicationContext, Const.REPEAT, "0");
        Prefs.getPrefInstance().setValue(ApplicationDMPlayer.applicationContext, Const.SHUFFEL, "0");
//        Prefs.getPrefInstance().setValue(HomeActivity.this, Const.NOW_PLAYING, "0");



        super.onTaskRemoved(rootIntent);
    }

    @SuppressLint("NewApi")
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("urtag", "my serice destroy called");
        if (remoteControlClient != null) {

            RemoteControlClient.MetadataEditor metadataEditor = remoteControlClient.editMetadata(true);
            metadataEditor.clear();
            metadataEditor.apply();
            audioManager.unregisterRemoteControlClient(remoteControlClient);
            audioManager.abandonAudioFocus(mOnAudioFocusChangeListener);
        }
        try {
            TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            if (mgr != null) {
                mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
            }
        } catch (Exception e) {
            Log.d("mytag", e.toString());
        }

        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioProgressDidChanged);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioPlayStateChanged);
    }

//    @Override
//    public void onAudioFocusChange(int focusChange) {
//        if (ignoreAudioFocus)
//        {
//            ignoreAudioFocus = false;
//            return;
//        }
//        if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
//            // Pause
//           // ignoreAudioFocus = true;
//            Log.d("urtag", "AUDIOFOCUS_LOSS_TRANSIENT");
////            if (MediaController.getInstance().isPlayingAudio(MediaController.getInstance().getPlayingSongDetail())
////                    && !MediaController.getInstance().isAudioPaused())
////            {
////                MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
////            }
//
//
//            //----------this is for whatsapp mnagement-------------//
//            if(MediaController.getInstance().getPlayingSongDetail()!= null)
//            {
//                MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
//            }
//        }
//
//        else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK)
//        {
//            Log.d("urtag", "focus duck");
//            //ignoreAudioFocus = true;
//            MediaController.getInstance().setduckvolume();
//        }
//
//
//        else if (focusChange == AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK)
//        {
//            Log.d("urtag", "focus duck gain");
//            ignoreAudioFocus = true;
//            MediaController.getInstance().setnormalvolume();
//        }
//
//
//        else if (focusChange == AudioManager.AUDIOFOCUS_LOSS)
//        {
//            Log.d("urtag", "AUDIOFOCUS_LOSS");
//            if (MediaController.getInstance().getPlayingSongDetail() != null) {
//                if (MediaController.getInstance().isAudioPaused() == false) {
//                    MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
//                    ignoreAudioFocus = false;
//                }
//            }
//        }
//             else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
//
//                ignoreAudioFocus = false;
//                Log.d("urtag", "AUDIOFOCUS_GAIN");
//
//              //  MediaController.getInstance().setnormalvolume();
//
//                if (MediaController.getInstance().getPlayingSongDetail() != null) {
////                if (MediaController.getInstance().isAudioPaused() == true) {
//                    if (MediaController.getInstance().isAudioPaused()) {
//                        MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
//                        ignoreAudioFocus = false;
//
//                    } else
//                        {
//                        MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
//                        Log.d("urtag", "audio started");
//                        ignoreAudioFocus = false;
//                    }
//                }
////
//            }
////        else if(focusChange == AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK)
////        {
////            Log.d("urtag","GAIN_MAY_DUCK");
////            ignoreAudioFocus = false;
////            MediaController.getInstance().setnormalvolume();
////
////
////
////        }
//        }


    @Override
    public void didReceivedNotification(int id, Object... args) {
        if (id == NotificationManager.audioPlayStateChanged) {
            PojoSongForPlayer mSongDetail = MediaController.getInstance().getPlayingSongDetail();
            if (mSongDetail != null) {
                createNotification(mSongDetail);
            } else {
                stopSelf();
            }
        }
    }

    @Override
    public void newSongLoaded(Object... args) {

    }

    public static void setIgnoreAudioFocus() {
        ignoreAudioFocus = true;
    }
}
