package com.djsisko.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.djsisko.R;
import com.djsisko.Utility.Const;
import com.djsisko.Utility.PrefsUtils;
import com.djsisko.Utility.WebInterface;
import com.djsisko.phonemidea.Prefs;
import com.djsisko.phonemidea.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONObject;

import java.util.Locale;

public class SplashScreenActivity extends Activity {

    public static final String device = "Android";
    private ProgressDialog mProgressDialog;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        context=SplashScreenActivity.this;

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        String gcm_id = task.getResult().getToken();
                        Log.d("mytag", "Refreshed token: " + gcm_id);
                        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyAndroidId", MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                        Prefs.getPrefInstance().setValue(SplashScreenActivity.this, Const.GCM_ID, gcm_id);
                        Prefs.getPrefInstance().setValue(SplashScreenActivity.this, Const.DEVICE_TYPE, device);
                        editor.putString("token", gcm_id);
                        editor.putString("androidId", androidId);
                        editor.apply();
                    }
                });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                String skip_status =Prefs.getPrefInstance().getValue(SplashScreenActivity.this, Const.SKIP_STATUS, "");
                Log.d("mytag","skip status in splash screen "+skip_status);
                String skip_video_status =Prefs.getPrefInstance().getValue(SplashScreenActivity.this, Const.SKIP_VIDEO_STATUS, "");
                Log.d("mytag","skip_video_status status in splash screen "+skip_video_status);
                if (skip_status.equals("1")) {
                    Log.d("mytag", "Skip user" + Prefs.getPrefInstance().getValue(context, Const.SKIP_USERID, ""));
                    Intent i = new Intent(context, MainActivity.class);
                    startActivity(i);
                    finish();
                } else if (skip_status.equals("0")) {
                    Intent i = new Intent(context, MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(context, LoginActivity.class);
                    startActivity(i);
                    finish();
                }

            }
        }, 3000);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

}
