package com.djsisko.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.djsisko.R;
import com.djsisko.Utility.Const;
import com.djsisko.Utility.PrefsUtils;
import com.djsisko.Utility.WebInterface;
import com.djsisko.phonemidea.CheckNetwork;
import com.djsisko.phonemidea.Prefs;
import com.djsisko.phonemidea.Utils;

import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    TextView tv_forgot_pwd, tv_skip;
    LinearLayout btn_back;
    Context context;
    TextView tv_privacy, tv_terms;
    Button btn_login, btn_create_pro;
    EditText edt_pwd, edt_user_name;
    String userName, password;
    private ProgressDialog mProgressDialog;
    private LinearLayout ll_terms;
    private ImageView iv_pwd, iv_home, iv_video;
    private CheckBox cb_remember;
    private LinearLayout ll_remember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = LoginActivity.this;
        init();
        listener();
    }

    private void init() {
        tv_forgot_pwd = findViewById(R.id.tv_forgot_pwd);
        tv_skip = findViewById(R.id.tv_skip);
        btn_back = findViewById(R.id.btn_back);
        tv_privacy = findViewById(R.id.tv_privacy);
        tv_terms = findViewById(R.id.tv_terms);
        ll_terms = findViewById(R.id.ll_terms);
        btn_login = findViewById(R.id.btn_login);
        btn_create_pro = findViewById(R.id.btn_create_pro);
        edt_pwd = findViewById(R.id.edt_pwd);
        edt_user_name = findViewById(R.id.edt_user_name);
        iv_pwd = findViewById(R.id.iv_pwd);
        cb_remember = findViewById(R.id.cb_remember);
        iv_home = findViewById(R.id.iv_home);
        iv_video = findViewById(R.id.iv_video);
    }

    private void listener() {

        iv_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iv_pwd.isSelected()) {
                    iv_pwd.setSelected(false);
                    Log.d("mytag", "in case 1");
                    edt_pwd.setInputType(InputType.TYPE_CLASS_TEXT);
                    edt_pwd.setTextSize(19);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/SourceSansPro-Regular.ttf");
                    edt_pwd.setTypeface(type);
                } else {
                    Log.d("mytag", "in case 2");
                    iv_pwd.setSelected(true);
                    edt_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_pwd.setTextSize(19);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/SourceSansPro-Regular.ttf");
                    edt_pwd.setTypeface(type);
                }
            }
        });


        tv_forgot_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(i);
                finish();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SplashScreenActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.exit_anim, R.anim.entry_anim);
                finish();
            }
        });

        tv_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, PrivacyPolicyActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.exit_anim, R.anim.entry_anim);
                finish();
            }
        });

        ll_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, TermsActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.exit_anim, R.anim.entry_anim);
                finish();
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateData();

            }
        });

        btn_create_pro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, CreateProfileActivity.class);
                startActivity(intent);
            }
        });

        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    skip_user_api();
                    Intent i = new Intent(context, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }


            }
        });

        iv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    Prefs.getPrefInstance().setValue(context, Const.SKIP_VIDEO_STATUS, "1");
                    skip_user_api();
                    Intent i = new Intent(context, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        iv_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    Intent i = new Intent(context, EmbededVideoActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);

                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        String stst = Prefs.getPrefInstance().getValue(LoginActivity.this, Const.CHECKBOX_STATUS, "");
        Log.d("mytag", "statyus of the checkbox is----" + stst);
        if (stst.equals("1")) {
            cb_remember.setChecked(true);
            String userName1 = Prefs.getPrefInstance().getValue(context, Const.LOGIN_USERNAME, "");
            String password1 = Prefs.getPrefInstance().getValue(context, Const.LOGIN_PASSWORD, "");
            Log.d("mytag", "userName and password id edittext----" + userName + "         " + password);
            Log.d("mytag", "userName and password id----" + userName1 + "         " + password1);
            Log.d("mytag", "statyus of the in true part is----");
            if (!userName1.equals("") && !password1.equals("")) {
                edt_user_name.setText(userName1);
                edt_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                edt_pwd.setText(password1);
            } else {
                userName = edt_user_name.getText().toString().trim();
                password = edt_pwd.getText().toString().trim();
            }
        } else {
            cb_remember.setChecked(false);
        }

        cb_remember.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    Log.d("mytag", "checkbox is-----true----");
                    Prefs.getPrefInstance().setValue(LoginActivity.this, Const.CHECKBOX_STATUS, "1");
                    String userName1 = Prefs.getPrefInstance().getValue(context, Const.LOGIN_USERNAME, "");
                    String password1 = Prefs.getPrefInstance().getValue(context, Const.LOGIN_PASSWORD, "");
                    Log.d("mytag", "userName and password id edittext----" + userName + "         " + password);
                    Log.d("mytag", "userName and password id----" + userName1 + "         " + password1);
                    Log.d("mytag", "cb_remember true part is----");
                    if (!userName1.equals("") && !password1.equals("")) {
                        edt_user_name.setText(userName1);
                        edt_pwd.setText(password1);
                    } else {
                        userName = edt_user_name.getText().toString().trim();
                        password = edt_pwd.getText().toString().trim();
                    }
                } else {
                    Log.d("mytag", "checkbox is-----false----");
                    Prefs.getPrefInstance().remove(context, Const.LOGIN_USERNAME);
                    Prefs.getPrefInstance().remove(context, Const.LOGIN_PASSWORD);
                    Prefs.getPrefInstance().setValue(LoginActivity.this, Const.CHECKBOX_STATUS, "0");
                }
            }
        });
    }

    private void validateData() {
        userName = edt_user_name.getText().toString().trim();
        password = edt_pwd.getText().toString().trim();
        if (!userName.isEmpty() && !password.isEmpty()) {
            if (CheckNetwork.isInternetAvailable(context)) {
                Login(userName, password);
            } else {
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (userName.isEmpty()) {
                edt_user_name.setError("Please Enter UserName or Email");
                edt_user_name.requestFocus();
            } else if (password.isEmpty()) {
                edt_pwd.setError("Please Enter Password");
                edt_pwd.requestFocus();
            }
        }
    }

    private void clearAllEdditText() {
        edt_user_name.requestFocus();
        edt_user_name.getText().clear();
        edt_pwd.getText().clear();
    }

    @SuppressLint("StaticFieldLeak")
    public void skip_user_api() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    String gcm_id = Prefs.getPrefInstance().getValue(context, Const.GCM_ID, "");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("type", 0);
                    jsonObject.put("gcm_id", gcm_id);
                    Utils.getInstance().d("mytag put is -------" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.SIGNUP, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);

                Log.d("mytag", "skip user response iss--response----" + response);
                if (response == null) {
                    Toast.makeText(context, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            int user_id = jsonObject.getInt("user_id");
                            String gcm_id = jsonObject.getString("gcm_id");
                            Log.d("mytag", "skip user api user id----" + user_id);
                            Prefs.getPrefInstance().setValue(context, Const.SKIP_STATUS, "0");
                            String skip_user_id = String.valueOf(user_id);
                            Log.d("mytag", "skip skip_user_id api user id----" + skip_user_id);
                            PrefsUtils.saveToPrefs(context, Const.SKIP_USERID, skip_user_id);

                            Prefs.getPrefInstance().remove(context, Const.LOGIN_USERNAME);
                            Prefs.getPrefInstance().remove(context, Const.LOGIN_PASSWORD);
                            Prefs.getPrefInstance().remove(context, Const.CHECKBOX_STATUS);
                            Prefs.getPrefInstance().remove(context, Const.PROFILE_IMAGE);
                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    public void skip_user_api_video() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    String gcm_id = Prefs.getPrefInstance().getValue(context, Const.GCM_ID, "");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("type", 0);
                    jsonObject.put("gcm_id", gcm_id);
                    Utils.getInstance().d("mytag put is -------" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.SIGNUP, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);

                Log.d("mytag", "skip user response iss--response----" + response);
                if (response == null) {
                    Toast.makeText(context, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            int user_id = jsonObject.getInt("user_id");
                            String gcm_id = jsonObject.getString("gcm_id");
                            Log.d("mytag", "skip user api user id----" + user_id);
                            Prefs.getPrefInstance().setValue(context, Const.SKIP_STATUS, "0");
                            String skip_user_id = String.valueOf(user_id);
                            Log.d("mytag", "skip skip_user_id api user id----" + skip_user_id);
                            PrefsUtils.saveToPrefs(context, Const.SKIP_USERID, skip_user_id);

                            Intent i = new Intent(context, LoginVideoActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);

                            Prefs.getPrefInstance().remove(context, Const.LOGIN_USERNAME);
                            Prefs.getPrefInstance().remove(context, Const.LOGIN_PASSWORD);
                            Prefs.getPrefInstance().remove(context, Const.CHECKBOX_STATUS);
                            Prefs.getPrefInstance().remove(context, Const.PROFILE_IMAGE);
                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void Login(String userName, String password) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("login_id", userName);
                    jsonObject.put("login_password", password);
                    Prefs.getPrefInstance().setValue(LoginActivity.this, Const.LOGIN_USERNAME, userName);
                    Prefs.getPrefInstance().setValue(LoginActivity.this, Const.LOGIN_PASSWORD, password);
                    Utils.getInstance().d("mytag put is -------" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.LOGIN, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Utils.getInstance().d("mytag" + response);
                mProgressDialog.dismiss();
                if (response == null) {
                    Toast.makeText(context, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        Log.d("mytag", "message is----" + message);
                        if (status == 1) {
                            clearAllEdditText();

                            String user_id = jsonObject.getString("user_id");
                            String user_name = jsonObject.getString("user_name");
                            Log.d("mytag", "name - "+ user_name);
                            String email = jsonObject.getString("email");
                            String gcm_id = jsonObject.getString("gcm_id");
                            String song_comment = jsonObject.getString("song_comment");
                            PrefsUtils.saveToPrefs(LoginActivity.this, Const.USERID, user_id);
                            Prefs.getPrefInstance().setValue(LoginActivity.this, Const.NAME, user_name);
                            Log.d("mytag", "name pref - "+ Prefs.getPrefInstance().getValue(LoginActivity.this, Const.NAME, ""));
                            Prefs.getPrefInstance().setValue(LoginActivity.this, Const.SKIP_USERID, "");
                            Prefs.getPrefInstance().setValue(LoginActivity.this, Const.SKIP_STATUS, "1");

                            AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
                            builder1.setMessage(message);
                            builder1.setCancelable(true);

                            builder1.setNegativeButton(
                                    "Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent loginIntent = new Intent(LoginActivity.this, MainActivity.class);
                                            startActivity(loginIntent);
                                            finish();
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }


}
