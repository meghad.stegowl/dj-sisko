package com.djsisko.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.djsisko.R;
import com.djsisko.Utility.Const;
import com.djsisko.Utility.FileUtils;
import com.djsisko.Utility.MultipartUtility;
import com.djsisko.Utility.PrefsUtils;
import com.djsisko.Utility.WebInterface;
import com.djsisko.phonemidea.CheckNetwork;
import com.djsisko.phonemidea.Prefs;
import com.djsisko.phonemidea.Utils;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class CreateProfileActivity extends AppCompatActivity {

    EditText edt_name,edt_user_name,edt_email,edt_phone,edt_pwd,edt_confirm_pwd;
    String name, userName, email,phone, password, confirm_password;
    Button btn_create_profile;
    CircleImageView profilePic,iv_edit;
    Context context;
    private ProgressDialog mProgressDialog;
    LinearLayout btn_back;
    private  ImageView iv_pwd,iv_confirm_pwd;

    ArrayAdapter<String> adapter;
    private static final int PICK_FROM_CAMERA = 11;
    private static final int PICK_FROM_FILE = 13;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    private Uri mImageCaptureUri;
    private String imageToString = "1";
    String image_from_pref;
    private ImageView iv_video,iv_home;
    FrameLayout fm_profile;
    private String lastChar = " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);
        context = CreateProfileActivity.this;
        Prefs.getPrefInstance().setValue(context, Const.PROFILE_IMAGE, "");

        final String[] items = new String[]{"Take from camera", "Gallery", "Cancel"};
        adapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, items);

//        if (Utils.getInstance().isConnectivity(context)) {
//            loadImage();
//        } else {
//            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
//        }

        init();
        listener();
    }

    private void init() {
        btn_back = findViewById(R.id.btn_back);
        edt_name = findViewById(R.id.edt_name);
        edt_user_name = findViewById(R.id.edt_user_name);
        edt_email = findViewById(R.id.edt_email);
        edt_phone = findViewById(R.id.edt_phone);
        edt_pwd = findViewById(R.id.edt_pwd);
        edt_confirm_pwd = findViewById(R.id.edt_confirm_pwd);
        btn_create_profile = findViewById(R.id.btn_create_profile);
        profilePic = findViewById(R.id.profilePic);
        iv_edit = findViewById(R.id.iv_edit);
        iv_pwd = findViewById(R.id.iv_pwd);
        iv_video = findViewById(R.id.iv_video);
        iv_home = findViewById(R.id.iv_home);
        fm_profile = findViewById(R.id.fm_profile);
        iv_confirm_pwd = findViewById(R.id.iv_confirm_pwd);
    }

    private void listener() {

        iv_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skip_user_api();
                Intent intent = new Intent(context, LoginVideoActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        iv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    skip_user_api();
                    Intent i = new Intent(context, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
            }
        });

        iv_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iv_pwd.isSelected()) {
                    iv_pwd.setSelected(false);
                    Log.d("mytag", "in case 1");
                    edt_pwd.setInputType(InputType.TYPE_CLASS_TEXT);
                    edt_pwd.setTextSize(19);
                    Typeface type = Typeface.createFromAsset(getAssets(),"fonts/SourceSansPro-Regular.ttf");
                    edt_pwd.setTypeface(type);
                } else {
                    Log.d("mytag", "in case 2");
                    iv_pwd.setSelected(true);
                    edt_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_pwd.setTextSize(19);
                    Typeface type = Typeface.createFromAsset(getAssets(),"fonts/SourceSansPro-Regular.ttf");
                    edt_pwd.setTypeface(type);
                }
            }
        });

        iv_confirm_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iv_confirm_pwd.isSelected()) {
                    iv_confirm_pwd.setSelected(false);
                    Log.d("mytag", "in case 1");
                    edt_confirm_pwd.setInputType(InputType.TYPE_CLASS_TEXT);
                    edt_confirm_pwd.setTextSize(19);
                    Typeface type = Typeface.createFromAsset(getAssets(),"fonts/SourceSansPro-Regular.ttf");
                    edt_confirm_pwd.setTypeface(type);
                } else {
                    Log.d("mytag", "in case 2");
                    iv_confirm_pwd.setSelected(true);
                    edt_confirm_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_confirm_pwd.setTextSize(19);
                    Typeface type = Typeface.createFromAsset(getAssets(),"fonts/SourceSansPro-Regular.ttf");
                    edt_confirm_pwd.setTypeface(type);
                }
            }
        });


        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateProfileActivity.this, LoginActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.exit_anim, R.anim.entry_anim);
                finish();
            }
        });

        btn_create_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateData();
            }
        });

        fm_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    Utils.getInstance().d("In check permission");
                    CheckPermission();
                } else {
                    if (Utils.getInstance().isConnectivity(context)) {
                        imageupload();
                    } else {
                        Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        edt_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                int digits = edt_phone.getText().toString().length();
                if (digits > 1)
                    lastChar = edt_phone.getText().toString().substring(digits-1);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int digits = edt_phone.getText().toString().length();
                if (!lastChar.equals("-")) {
                    if (digits == 3 || digits == 7) {
                        edt_phone.append("-");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    public static void loadGlide(Context context, ImageView iv, String url) {
        try {
            Glide.with(context)
                    .load(url)
                    .placeholder(R.drawable.big1logo)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(iv);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void validateData() {
        name = edt_name.getText().toString().trim();
        userName = edt_user_name.getText().toString().trim();
        email = edt_email.getText().toString().trim();
        phone = edt_phone.getText().toString().replace("-","");
        password = edt_pwd.getText().toString().trim();
        confirm_password = edt_confirm_pwd.getText().toString().trim();

        image_from_pref = Prefs.getPrefInstance().getValue(context, Const.PROFILE_IMAGE, "");
        loadGlide(context, profilePic, image_from_pref);

        if (!name.isEmpty() && !userName.isEmpty() && isValidEmail(email)&& isValidphone(phone)
                && !password.isEmpty() && !confirm_password.isEmpty()) {
            if (CheckNetwork.isInternetAvailable(context)) {
                if (edt_pwd.getText().toString().equals(edt_confirm_pwd.getText().toString())) {
                    if (Utils.getInstance().isConnectivity(context)) {
                        submitProfileDetail(name,userName,email,phone,password,confirm_password);
                    } else {
                        Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("Password do not match");
                    builder1.setCancelable(true);

                    builder1.setNegativeButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            } else {
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (name.isEmpty()) {
                edt_name.setError("Please Enter Name");
                edt_name.requestFocus();
            } else if (userName.isEmpty()) {
                edt_user_name.setError("Please Enter UserName");
                edt_user_name.requestFocus();
            }  else if (!isValidEmail(email)) {
                edt_email.setError("Please Enter Valid Email");
                edt_email.requestFocus();
            } else if (edt_phone.getText().toString().length() == 0) {
                edt_phone.setError("Please Enter Mobile Number");
                edt_phone.requestFocus();
            } else if (edt_phone.getText().toString().length() != 0 && edt_phone.getText().toString().length() < 10) {
                edt_phone.setError("Please Enter Valid Mobile Number");
                edt_phone.requestFocus();
            }else if (password.isEmpty()) {
                edt_pwd.setError("Please Enter Password");
                edt_pwd.requestFocus();
            }else if (confirm_password.isEmpty()) {
                edt_confirm_pwd.setError("Please Enter Confirm Password");
                edt_confirm_pwd.requestFocus();
            }
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }


    private boolean isValidphone(String bphone) {
        return !TextUtils.isEmpty(bphone) && bphone.length() == 10 && TextUtils.isDigitsOnly(bphone);
    }

    @SuppressLint("StaticFieldLeak")
    private void submitProfileDetail(String name, String userName, String email, String phone, String password,
                                     String confirm_password) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    String gcm_id = Prefs.getPrefInstance().getValue(CreateProfileActivity.this, Const.GCM_ID, "");
                    String device = Prefs.getPrefInstance().getValue(CreateProfileActivity.this, Const.DEVICE_TYPE, "");
                    String profile_img = Prefs.getPrefInstance().getValue(context, Const.PROFILE_IMAGE, "");
                    Log.d("mytag", "image is ---" + profile_img);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("type", 1);
                    jsonObject.put("u_id", androidId);
                    jsonObject.put("gcm_id", gcm_id);
                    jsonObject.put("name", name);
                    jsonObject.put("user_name", userName);
                    jsonObject.put("email", email);
                    jsonObject.put("contact", phone);
                    jsonObject.put("password", password);
                    image_from_pref = Prefs.getPrefInstance().getValue(context, Const.PROFILE_IMAGE, "");
                    if (!image_from_pref.equals("")) {
                        Log.d("mytag", "image_from_pref image is ---" + image_from_pref);
                        jsonObject.put("image", image_from_pref);
                    } else {
                        jsonObject.put("image", "");
                        Utils.getInstance().d("image in else -------");
                    }
                    jsonObject.put("device", device);
                    Utils.getInstance().d("mytag put is -------" + jsonObject.toString());
                    Prefs.getPrefInstance().setValue(context, Const.PROFILE_IMAGE, imageToString);
                    response = WebInterface.getInstance().doPostRequest(Const.SIGNUP, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Utils.getInstance().d("mytag" + response);
                mProgressDialog.dismiss();
                if (response == null) {
                    Toast.makeText(context, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                            int user_id = jsonObject.getInt("user_id");
                            String gcm_id = jsonObject.getString("gcm_id");
                            String user_name = jsonObject.getString("user_name");
                            String email = jsonObject.getString("email");
                            PrefsUtils.saveToPrefs(CreateProfileActivity.this, Const.USERID, String.valueOf(user_id));
                            clearAllEdditText();

                            Intent intent = new Intent(CreateProfileActivity.this, LoginActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.exit_anim, R.anim.entry_anim);
                            finish();

                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

    private void clearAllEdditText() {
        edt_name.requestFocus();
        edt_name.getText().clear();
        edt_user_name.getText().clear();
        edt_email.getText().clear();
        edt_phone.getText().clear();
        edt_pwd.getText().clear();
        edt_confirm_pwd.getText().clear();

    }

    @SuppressLint("NewApi")
    private void CheckPermission() {

        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera Access");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write Storage.");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "Sisko needs some permissions from your device.";
                for (int i = 1; i < permissionsNeeded.size(); i++)

                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                }
                            });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }

        imageupload();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                return shouldShowRequestPermissionRationale(permission);
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    imageupload();
                } else {
                    Toast.makeText(context, "Some Permission is Denied", Toast.LENGTH_SHORT).show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void imageupload() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(),
                            "djsisko" + System.currentTimeMillis() + ".jpg"));
                    Log.d("mytag", "imagetostring---------" + mImageCaptureUri);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                    try {
                        intent.putExtra("return-data", true);
                        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                        StrictMode.setVmPolicy(builder.build());
                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                if (item == 1) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != -1) return;

        switch (requestCode) {
            case PICK_FROM_CAMERA:
                Log.d("mytag", "inside class:FragmentMyProfile \t pick from camara");
                Log.d("mytag", "imagepathCamera" + FileUtils.getPath(context, mImageCaptureUri));
                docrop(mImageCaptureUri);
                break;

            case PICK_FROM_FILE:
                Log.d("mytag", "inside class:FragmentMyProfile \t pic from file");
                mImageCaptureUri = data.getData();
                Log.d("mytag", "imagepathGallary" + FileUtils.getPath(context, mImageCaptureUri));
                docrop(mImageCaptureUri);
                break;
        }
    }

    private void docrop(Uri uri) {
        final AppCompatDialog dialog1;
        dialog1 = new AppCompatDialog(context);
        dialog1.setContentView(R.layout.custom_dialog_crop);
        dialog1.setTitle("");
        dialog1.show();
        Button btn_crop = dialog1.findViewById(R.id.btn_crop);
        Button btn_rotate = dialog1.findViewById(R.id.btn_roate);
        final CropImageView cropImageView = dialog1.findViewById(R.id.cropImageView);
        cropImageView.setImageUriAsync(uri);

        btn_crop.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onClick(View v) {
                final Bitmap bitmap = cropImageView.getCroppedImage(200, 200);
                dialog1.dismiss();
                {
                    final String[] str = {null};
                    new AsyncTask<Void, Void, String>() {

                        @Override
                        protected void onPreExecute() {
                            mProgressDialog = new ProgressDialog(context);
                            mProgressDialog.setMessage("Loading");
                            mProgressDialog.setCanceledOnTouchOutside(false);
                            mProgressDialog.setIndeterminate(true);
                            mProgressDialog.setCancelable(true);
                            mProgressDialog.show();
                            super.onPreExecute();
                        }

                        @Override
                        protected String doInBackground(Void... params) {
                            try {
                                MultipartUtility multipart = new MultipartUtility(Const.UPLOAD_IMAGE);
                                File f1 = new File(FileUtils.getPath(context, mImageCaptureUri));
                                multipart.addFilePart("image", f1);
                                List<String> response2 = multipart.finish();
                                StringBuilder sb = new StringBuilder();
                                for (String line : response2) {
                                    sb.append(line);
                                    sb.append("\t");
                                }
                                str[0] = sb.toString();

                                Utils.getInstance().d(" URL Upload ::" + sb.toString());
                            } catch (Exception ex) {
                                System.err.println(ex);
                            }

                            return str[0];
                        }

                        @Override
                        protected void onPostExecute(String response) {
                            super.onPostExecute(response);
                            mProgressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                imageToString = jsonObject.getString("image");
                                Log.d("mytag", "imagetostring" + imageToString);
                                if (!imageToString.equals("1") && !imageToString.equals(null)) {
                                    String image = Utils.getInstance().bitmapToString(bitmap);
                                    profilePic.setImageBitmap(bitmap);
                                    Log.d("mytag", "bitmap image is--------------" + bitmap);
                                    Log.d("mytag", "imageToString image is--------------" + imageToString);
                                    Prefs.getPrefInstance().setValue(context, Const.PROFILE_IMAGE, imageToString);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }.execute();
                }
            }
        });

        btn_rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropImageView.rotateImage(90);
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    public void skip_user_api() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    String gcm_id = Prefs.getPrefInstance().getValue(context, Const.GCM_ID, "");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("type", 0);
                    jsonObject.put("gcm_id", gcm_id);
                    Utils.getInstance().d("mytag put is -------" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.SIGNUP, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);

                Log.d("mytag", "skip user response iss--response----" + response);
                if (response == null) {
                    Toast.makeText(context, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            int skip_user_id = jsonObject.getInt("user_id");
                            String gcm_id = jsonObject.getString("gcm_id");
                            Log.d("mytag", "skip user api user id----" + skip_user_id);
                            Prefs.getPrefInstance().setValue(context, Const.SKIP_STATUS, "0");

                            PrefsUtils.saveToPrefs(context, Const.SKIP_USERID, String.valueOf(skip_user_id));

                            Prefs.getPrefInstance().remove(context, Const.LOGIN_USERNAME);
                            Prefs.getPrefInstance().remove(context, Const.LOGIN_PASSWORD);
                            Prefs.getPrefInstance().remove(context, Const.PROFILE_IMAGE);
                            Prefs.getPrefInstance().remove(context, Const.USERID);
                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}
