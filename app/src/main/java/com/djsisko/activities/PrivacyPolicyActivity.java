package com.djsisko.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.djsisko.R;
import com.djsisko.Utility.WebInterface;
import com.djsisko.Utility.Const;
import com.djsisko.phonemidea.Utils;

import org.json.JSONObject;

public class PrivacyPolicyActivity extends AppCompatActivity {

    WebView webView;
    ProgressDialog mProgressDialog;
    Context context;
    LinearLayout btn_back;
    private String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        context = PrivacyPolicyActivity.this;
        init();
        if (Utils.getInstance().isConnectivity(context)) {
            PrivacyPolicy();
        } else {
            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
        }
        listener();
    }

    private void init() {
        webView = findViewById(R.id.webView);
        btn_back = findViewById(R.id.btn_back);
    }

    private void listener() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PrivacyPolicyActivity.this,LoginActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.exit_anim,R.anim.entry_anim);
                finish();
            }
        });

    }

    @SuppressLint("StaticFieldLeak")
    private void PrivacyPolicy() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    response = WebInterface.getInstance().doGet(Const.PRIVACY_POLICY);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Utils.getInstance().d("mytag" + response);
                mProgressDialog.dismiss();
                if (response == null) {
                    Toast.makeText(context, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                            String data_link = jsonObject.getString("data").toString();
                            Log.d("mytag","data is in privacy"+data_link);
                            link = data_link;
//                            startWebView(link);


                            webView.getSettings().setJavaScriptEnabled(true);
                            webView.setBackgroundColor(context.getResources().getColor(R.color.black));

                            webView.loadDataWithBaseURL(null, link, "text/html", "utf-8", null);
                            webView.setWebViewClient(new WebViewClient() {
                                public void onPageFinished(WebView view, String url) {
                                    view.loadUrl("javascript:document.body.style.setProperty(\"color\", \"white\");"
                                    );
                                }
                            });

                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

}
