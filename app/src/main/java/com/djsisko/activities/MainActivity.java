package com.djsisko.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.djsisko.ApplicationDMPlayer;
import com.djsisko.R;
import com.djsisko.Utility.Const;
import com.djsisko.Utility.PrefsUtils;
import com.djsisko.Utility.WebInterface;
import com.djsisko.adapter.MySliderAdapter;
import com.djsisko.custom.ClickableViewPager;
import com.djsisko.custom.OnSwipeTouchListener;
import com.djsisko.data.BufferData;
import com.djsisko.data.PojoSongForPlayer;
import com.djsisko.data.Song;
import com.djsisko.fragment.DjFragment;
import com.djsisko.fragment.HitSingles;
import com.djsisko.fragment.MenuFragment;
import com.djsisko.fragment.MusicCategoryInner;
import com.djsisko.fragment.MyFavorites;
import com.djsisko.fragment.MyPlayList;
import com.djsisko.fragment.MyPlayListSongsFragment;
import com.djsisko.fragment.sliderVideo;
import com.djsisko.manager.MediaController;
import com.djsisko.manager.MusicPlayerService;
import com.djsisko.manager.MusicPreferance;
import com.djsisko.manager.NotificationManager;
import com.djsisko.phonemidea.CheckNetwork;
import com.djsisko.phonemidea.PhoneMediaControl;
import com.djsisko.phonemidea.Prefs;
import com.djsisko.phonemidea.Utils;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, NotificationManager.NotificationCenterDelegate, SeekBar.OnSeekBarChangeListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, AudioManager.OnAudioFocusChangeListener {

    private static final Object progressTimerSync = new Object();
    private static final Object sync = new Object();
    public static volatile Handler applicationHandler = null;
    public static FrameLayout container;
    public static RelativeLayout playLayout, fragmentLayout, mainPlayer, radioLayout, latinLayout;
    public static ImageView play;
    public static GifDrawable drawable1;
    public static ImageView play2;
    public static ImageView playRadio, pauseRadio;
    public static MediaPlayer radioMusic;
    public static ImageView liveRadioButton;
    public static ImageView liveRadioButtonon1;
    public static RelativeLayout bottomViewPlayer, bottomView2, songDurationDetails;
    public static FragmentManager fm;
    static Context context;
    static MainActivity activity;
    static LinearLayout adViewContainer;
    static String radioLink;
    static AudioManager audioManager;
    static Context instance;
    private static Timer progressTimer = null;
    public String live_video = "", live_chat = "";
    public boolean isRadioPlaying = false;
    public LinearLayout headerPlayer;
    ProgressDialog mProgressDialog, mDialog;
    MySliderAdapter mySliderAdapter;
    PojoSongForPlayer songDetail = null;
    LinearLayout songDetails;
    SeekBar seekBar;
    ProgressBar bottom_seekbar;
    String token;
    SharedPreferences preferences;
    GifImageView songGif;
    JSONObject object;
    Fragment frag;
    FragmentTransaction ft;
    String androidId;
    ImageView backRadio, listButton, prv, next, backButton1, playerButton1, prv2, pause2, next2, repeat, like, shuffle, share;
    int TAG_Observer;
    Handler handler;
    ClickableViewPager pager;
    int currentPage = 0;
    ArrayList<PojoSongForPlayer> allSongs = new ArrayList<>();
    ArrayList<Song> dataSlider = new ArrayList<>();
    ArrayList<PojoSongForPlayer> songList = new ArrayList<>();
    ArrayList<Song> imageArray = new ArrayList<>();
    ImageView iv_radioBackImage, iv_latinImage;
    private TextView radioTitle, menuLatinHeader, songName, artist, songNamePlayer, songArtistPlayer, songDuration, songDuration1;
    private int progressValue = 0;
    private boolean isOpenLayout = false;
    private boolean isFragmentLoaded = false;
    private ImageView iv_mini_player, iv_logo2;

//    public static void setchatMessage() {
//        Log.d("mytag","in main activity called");
//        Fragment f = fm.findFragmentById(R.id.content);
//        if (f instanceof videoFromFragment) {
//            Log.d("mytag","in if part of main activity called");
//            ((MainActivity)instance).runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    videoFromFragment.updateChat();
//                }
//            });
//        }

    public static void runOnUIThread(Runnable runnable) {
        runOnUIThread(runnable, 0);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        if (delay == 0) {
            ApplicationDMPlayer.applicationHandler.post(runnable);
        } else {
            ApplicationDMPlayer.applicationHandler.postDelayed(runnable, delay);
        }
    }

    public static MainActivity getInstance() {
        return activity;
    }

    public static void removeAd() {
        adViewContainer.setVisibility(View.GONE);
    }

    public static void VisibleAd() {
        adViewContainer.setVisibility(View.VISIBLE);
    }

    public static void getAd() {
        adViewContainer.setVisibility(View.VISIBLE);
    }

    public static void setRadioLayout() {
        radioLayout.setVisibility(View.VISIBLE);
        fragmentLayout.setVisibility(View.GONE);
        playLayout.setVisibility(View.GONE);
    }

    public static void checkRadio() {
        playRadio.setVisibility(View.VISIBLE);
        liveRadioButton.setVisibility(View.VISIBLE);
        pauseRadio.setVisibility(View.GONE);
        liveRadioButtonon1.setVisibility(View.GONE);
    }

    public static void setPlayLayout() {
        radioLayout.setVisibility(View.GONE);
        fragmentLayout.setVisibility(View.GONE);
        latinLayout.setVisibility(View.GONE);
        playLayout.setVisibility(View.VISIBLE);
    }

    public static void setLatinLayout() {
        container.setVisibility(View.GONE);
        latinLayout.setVisibility(View.VISIBLE);
        playLayout.setVisibility(View.GONE);
        radioLayout.setVisibility(View.GONE);
        fragmentLayout.setVisibility(View.VISIBLE);
    }

    public static void setFragmentLayout() {
        latinLayout.setVisibility(View.GONE);
        playLayout.setVisibility(View.GONE);
        radioLayout.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);
        fragmentLayout.setVisibility(View.VISIBLE);
    }

    public static void PlayPauseEvent() {
        if (MediaController.getInstance().isAudioPaused()) {
            MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
            play2.setSelected(true);
            play.setSelected(true);
            drawable1.start();
        } else {
            MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
            play2.setSelected(false);
            play.setSelected(false);
            drawable1.pause();
        }
    }

    private static void startRadioTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {
                }
            }
            progressTimer = new Timer();
            progressTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (sync) {
                        runOnUIThread(new Runnable() {
                            @Override
                            public void run() {
                                MediaPlayer mediaPlayer = BufferData.getInstance().getRadioPlayer();
                                if (mediaPlayer != null) {
                                    int progress = mediaPlayer.getCurrentPosition();
                                    if (progress > 2) {
                                        final ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                                        if (progressDialog != null) {
                                            if (progressDialog.isShowing()) {
                                                new Handler().postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        progressDialog.dismiss();
                                                        stopRadioTimer();
                                                    }
                                                }, 100);
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
            }, 0, 17);
        }
    }

    private static void stopRadioTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {
                    Log.e("tmessages", e.toString());
                }
            }
        }
    }

    //    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = MainActivity.this;
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        MobileAds.initialize(getApplicationContext(), getResources().getString(R.string.adview));
        FirebaseMessaging.getInstance().subscribeToTopic("dj_sisko");
        isInternetOn();
        context = MainActivity.this;
        activity = this;
        adViewContainer = findViewById(R.id.adViewContainer);
        getAds();
        getMenuLinks();
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        token = task.getResult().getToken();
                        Log.d("mytag", "Refreshed token: " + token);
                        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyAndroidId", MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("token", token);
                        androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                        Log.d("mytag", "user id is--------" + androidId);
                        editor.putString("androidId", androidId);
                        editor.putBoolean("isConnected", isInternetOn());
                        editor.apply();

                        Log.d("Android id is:", androidId);
                        if (CheckNetwork.isInternetAvailable(context)) {
                            object = new JSONObject();
                            try {
                                object.put("u_id", androidId);
                                object.put("gcm_id", token);
                                object.put("device", "android");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            new userAsyncTask(object).execute();
                        } else {
                            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        getInit();
        getIntentData();
        onclicks();

        String skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
        Log.d("mytag", "skip status is in main activity is---" + skip_status);
        applicationHandler = new Handler(context.getMainLooper());
    }

    private void getAds() {
        if (CheckNetwork.isInternetAvailable(context)) {
            new adsdata().execute();
        }
    }

    private void getMenuLinks() {
        if (CheckNetwork.isInternetAvailable(context)) {
            new menulinkdata().execute();
        }
    }

    public void getInit() {
        if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)) {
            checkPermission();
            getPermission();
        }
        mDialog = new ProgressDialog(context);
        mDialog.setMessage("Loading");
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);
        BufferData.getInstance().setUniversalProgressLoader(mDialog);
        getImageSlider();
        preferences = getApplicationContext().getSharedPreferences("MyAndroidId", MODE_PRIVATE);
        token = preferences.getString("token", "");
        Log.d("MainActivity", "Token is:" + token);

        JSONObject object = new JSONObject();
        try {
            object.put("u_id", androidId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (CheckNetwork.isInternetAvailable(context)) {
        } else {
        }
        iv_radioBackImage = findViewById(R.id.radioBackImage);
        iv_latinImage = findViewById(R.id.latinImage);
        fragmentLayout = findViewById(R.id.fragmentLayout);
        playLayout = findViewById(R.id.playLayout);
        radioLayout = findViewById(R.id.radioLayout);
        latinLayout = findViewById(R.id.latinLayout);
        backButton1 = latinLayout.findViewById(R.id.backButton1);
        playerButton1 = latinLayout.findViewById(R.id.playerButton1);
        menuLatinHeader = latinLayout.findViewById(R.id.menuLatinHeader);
        menuLatinHeader.setSelected(true);
        pager = findViewById(R.id.pager);
        container = findViewById(R.id.container);
        iv_mini_player = findViewById(R.id.iv_mini_player);
        bottomViewPlayer = fragmentLayout.findViewById(R.id.bottomViewPlayer);
        play = bottomViewPlayer.findViewById(R.id.play);
        next = bottomViewPlayer.findViewById(R.id.next);
        prv = bottomViewPlayer.findViewById(R.id.prv);
        songName = bottomViewPlayer.findViewById(R.id.songName);
        songName.setSelected(true);
        artist = bottomViewPlayer.findViewById(R.id.artist);
        artist.setSelected(true);
        bottom_seekbar = findViewById(R.id.bottom_seekbar);
        BufferData.getInstance().setSb_progress_single(bottom_seekbar);
        bottom_seekbar.setProgress(0);
        mainPlayer = playLayout.findViewById(R.id.mainPlayer);
        songDetails = mainPlayer.findViewById(R.id.songDetails);
        songDurationDetails = mainPlayer.findViewById(R.id.songDurationDetails);
        songDuration = songDurationDetails.findViewById(R.id.songDuration);
        songDuration1 = songDurationDetails.findViewById(R.id.songDuration1);
        seekBar = songDurationDetails.findViewById(R.id.seekBar);
        BufferData.getInstance().setSb_progress(seekBar);
        seekBar.setProgress(0);
        songNamePlayer = findViewById(R.id.songNamePlayer);
        songNamePlayer.setSelected(true);
        songArtistPlayer = findViewById(R.id.songArtistPlayer);
        songArtistPlayer.setSelected(true);
        headerPlayer = playLayout.findViewById(R.id.headerPlayer);
        liveRadioButton = headerPlayer.findViewById(R.id.liveRadioButton);
        liveRadioButtonon1 = headerPlayer.findViewById(R.id.liveRadioButtonon1);
        listButton = headerPlayer.findViewById(R.id.listButton);
        bottomView2 = playLayout.findViewById(R.id.bottomView2);
        prv2 = bottomView2.findViewById(R.id.prv2);
        pause2 = bottomView2.findViewById(R.id.pause2);
        play2 = bottomView2.findViewById(R.id.play2);
        next2 = bottomView2.findViewById(R.id.next2);
        repeat = bottomView2.findViewById(R.id.repeat);
        shuffle = bottomView2.findViewById(R.id.suffle);
        like = bottomView2.findViewById(R.id.like);
        share = bottomView2.findViewById(R.id.share);
        songGif = bottomView2.findViewById(R.id.songGif);
        drawable1 = (GifDrawable) songGif.getDrawable();
        drawable1.stop();
        backRadio = findViewById(R.id.backRadio);
        playRadio = findViewById(R.id.playRadio);
        pauseRadio = findViewById(R.id.pauseRadio);
        radioTitle = findViewById(R.id.radioTitle);
        iv_logo2 = findViewById(R.id.iv_logo2);
        radioMusic = BufferData.getInstance().getRadioPlayer();

        iv_logo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EmbededVideoActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        if (radioMusic != null && !radioMusic.isPlaying()) {
            playRadio.setVisibility(View.VISIBLE);
            liveRadioButton.setVisibility(View.VISIBLE);
            pauseRadio.setVisibility(View.GONE);
            liveRadioButtonon1.setVisibility(View.GONE);
        }
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        isOpenLayout = playLayout.getVisibility() == View.VISIBLE || f instanceof MenuFragment;
        if (CheckNetwork.isInternetAvailable(context)) {
            new radioAsync().execute();
        } else {
            Toast.makeText(MainActivity.this, "No Internet Connection", Toast.LENGTH_LONG).show();
        }

        String isRepeat = Prefs.getPrefInstance().getValue(this, Const.REPEAT, "");
        String isShuffel = Prefs.getPrefInstance().getValue(this, Const.SHUFFEL, "");

        if (isRepeat.equals("1")) {
            repeat.setSelected(true);
        } else {
            repeat.setSelected(false);
        }
        if (isShuffel.equals("1")) {
            shuffle.setSelected(true);
        } else {
            shuffle.setSelected(false);
        }

        String skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
        Log.d("mytag", "skip status is in menu list frgmnet is---" + skip_status);

        if (skip_status.equals("0")) {
        }else {
            if (Utils.getInstance().isConnectivity(context)) {
                getProfileDetail();
            }
        }


    }

    public void checkPermission() {
        boolean hasPermission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE)
                == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE)
                        == PackageManager.PERMISSION_GRANTED;
        final String[] permissions = new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_WIFI_STATE};
        if (hasPermission) {
            ActivityCompat.requestPermissions(this, permissions, 0);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Call Permission is Required.");
            builder.setTitle("Dj Sisko");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(MainActivity.this, permissions, 0);
                }
            });
            builder.show();
        }
    }

    public void getPermission() {
        int permissionCheckCamera = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);

        int permissionCheckRead = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        int permissionCheckWrite = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        int permissionCheckLocation = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if ((permissionCheckCamera == -1) || (permissionCheckRead == -1) || (permissionCheckWrite == -1) || (permissionCheckLocation == -1)) {

                if (!Settings.System.canWrite(this)) {
                    requestPermissions(new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                            Manifest.permission.ACCESS_FINE_LOCATION}, 2909);
                }
            }
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    public void onclicks() {
        play2.setOnClickListener(this);
        play2.setSelected(false);
        next2.setOnClickListener(this);
        prv2.setOnClickListener(this);
        pause2.setOnClickListener(this);
        repeat.setOnClickListener(this);
        repeat.setSelected(false);
        shuffle.setOnClickListener(this);
        shuffle.setSelected(false);
        like.setOnClickListener(this);
        share.setOnClickListener(this);
        play.setOnClickListener(this);
        play.setSelected(false);
        next.setOnClickListener(this);
        prv.setOnClickListener(this);
        listButton.setOnClickListener(this);
        liveRadioButton.setOnClickListener(this);
        liveRadioButtonon1.setOnClickListener(this);
        backRadio.setOnClickListener(this);
        seekBar.setOnSeekBarChangeListener(this);
        backButton1.setOnClickListener(this);
        playerButton1.setOnClickListener(this);
        playRadio.setOnClickListener(this);
        pauseRadio.setOnClickListener(this);
        pager.setOnItemClickListener(new ClickableViewPager.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Log.d("ClickableViewPager pos:", " " + position);
                switch (position) {
                    case 0:
                        final Song item1 = dataSlider.get(2);
                        Log.d("First Position Slider", item1.getSongTitle());
                        String videoLink = item1.getVideoLink();
                        String title = item1.getSongTitle();
                        if (mDialog != null) {
                            if (!mDialog.isShowing()) {
                                mDialog.show();
                            }
                        }
                        handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                frag = new sliderVideo(context, item1.getSongTitle(), item1.getVideoLink(), "Videos", radioMusic, adViewContainer);
                                fm = ((MainActivity) context).getSupportFragmentManager();
                                ft = fm.beginTransaction();
                                ft.replace(R.id.container, frag);
                                ft.addToBackStack(null);
                                ft.commitAllowingStateLoss();
                                setFragmentLayout();
                                mDialog.dismiss();
                            }
                        }, 500); // 500 milliseconds delay

                        break;
                    case 1:
                        final PojoSongForPlayer item2 = songList.get(0);
                        Log.d("Second Position Slider", item2.getS_name());
                        if (mDialog != null) {
                            if (!mDialog.isShowing()) {
                                mDialog.show();
                            }
                        }
                        handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                if (radioMusic != null && radioMusic.isPlaying()) {
                                    radioMusic.pause();
                                    Prefs.getPrefInstance().setValue(ApplicationDMPlayer.applicationContext, Const.CHECK2, "");
                                    radioMusic.release();
                                    isRadioPlaying = false;
                                    playRadio.setVisibility(View.VISIBLE);
                                    liveRadioButton.setVisibility(View.VISIBLE);
                                    pauseRadio.setVisibility(View.GONE);
                                    liveRadioButtonon1.setVisibility(View.GONE);
                                }
                                if (MediaController.getInstance().isPlayingAudio(item2) && !MediaController.getInstance().isAudioPaused()) {
                                    MediaController.getInstance().pauseAudio(item2);

                                } else {
                                    MediaController.getInstance().setPlaylist(songList, item2, PhoneMediaControl.SonLoadFor.All.ordinal(), -1);
                                }
                                mDialog.dismiss();
                            }
                        }, 500); // 500 milliseconds delay
                        break;
                    case 2:
                        final Song item3 = dataSlider.get(1);
                        Log.d("Third Position Slider", item3.getSongTitle());
                        if (mDialog != null) {
                            if (!mDialog.isShowing()) {
                                mDialog.show();
                            }
                        }
                        handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {

                                frag = new sliderVideo(context, item3.getSongTitle(), item3.getVideoLink(), "Videos", radioMusic, adViewContainer);
                                fm = ((MainActivity) context).getSupportFragmentManager();
                                ft = fm.beginTransaction();
                                ft.replace(R.id.container, frag);
                                ft.addToBackStack(null);
                                ft.commitAllowingStateLoss();
                                setFragmentLayout();
                                mDialog.dismiss();
                            }
                        }, 500); // 500 milliseconds delay
                        break;
                    case 3:
                        final PojoSongForPlayer item4 = songList.get(1);
                        Log.d("Forth Position Slider", item4.getS_name());

                        if (mDialog != null) {
                            if (!mDialog.isShowing()) {
                                mDialog.show();
                            }
                        }
                        handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                if (radioMusic != null && radioMusic.isPlaying()) {
                                    radioMusic.pause();
                                    isRadioPlaying = false;
                                    playRadio.setVisibility(View.VISIBLE);
                                    liveRadioButton.setVisibility(View.VISIBLE);
                                    pauseRadio.setVisibility(View.GONE);
                                    liveRadioButtonon1.setVisibility(View.GONE);
                                }
                                if (MediaController.getInstance().isPlayingAudio(item4) && !MediaController.getInstance().isAudioPaused()) {
                                    MediaController.getInstance().pauseAudio(item4);

                                } else {
                                    MediaController.getInstance().setPlaylist(songList, item4, PhoneMediaControl.SonLoadFor.All.ordinal(), -1);
                                }
                                mDialog.dismiss();
                            }
                        }, 500); // 500 milliseconds delay

                        break;
                    case 4:
                        final Song item5 = dataSlider.get(0);
                        Log.d("Fifth Position Slider", item5.getSongTitle());
                        if (mDialog != null) {
                            if (!mDialog.isShowing()) {
                                mDialog.show();
                            }
                        }
                        handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {

                                frag = new sliderVideo(context, item5.getSongTitle(), item5.getVideoLink(), "Videos", radioMusic, adViewContainer);
                                fm = ((MainActivity) context).getSupportFragmentManager();
                                ft = fm.beginTransaction();
                                ft.replace(R.id.container, frag);
                                ft.addToBackStack(null);
                                ft.commitAllowingStateLoss();
                                setFragmentLayout();
                                mDialog.dismiss();
                            }
                        }, 500); // 500 milliseconds delay
                        break;
                }
            }
        });

        bottomViewPlayer.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeUp() {
                super.onSwipeUp();
//                Toast.makeText(context,"Up",Toast.LENGTH_SHORT).show();
                playLayout.setVisibility(View.VISIBLE);
                ViewAnimator.animate(playLayout)
                        .duration(500)
                        .translationY(2000, 0)
                        .andAnimate(fragmentLayout)
                        .duration(500)
                        .translationY(0, -2000)
                        .onStop(new AnimationListener.Stop() {
                            @Override
                            public void onStop() {
                                stopProgressTimer();
                                fragmentLayout.setVisibility(View.GONE);
                                ViewAnimator.animate(fragmentLayout).duration(500).translationY(-2000, 0).start();
                            }
                        })
                        .start();
            }

        });

        headerPlayer.setOnTouchListener(new OnSwipeTouchListener(this) {
                                            @Override
                                            public void onSwipeDown() {
                                                super.onSwipeDown();
                                                if (isFragmentLoaded) {
                                                    fragmentLayout.setVisibility(View.VISIBLE);
                                                    ViewAnimator.animate(fragmentLayout)
                                                            .duration(500)
                                                            .translationY(-2000, 0)
                                                            .andAnimate(playLayout)
                                                            .translationY(0, 2000)
                                                            .duration(500)
                                                            .onStop(new AnimationListener.Stop() {
                                                                @Override
                                                                public void onStop() {
                                                                    playLayout.setVisibility(View.GONE);
                                                                    ViewAnimator.animate(playLayout).duration(500).translationY(-2000, 0).start();
                                                                }
                                                            })
                                                            .start();
                                                }
                                            }
                                        }
        );

    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        if (playLayout.getVisibility() == View.VISIBLE) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Dj Sisko");
            alertDialog.setMessage("Are you sure want to exit?");
            alertDialog.setPositiveButton("YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    });
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alertDialog.show();
        } else if (f instanceof MenuFragment) {
            setPlayLayout();
        } else if (f instanceof MyFavorites && Objects.equals(f.getTag(), "outer")) {
            setPlayLayout();
        } else if (f instanceof MyPlayList && Objects.equals(f.getTag(), "outer")) {
            setPlayLayout();
        } else if (radioLayout.getVisibility() == View.VISIBLE) {
            setPlayLayout();
        } else {
            super.onBackPressed();
        }
    }

    //    http://durisimomobileapps.net/djsisko/api/slider
    private void getImageSlider() {
        if (CheckNetwork.isInternetAvailable(context)) {
            new sliderData().execute();
        } else {
            Toast.makeText(context, "No Internet Conncetion", Toast.LENGTH_SHORT).show();
        }
    }

    public final boolean isInternetOn() {
        ConnectivityManager connec = (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {
            Log.d("MainActivity", "Connected");
            return true;
        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
            Log.d("MainActivity", "Not Connected");
            return false;
        }
        return false;
    }

    private void getIntentData() {
        try {
            Uri data = getIntent().getData();
            if (data != null) {
                if (data.getScheme().equalsIgnoreCase("file")) {
                    String path = data.getPath();
                    if (!TextUtils.isEmpty(path)) {
                        MediaController.getInstance().cleanupPlayer(context, true, true);
                        updateTitle(false);
                        MediaController.getInstance().playAudio(MusicPreferance.playingSongDetail);

                    }
                }
                if (data.getScheme().equalsIgnoreCase("http")) {
                }
                if (data.getScheme().equalsIgnoreCase("content")) {
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateProgress(final PojoSongForPlayer mSongDetail) {
        if (seekBar != null && bottom_seekbar != null) {
            progressValue = (int) (mSongDetail.audioProgress * 100);
            seekBar.setProgress(progressValue);
            bottom_seekbar.setProgress(progressValue);


            String timeString = String.format("%02d:%02d:%02d", mSongDetail.audioProgressSec / 3600, (mSongDetail.audioProgressSec % 3600) / 60, mSongDetail.audioProgressSec % 60);
            songDuration.setText(timeString);


            final ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            String duration = songDuration.getText().toString();
                            String strTime = duration.substring(duration.length() - 2);
                            int sec1 = mSongDetail.audioProgressSec % 60;
                            int sec = Integer.parseInt(strTime);
                            if (sec > 1 && sec1 > 1) {
                                progressDialog.dismiss();
                            }
                        }
                    }, 500);
                }
            }
        }

    }

    public void playAllSongs() {
        if (allSongs.size() > 0) {
            PojoSongForPlayer currentData = allSongs.get(0);
            if (currentData != null) {
                if (MediaController.getInstance().isPlayingAudio(currentData) && !MediaController.getInstance().isAudioPaused()) {
                    MediaController.getInstance().pauseAudio(currentData);
                } else {
                    MediaController.getInstance().setPlaylist(allSongs, currentData, PhoneMediaControl.SonLoadFor.All.ordinal(), -1);
                }
                songDetail = currentData;
                updateTitle(false);
                loadSongsDetails(currentData);
            }
        }
    }

    @SuppressLint("DefaultLocale")
    private void updateTitle(boolean shutdown) {
        PojoSongForPlayer mSongDetail = MediaController.getInstance().getPlayingSongDetail();
        if (mSongDetail == null && shutdown) {
            return;
        } else {
            updateProgress(mSongDetail);
            if (MediaController.getInstance().isAudioPaused()) {
                play2.setSelected(false);
                play.setSelected(false);
            } else {
                play2.setSelected(true);
                play.setSelected(true);
            }
            PojoSongForPlayer audioInfo = MediaController.getInstance().getPlayingSongDetail();
            loadSongsDetails(audioInfo);

            if (songDuration1 != null) {
                long duration = Long.valueOf(audioInfo.getS_duration());
                if (duration != 0) {
                    songDuration1.setText(Utils.getInstance().milliToString(duration));
                } else {
                    songDuration1.setText("00:00:00");
                }
            }
        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        MediaPlayer mediaPlayer = BufferData.getInstance().getRadioPlayer();
        Log.d("mytag", " here audio focus out");

        if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
            Log.d("mytag", " here audio focus loss trans out");

            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
                BufferData.getInstance().setRadioPlayer(mediaPlayer);
                checkRadio();

            }

        } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
            Log.d("mytag", " here audio focus loss out");

            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
                BufferData.getInstance().setRadioPlayer(mediaPlayer);
                checkRadio();
            }
        } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
        } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN_TRANSIENT) {
            Log.d("mytag", " here audio focus gain trans out");

        }
    }

    @SuppressLint("DefaultLocale")
    public void loadSongsDetails(PojoSongForPlayer mDetail) {
        songNamePlayer.setText(mDetail.getS_name());
        songName.setText(mDetail.getS_name());
        Glide.with(MainActivity.this)
                .load(mDetail.getS_img())
                .fitCenter()
                .transition(new DrawableTransitionOptions().crossFade())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.smalllogo)
                .into(iv_mini_player);
        songArtistPlayer.setText(mDetail.getArtist());
        artist.setText(mDetail.getArtist());
        menuLatinHeader.setText(mDetail.getS_name());
        Glide.with(MainActivity.this)
                .load(mDetail.getS_img())
                .fitCenter()
                .transition(new DrawableTransitionOptions().crossFade())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.big1logo)
                .into(iv_latinImage);

        if (songDuration1 != null) {
            long duration = Long.valueOf(mDetail.getS_duration());
            if (duration != 0) {
                songDuration1.setText(Utils.getInstance().milliToString(duration));
            } else {
                songDuration1.setText("00:00:00");
            }
        }
        updateProgress(mDetail);
    }

    @Override
    protected void onDestroy() {
        removeObserver();
        currentPage = 0;
        MediaPlayer mediaPlayer = BufferData.getInstance().getRadioPlayer();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
            checkRadio();
            BufferData.getInstance().setRadioPlayer(mediaPlayer);

        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        addObserver();
        loadAlreadyPlayng();
    }

    @Override
    protected void onPause() {
        super.onPause();
        removeObserver();
    }

    private void loadAlreadyPlayng() {
        boolean isPaused = MediaController.getInstance().isAudioPaused();
        PojoSongForPlayer mSongDetail = MusicPreferance.getLastSong(context);
        if (mSongDetail != null && !isPaused) {
            songDetail = mSongDetail;
            updateTitle(false);
            loadSongsDetails(mSongDetail);
        }
    }

    private void startImageSliderTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {
                }
            }
            progressTimer = new Timer();
            progressTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (sync) {
                        runOnUIThread(new Runnable() {
                            @Override
                            public void run() {
                                int currentItem = pager.getCurrentItem() + 1;
                                int totalItem = mySliderAdapter.getCount();
                                if (currentItem == totalItem) {
                                    pager.setCurrentItem(0);
                                } else {
                                    pager.setCurrentItem(currentItem);
                                }
                            }
                        });
                    }
                }
            }, 0, 3000);
        }

    }

    private void stopProgressTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {
                    Log.e("message", e.toString());
                }
            }
        }
    }

    public void addObserver() {
        TAG_Observer = MediaController.getInstance().generateObserverTag();
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioDidReset);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioPlayStateChanged);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioDidStarted);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioProgressDidChanged);
        NotificationManager.getInstance().addObserver(this, NotificationManager.newaudioloaded);
    }

    public void removeObserver() {
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioDidReset);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioPlayStateChanged);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioDidStarted);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioProgressDidChanged);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.newaudioloaded);
    }

    @Override
    public void onClick(final View view) {
        int id = view.getId();
        switch (id) {

            case R.id.playRadio:
                if (CheckNetwork.isInternetAvailable(context)) {
                    playAudio();
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.pauseRadio:
                if (radioMusic != null && radioMusic.isPlaying()) {
                    radioMusic.pause();
                    playRadio.setVisibility(View.VISIBLE);
                    liveRadioButton.setVisibility(View.VISIBLE);
                    pauseRadio.setVisibility(View.GONE);
                    liveRadioButtonon1.setVisibility(View.GONE);
                }
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    playRadio.setVisibility(View.VISIBLE);
                    liveRadioButton.setVisibility(View.VISIBLE);
                    pauseRadio.setVisibility(View.GONE);
                    liveRadioButtonon1.setVisibility(View.GONE);
                }
                break;

            case R.id.backButton1:
                setFragmentLayout();
                break;

            case R.id.playerButton1:
                setPlayLayout();
                break;

            case R.id.play2:
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    radioMusic = BufferData.getInstance().getRadioPlayer();
                    if (radioMusic != null && radioMusic.isPlaying()) {
                        radioMusic.pause();
                        radioMusic = null;
                        isRadioPlaying = false;
                        playRadio.setVisibility(View.VISIBLE);
                        liveRadioButton.setVisibility(View.VISIBLE);
                        pauseRadio.setVisibility(View.GONE);
                        liveRadioButtonon1.setVisibility(View.GONE);
                    }
                    PlayPauseEvent();
                } else {
                    MediaPlayer mediaPlayer = BufferData.getInstance().getRadioPlayer();
                    if (mediaPlayer != null) {
                        mediaPlayer.pause();
                        mediaPlayer.release();
                        BufferData.getInstance().setRadioPlayer(null);
                        MainActivity.checkRadio();
                    }
                    if (CheckNetwork.isInternetAvailable(context)) {
                        new AllSongsAsyncTask(object).execute();
                    } else {
                        Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case R.id.next2:
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    radioMusic = BufferData.getInstance().getRadioPlayer();
                    if (radioMusic != null && radioMusic.isPlaying()) {
                        radioMusic.pause();
                        radioMusic = null;
                        isRadioPlaying = false;
                        playRadio.setVisibility(View.VISIBLE);
                        liveRadioButton.setVisibility(View.VISIBLE);
                        pauseRadio.setVisibility(View.GONE);
                        liveRadioButtonon1.setVisibility(View.GONE);
                    }
                    ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                    if (progressDialog != null) {
                        Utils.getInstance().d("not null");
                        if (!progressDialog.isShowing()) {
                            Utils.getInstance().d("not showing");
                            progressDialog.show();
                            progressDialog.setCanceledOnTouchOutside(true);
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    MediaController.getInstance().playNextSong();
                                    loadSongsDetails(MediaController.getInstance().getPlayingSongDetail());
                                }
                            }, 1000);
                        }
                    }
                }
                break;

            case R.id.next:
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    radioMusic = BufferData.getInstance().getRadioPlayer();
                    if (radioMusic != null && radioMusic.isPlaying()) {
                        radioMusic.pause();
                        radioMusic = null;
                        isRadioPlaying = false;
                        playRadio.setVisibility(View.VISIBLE);
                        liveRadioButton.setVisibility(View.VISIBLE);
                        pauseRadio.setVisibility(View.GONE);
                        liveRadioButtonon1.setVisibility(View.GONE);
                    }
                    ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                    if (progressDialog != null) {
                        Utils.getInstance().d("not null");
                        if (!progressDialog.isShowing()) {
                            Utils.getInstance().d("not showing");
                            progressDialog.show();
                            progressDialog.setCanceledOnTouchOutside(true);
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    MediaController.getInstance().playNextSong();
                                    loadSongsDetails(MediaController.getInstance().getPlayingSongDetail());
                                }
                            }, 500);
                        }
                    }
                }
                break;

            case R.id.prv:
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    radioMusic = BufferData.getInstance().getRadioPlayer();
                    if (radioMusic != null && radioMusic.isPlaying()) {
                        radioMusic.pause();
                        radioMusic = null;
                        isRadioPlaying = false;
                        playRadio.setVisibility(View.VISIBLE);
                        liveRadioButton.setVisibility(View.VISIBLE);
                        pauseRadio.setVisibility(View.GONE);
                        liveRadioButtonon1.setVisibility(View.GONE);
                    }
                    ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                    if (progressDialog != null) {
                        Utils.getInstance().d("not null");
                        if (!progressDialog.isShowing()) {
                            Utils.getInstance().d("not showing");
                            progressDialog.show();
                            progressDialog.setCanceledOnTouchOutside(true);
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    MediaController.getInstance().playPreviousSong();
                                    loadSongsDetails(MediaController.getInstance().getPlayingSongDetail());
                                }
                            }, 500);

                        }

                    }
                }
                break;

            case R.id.prv2:
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    radioMusic = BufferData.getInstance().getRadioPlayer();
                    if (radioMusic != null && radioMusic.isPlaying()) {
                        radioMusic.pause();
                        radioMusic = null;
                        isRadioPlaying = false;
                        playRadio.setVisibility(View.VISIBLE);
                        liveRadioButton.setVisibility(View.VISIBLE);
                        pauseRadio.setVisibility(View.GONE);
                        liveRadioButtonon1.setVisibility(View.GONE);
                    }
                    ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                    if (progressDialog != null) {
                        Utils.getInstance().d("not null");
                        if (!progressDialog.isShowing()) {
                            Utils.getInstance().d("not showing");
                            progressDialog.show();
                            progressDialog.setCanceledOnTouchOutside(true);
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    MediaController.getInstance().playPreviousSong();
                                    loadSongsDetails(MediaController.getInstance().getPlayingSongDetail());
                                }
                            }, 1000);
                        }
                    }
                }
                break;

            case R.id.play:
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    radioMusic = BufferData.getInstance().getRadioPlayer();
                    if (radioMusic != null && radioMusic.isPlaying()) {
                        radioMusic.pause();
                        radioMusic = null;
                        isRadioPlaying = false;
                        playRadio.setVisibility(View.VISIBLE);
                        liveRadioButton.setVisibility(View.VISIBLE);
                        pauseRadio.setVisibility(View.GONE);
                        liveRadioButtonon1.setVisibility(View.GONE);
                    }
                    PlayPauseEvent();
                } else {
                    MediaPlayer mediaPlayer = BufferData.getInstance().getRadioPlayer();
                    if (mediaPlayer != null) {
                        mediaPlayer.stop();
                        mediaPlayer.release();
                        BufferData.getInstance().setRadioPlayer(null);
                    }
                    if (CheckNetwork.isInternetAvailable(context)) {
                        new AllSongsAsyncTask(object).execute();
                    } else {
                        Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case R.id.pause2:
                break;

            case R.id.repeat:
                if (repeat.isSelected()) {
                    repeat.setSelected(!repeat.isSelected());
                    Prefs.getPrefInstance().setValue(this, Const.REPEAT, "0");

                } else {
                    repeat.setSelected(!repeat.isSelected());
                    Prefs.getPrefInstance().setValue(this, Const.REPEAT, "1");
                }
                break;

            case R.id.suffle:
                if (shuffle.isSelected()) {
                    shuffle.setSelected(!shuffle.isSelected());
                    Prefs.getPrefInstance().setValue(this, Const.SHUFFEL, "0");
                } else {
                    shuffle.setSelected(!shuffle.isSelected());
                    Prefs.getPrefInstance().setValue(this, Const.SHUFFEL, "1");
                }
                break;

            case R.id.like:
                isFragmentLoaded = true;
                setFragmentLayout();
                frag = new MyFavorites(context, radioMusic);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag, "outer");
                ft.addToBackStack("outer");
                ft.commitAllowingStateLoss();
                break;

            case R.id.share:
                isFragmentLoaded = true;
                setFragmentLayout();
                frag = new MyPlayList(context, radioMusic);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag, "outer");
                ft.addToBackStack("outer");
                ft.commitAllowingStateLoss();
                break;

            case R.id.listButton:
                isFragmentLoaded = true;
                frag = new MenuFragment(context, radioMusic, adViewContainer);
                fm = getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commitAllowingStateLoss();
                setFragmentLayout();
                break;

            case R.id.liveRadioButton:
                setRadioLayout();
                break;

            case R.id.backRadio:
                setPlayLayout();
                break;
        }
    }

    @Override
    public void didReceivedNotification(int id, Object... args) {
        if (id == NotificationManager.audioDidStarted || id == NotificationManager.audioPlayStateChanged || id == NotificationManager.audioDidReset) {
            updateTitle(id == NotificationManager.audioDidReset && (Boolean) args[1]);
        } else if (id == NotificationManager.audioProgressDidChanged) {
            PojoSongForPlayer mSongDetail = MediaController.getInstance().getPlayingSongDetail();
            updateProgress(mSongDetail);
        }
    }

    @Override
    public void newSongLoaded(Object... args) {
        songDetail = (PojoSongForPlayer) args[0];
        drawable1.start();
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        if (f instanceof MusicCategoryInner) {
            ((MusicCategoryInner) f).songLoaded(songDetail.getS_url());
        }
        if (f instanceof HitSingles) {
            ((HitSingles) f).songLoaded(songDetail.getS_url());
        }
        if (f instanceof DjFragment) {
            ((DjFragment) f).songLoaded(songDetail.getS_url());
        }
        if (f instanceof MyPlayListSongsFragment) {
            ((MyPlayListSongsFragment) f).songLoaded(songDetail.getS_url());
        }
        if (f instanceof MyFavorites) {
            ((MyFavorites) f).songLoaded(songDetail.getS_url());
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(final SeekBar seekBar) {

        if (mDialog != null) {
            if (!mDialog.isShowing()) {
                mDialog.show();
            }
        }
        int prog = seekBar.getProgress();
        MediaController.getInstance().seekToProgress(MediaController.getInstance().getPlayingSongDetail(), (float) prog / 100);
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                mDialog.dismiss();
            }
        }, 1000); // 2000 milliseconds delay
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

    }

    public void playAudio() {
        Prefs.getPrefInstance().setValue(ApplicationDMPlayer.applicationContext, Const.CHECK2, "playing");
        if (MediaController.getInstance().getPlayingSongDetail() != null) {
            if (MediaController.getInstance().isAudioPaused() == false) {
                MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                drawable1.stop();
            }
        }
        try {
            radioMusic = BufferData.getInstance().getRadioPlayer();
            if (radioMusic != null) {
                Log.d("mytag", "audioPlayer != null");
                if (radioMusic.isPlaying()) {
                    Log.d("mytag", "audioPlayer.isPlaying()");
                    radioMusic.stop();
                    Prefs.getPrefInstance().setValue(ApplicationDMPlayer.applicationContext, Const.CHECK2, "");
                    radioMusic.release();
                    radioMusic = null;
                    BufferData.getInstance().setRadioPlayer(radioMusic);

                    final ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                    if (progressDialog != null) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }
                    return;
                }
            }
            final ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
            if (progressDialog != null) {
                if (!progressDialog.isShowing()) {
                    progressDialog.setCancelable(false);
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();
                }

            }

            radioMusic = new MediaPlayer();
            radioMusic.setAudioStreamType(AudioManager.STREAM_MUSIC);
            audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
            Log.i("mytag", "Play method Url : " + radioLink);
            Intent intent = new Intent(ApplicationDMPlayer.applicationContext, MusicPlayerService.class);
            ApplicationDMPlayer.applicationContext.startService(intent);
            radioMusic.setDataSource(radioLink);
            radioMusic.prepareAsync();
            radioMusic.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    if (radioMusic != null && radioMusic.isPlaying()) {
                        radioMusic.release();
                        Prefs.getPrefInstance().setValue(ApplicationDMPlayer.applicationContext, Const.CHECK2, "");
                        radioMusic = null;
                    } else {
                        //show a message or something
                    }
                }
            });


            radioMusic.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
                }
            });
            radioMusic.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    startRadioTimer();
                    radioMusic.start();

                    if (radioMusic.isPlaying()) {
                        playRadio.setVisibility(View.GONE);
                        liveRadioButton.setVisibility(View.GONE);
                        pauseRadio.setVisibility(View.VISIBLE);
                        liveRadioButtonon1.setVisibility(View.VISIBLE);
                    } else {
                        playRadio.setVisibility(View.VISIBLE);
                        liveRadioButton.setVisibility(View.VISIBLE);
                        pauseRadio.setVisibility(View.GONE);
                        liveRadioButtonon1.setVisibility(View.GONE);
                    }
                    if (MediaController.getInstance().getPlayingSongDetail() != null) {
                        if (MediaController.getInstance().isAudioPaused() == false) {
                            MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                            drawable1.stop();
                        }
                    }
                    BufferData.getInstance().setRadioPlayer(radioMusic);
//                    Utils.getInstance().d("Player set to ");
                }
            });

        } catch (Exception e) {
            if (radioMusic != null && radioMusic.isPlaying()) {
                radioMusic.release();
                Prefs.getPrefInstance().setValue(ApplicationDMPlayer.applicationContext, Const.CHECK2, "");
                radioMusic.stop();
                radioMusic = null;
            } else {
                //show a message or something
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
        //  return
    }

    private class adsdata extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/googleads";
                OkHttpClient client = new OkHttpClient();
//                    RequestBody body = RequestBody.create(JSON, String.valueOf(object));
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (Exception e) {
                return null;
            }
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if (s == null) {
                Log.d("mytag", "Bookings Response is null");
            } else {
                {
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            String android_key = jsonObject.getString("android_key");
                            Log.d("mytag", "android key is---" + android_key);
                            AdView adView = new AdView(MainActivity.this);
                            adView.setAdUnitId(android_key);
//                            adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");
                            adView.setAdSize(AdSize.SMART_BANNER);
                            AdRequest adRequest = new AdRequest.Builder().build();
                            adView.loadAd(adRequest);
                            adViewContainer.addView(adView);
                            adView.setAdListener(new AdListener() {
                                @Override
                                public void onAdLoaded() {
                                    Log.d("MainActivity", "Ad is loaded");
                                }

                                @Override
                                public void onAdClosed() {
                                    Log.d("MainActivity", "Ad is closed");
                                }

                                @Override
                                public void onAdFailedToLoad(int errorCode) {
                                }

                                @Override
                                public void onAdLeftApplication() {
                                }

                                @Override
                                public void onAdOpened() {
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    //http://durisimomobileapps.net/djsisko/api/add_user
    private class userAsyncTask extends AsyncTask<String, Void, String> {
        JSONObject jsonObject;

        public userAsyncTask(JSONObject jsonObject) {
            this.jsonObject = jsonObject;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/add_user";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(jsonObject));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                return result;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("Register User", "Response is:" + s);
            if (s == null) {
                Log.d("Register Response is:", "null");
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    int status = jsonObject.getInt("status");
                    Log.d("MainActivity", "Status" + status);
                    String msg = jsonObject.getString("msg");
                    Log.d("MainActivity", "Msg " + msg);
                    JSONObject object = jsonObject.getJSONObject("data");
                    String user_id = object.getString("u_id");
                    Log.d("User Id is:", user_id);

                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyAndroidId", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    Log.d("mytag", user_id);
                    editor.putString("androidId", user_id);
                    editor.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //http://durisimomobileapps.net/djsisko/api/all_songs
    private class AllSongsAsyncTask extends AsyncTask<String, Void, String> {
        JSONObject object;

        private AllSongsAsyncTask(JSONObject object) {
            this.object = object;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/all_songs";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(object));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (Exception e) {
                return "Response is null";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.d("mytag", "All songs Response : " + s);
            if (!s.equals(null)) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        if (jsonArray.length() == 0) {
                            Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
                        }
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject idJson = jsonArray.getJSONObject(i);
                            int id = idJson.getInt("song_id");
                            JSONObject songNameJson = jsonArray.getJSONObject(i);
                            String song = songNameJson.getString("song_name");
                            JSONObject artistJson = jsonArray.getJSONObject(i);
                            String artist = artistJson.getString("artist_name");
                            JSONObject imageJson = jsonArray.getJSONObject(i);
                            String image = imageJson.getString("image");
                            JSONObject durationJson = jsonArray.getJSONObject(i);
                            String duration = durationJson.getString("song_time");
                            String durInMili = String.valueOf(Utils.getInstance().strToMilli(duration));
                            Log.d("Duration is", duration);
                            JSONObject songUrlJson = jsonArray.getJSONObject(i);
                            String songUrl = songUrlJson.getString("song_url");
                            Log.d("Song Url is", songUrl);
                            JSONObject likeNoJson = jsonArray.getJSONObject(i);
                            int likes = likeNoJson.getInt("like_status");
                            Log.d("like no. is", "" + likes);
                            JSONObject totalLikes = jsonArray.getJSONObject(i);
                            int total = totalLikes.getInt("total_likes");
                            Log.d("Total Likes :", "" + total);
                            allSongs.add(new PojoSongForPlayer(song, durInMili, songUrl, image, artist, id));

//                            allSongs.add(new Song(id, image, song, artist, songUrl, durInMili, likes, total, 0, 0, 0, 0, 0));
                            Log.d("MainActivity", "All songs size:" + allSongs.size());
                        }
                        if (allSongs.size() > 0) {
                            PojoSongForPlayer currentData = allSongs.get(0);
                            if (currentData != null) {
                                if (MediaController.getInstance().isPlayingAudio(currentData) && !MediaController.getInstance().isAudioPaused()) {
                                    MediaController.getInstance().pauseAudio(currentData);
                                } else {
                                    MediaController.getInstance().setPlaylist(allSongs, currentData, PhoneMediaControl.SonLoadFor.All.ordinal(), -1);
                                }
                                songDetail = currentData;
                                updateTitle(false);
                                loadSongsDetails(currentData);
                                mProgressDialog.dismiss();
                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

            }
            System.out.println(s);
        }
    }

    private class sliderData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/slider";
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (Exception e) {
                return null;
            }
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("mytag", "Slider Response : " + s);
            if (s == null) {
                Log.d("mytag", "Bookings Response is null");
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        JSONArray data = jsonObject.getJSONArray("data");

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject slider_image_data = data.optJSONObject(i);
                            String image = slider_image_data.getString("image");
                            imageArray.add(new Song(image));
                            Log.d("MainActivity", "Size is:" + imageArray.size());
                        }
                        mySliderAdapter = new MySliderAdapter(context, imageArray);
                        pager.setAdapter(mySliderAdapter);
                        startImageSliderTimer();
                        JSONArray jsonArray2 = jsonObject.getJSONArray("video");
                        for (int j = 0; j < jsonArray2.length(); j++) {
                            JSONObject video_data = jsonArray2.getJSONObject(j);
                            String name = video_data.getString("name");
                            Log.d("Video Entry", name);
                            String video_link = video_data.getString("video_link");
                            dataSlider.add(new Song(name, video_link, "", "", ""));
                        }
                        JSONArray jsonArray3 = jsonObject.getJSONArray("audio");
                        for (int k = 0; k < jsonArray3.length(); k++) {
                            JSONObject audio_data = jsonArray3.getJSONObject(k);

                            int song_id = audio_data.getInt("song_id");
                            String song_name = audio_data.getString("song_name");
                            String song_url = audio_data.getString("song_url");
                            String image = audio_data.getString("image");
                            String artists_name = audio_data.getString("artist_name");
                            String song_duration = audio_data.getString("song_time");
                            int total_likes = audio_data.getInt("total_likes");
                            String durInMili = String.valueOf(Utils.getInstance().strToMilli(song_duration));
                            songList.add(new PojoSongForPlayer(song_name, durInMili, song_url, image, artists_name, song_id));
                            Log.d("songList Details:", "Size is:" + songList.size());
                            Log.d("songList details", "Song name is:" + songList.get(0).getS_name());
                        }
                    }
                    System.out.println(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //radioLink
    //http://durisimomobileapps.net/djsisko/api/onair
    public class radioAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/onair";
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            Log.d("Dj Songs", "Response is:" + s);
            if (s == null) {
                Log.d("mytag", "Bookings Response is null");
            } else {
                try {
                    Log.d("Radio Response", s);
                    JSONObject jsonObject = new JSONObject(s);
                    JSONObject data = jsonObject.getJSONObject("data");
                    radioLink = data.getString("link");
                    Log.d("Radio Song Link", radioLink);
                    String title = data.getString("title");
                    radioTitle.setText(title);
                    Log.d("Radio Title", title);
                    String image = data.getString("onair_image");
                    ImageLoader.getInstance().displayImage(image, iv_radioBackImage);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class menulinkdata extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/weblinks";
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (Exception e) {
                return null;
            }
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if (s == null) {
                Log.d("justtag", "Bookings Response is null");
            } else {
                Log.d("justtag", "Response is " + s);
                {
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            JSONObject data = jsonObject.getJSONObject("data");
                            live_video = data.getString("live_video");
                            live_chat = data.getString("live_chat");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void getProfileDetail() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    String user_id = PrefsUtils.getFromPrefs(context, Const.USERID, "");
                    response = WebInterface.getInstance().doGet(Const.GETPROFILE + "?filter=" + user_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Utils.getInstance().d("mytag" + response);
                if (response == null) {
                    } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject data = jsonArray.getJSONObject(i);
                                String gcm_id = data.getString("gcm_id");
                                String name = data.getString("name");
                                Log.d("mytag", "name is----------" + name);
                                String contact = data.getString("contact");
                                String contact1 = contact.substring(0, 3)+"-"+contact.substring(3, 6)+"-"+contact.substring(6, 10);
//                                String formattedNumber = PhoneNumberUtils.formatNumber(contact);
                                Log.d("mytag", "name is----------" + contact1);
                                String image = data.getString("image");
                                Log.d("mytag", "image is----------" + image);
                                Prefs.getPrefInstance().setValue(context, Const.PROFILE_IMAGE, image);
                            }

                            JSONArray jsonArray1 = jsonObject.getJSONArray("image_assets");
                            for (int j = 0; j < jsonArray1.length(); j++) {
                                JSONObject data1 = jsonArray1.getJSONObject(j);
                                String display_type = data1.getString("display_type");
                                String image_type = data1.getString("image_type");
                            }

                            JSONArray jsonArray3 = jsonObject.getJSONArray("image_size");
                            for (int k = 0; k < jsonArray3.length(); k++) {
                                JSONObject data3 = jsonArray3.getJSONObject(k);
                                String height = data3.getString("height");
                                String width = data3.getString("width");
                            }

                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }
}
