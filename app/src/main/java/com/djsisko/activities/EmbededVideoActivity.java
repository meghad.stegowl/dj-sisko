package com.djsisko.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.djsisko.ApplicationDMPlayer;
import com.djsisko.R;
import com.djsisko.Remote.ApiClient;
import com.djsisko.Remote.ApiInterface;
import com.djsisko.Utility.Const;
import com.djsisko.Utility.PrefsUtils;
import com.djsisko.Utility.WebInterface;
import com.djsisko.adapter.AdapterUserComment;
import com.djsisko.adapter.AdapterViewPager;
import com.djsisko.adapter.RecyclerViewPositionHelper;
import com.djsisko.custom.CustomViewPager;
import com.djsisko.data.ChatModal;
import com.djsisko.data.ImageSliderModel;
import com.djsisko.data.MessageBoard;
import com.djsisko.data.MessageBoardData;
import com.djsisko.data.Video;
import com.djsisko.data.VideoData;
import com.djsisko.fragment.videoFromFragment;
import com.djsisko.phonemidea.CheckNetwork;
import com.djsisko.phonemidea.Prefs;
import com.djsisko.phonemidea.Utils;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

import static android.view.View.SYSTEM_UI_FLAG_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_RENDERER;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_SOURCE;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_UNEXPECTED;

public class EmbededVideoActivity extends AppCompatActivity {
    private final Object progressTimerSync = new Object();
    private final Object sync = new Object();
    PlayerView video_player;
    static Context context;
    String embedded_video_link;
    static String video_id;
    String video_like_status;
    int like_count;
    int dislike_count;
    static RecyclerView rv_message_board;
    static RecyclerView.LayoutManager layoutManager;
    static AdapterUserComment adapterUserComment;
    static ArrayList<MessageBoardData> messageBoardData = new ArrayList<>();
    ArrayList<VideoData> videoData = new ArrayList<>();
    Call<Video> call;
    String skip_user_id, skip_status,user_id;
    private static ProgressDialog mProgressDialog;
    private ExoPlayer player;
    private View rootView;
    private boolean playWhenReady = true;
    private CustomViewPager customViewPager;
    private ArrayList<ImageSliderModel> imageArraylist1;
    private AdapterViewPager adapterViewPager;
    private Timer progressTimer = null;
    private ImageView iv_like, iv_dislike;
    private TextView tv_like, tv_dislike;
    private RelativeLayout rl_share;
    private LinearLayout btn_back;
    private Button btn_send;
    private EditText edt_msg;
    public static boolean isfromEmbededVideoActivity = true;
    private static int page = 1;
    private static int totalPage;
    private static RecyclerViewPositionHelper recyclerViewPositionHelper;
    private boolean isLoading = false;
    private  RelativeLayout rl_video_view;
    private FirebaseRecyclerAdapter adapter;

    public static void runOnUIThread(Runnable runnable) {
        runOnUIThread(runnable, 0);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        if (delay == 0) {
            ApplicationDMPlayer.applicationHandler.post(runnable);
        } else {
            ApplicationDMPlayer.applicationHandler.postDelayed(runnable, delay);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_embeded_video);
        context = EmbededVideoActivity.this;
        isfromEmbededVideoActivity = true;
        videoFromFragment.isfromvideoFromFragment = false;
        LoginVideoActivity.isfromLoginVideoActivity = false;

        skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
        skip_user_id = PrefsUtils.getFromPrefs(context, Const.SKIP_USERID, "");
        user_id = PrefsUtils.getFromPrefs(context, Const.USERID, "");
        init();
        rl_video_view.setVisibility(View.VISIBLE);
        if (CheckNetwork.isInternetAvailable(context)) {
            getVideoApi();
            sliderData();
            listener();
            video_id = Prefs.getPrefInstance().getValue(context, Const.VIDEO_ID, "");
            Log.d("mytag", "video id is in video Login activity is---" + video_id);

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public static void updateChat()
    {
       runOnUIThread(() -> {
           page = 0;
           messageBoardData = new ArrayList<>();
           adapterUserComment.clear();
           getMessageDataApi(page);
       });
    }

    private void init() {
        video_player = findViewById(R.id.video_player);
        iv_like = findViewById(R.id.iv_like);
        iv_dislike = findViewById(R.id.iv_dislike);
        tv_like = findViewById(R.id.tv_like);
        tv_dislike = findViewById(R.id.tv_dislike);
        rl_share = findViewById(R.id.rl_share);
        btn_back = findViewById(R.id.btn_back);
        btn_send = findViewById(R.id.btn_send);
        edt_msg = findViewById(R.id.edt_msg);
        rv_message_board = findViewById(R.id.rv_message_board);
        customViewPager = findViewById(R.id.customViewPager);
        rl_video_view = findViewById(R.id.rl_video_view);

        layoutManager = new LinearLayoutManager(context);
        getMessages();
//        rv_message_board.setLayoutManager(layoutManager);
//        recyclerViewPositionHelper = RecyclerViewPositionHelper.createHelper(rv_message_board);
    }

    private void listener() {
//        rv_message_board.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                if (dy > 0) {
//                    LinearLayoutManager layoutManager = (LinearLayoutManager) rv_message_board.getLayoutManager();
//                    int total = layoutManager.getItemCount();
//                    int currentLastItem = recyclerViewPositionHelper.findLastCompletelyVisibleItemPosition() + 1;
//                    int firstVisibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
//                        if (page <= totalPage) {
//                            isLoading = true;
//                            page++;
//                            getMessageDataApi(page);
//                        }
//                }
//            }
//        });

        iv_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callLikeApi(video_like_status);
            }
        });

        iv_dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callDisLikeApi(video_like_status);
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                releasePlayer();
                Intent i = new Intent(context, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

        rl_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = ("Embedded Video Link : " + embedded_video_link + "\n\n" + "App Name :" + "DJ Sisko");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = edt_msg.getText().toString().trim();
                if (!TextUtils.isEmpty(msg)) {
                    if (Utils.getInstance().isConnectivity(context)) {
                        String user_id = "";
                        String path = "User";
                        String name = "";
                        String profile_pic = "";
                        String type = "user";
                        if (Prefs.getPrefInstance().getValue(EmbededVideoActivity.this, Const.SKIP_STATUS, "").equals("0")){
                            name = "User";
                            profile_pic = "http://admin.highlightmusicgroup.com/assets/admin/user.png";
                        }else {
                            name = Prefs.getPrefInstance().getValue(EmbededVideoActivity.this, Const.NAME, "");
                            if (!Prefs.getPrefInstance().getValue(EmbededVideoActivity.this, Const.PROFILE_IMAGE, "").isEmpty())
                                profile_pic = Prefs.getPrefInstance().getValue(EmbededVideoActivity.this, Const.PROFILE_IMAGE, "");
                            else profile_pic = "http://admin.highlightmusicgroup.com/assets/admin/user.png";
                        }

                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference myRef = database.getReference();
                        DatabaseReference databaseReference = myRef.child(path).push();
                        Map<String, Object> map = new HashMap<>();
                        Calendar cal = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a");
                        sdf.setTimeZone(TimeZone.getTimeZone("America/New_York"));
                        sdfTime.setTimeZone(TimeZone.getTimeZone("America/New_York"));
                        long time = System.currentTimeMillis();
                        String date = sdf.format(time);
                        String currentTime = sdfTime.format(time);
                        map.put("comment", edt_msg.getText().toString());
                        map.put("commentByName", name);
                        map.put("commentDate", date);
                        map.put("commentImageURL", profile_pic);
                        map.put("time", currentTime);
                        map.put("type", type);
                        databaseReference.setValue(map);
                        edt_msg.getText().clear();
                    } else {
                        Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Please Enter Some Text", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getMessages() {
        edt_msg.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                ((InputMethodManager) Objects.requireNonNull(getSystemService(Activity.INPUT_METHOD_SERVICE))).hideSoftInputFromWindow(v.getWindowToken(), 0);
                btn_send.performClick();
                return true;
            }
            return false;
        });
        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference();
        String path = "User";

        Query query = myRef.child(path);

        FirebaseRecyclerOptions<ChatModal> options = new FirebaseRecyclerOptions.Builder<ChatModal>()
                .setQuery(query, snapshot -> {
                    String time = "", date = "", image = "", name = "", comment = "", type = "";
                    if (snapshot.hasChild("time") && snapshot.child("time").getValue() != null)
                        time = snapshot.child("time").getValue().toString();
                    if (snapshot.hasChild("commentDate") && snapshot.child("commentDate").getValue() != null)
                        date = snapshot.child("commentDate").getValue().toString();
                    if (snapshot.hasChild("commentImageURL") && snapshot.child("commentImageURL").getValue() != null)
                        image = snapshot.child("commentImageURL").getValue().toString();
                    if (snapshot.hasChild("commentByName") && snapshot.child("commentByName").getValue() != null)
                        name = snapshot.child("commentByName").getValue().toString();
                    if (snapshot.hasChild("comment") && snapshot.child("comment").getValue() != null)
                        comment = snapshot.child("comment").getValue().toString();
                    if (snapshot.hasChild("type") && snapshot.child("type").getValue() != null)
                        type = snapshot.child("type").getValue().toString();
                    return new ChatModal(time, date, image, name, comment, type);
                })
                .build();

        adapter = new FirebaseRecyclerAdapter<ChatModal, ViewHolder>(options) {
            @NotNull
            @Override
            public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_comment, parent, false));
            }

            @Override
            protected void onBindViewHolder(@NotNull ViewHolder holder, final int position, @NotNull ChatModal model) {
                Log.d("mytag1", "name comment - "+ model.getCommentByName());
                holder.tv_title.setText(model.getComment());
                holder.tv_user_name.setText(model.getCommentByName());
                holder.tv_time.setText(model.getTime());

                Glide
                        .with(ApplicationDMPlayer.applicationContext)
                        .load(model.getImage())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.25f)
                        .error(R.drawable.user)
                        .placeholder(R.drawable.user)
                        .into(holder.iv_image);
            }
        };
        rv_message_board.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv_message_board.setLayoutManager(linearLayoutManager);
        rv_message_board.setAdapter(adapter);
        adapter.startListening();
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                rv_message_board.postDelayed(() -> rv_message_board.scrollToPosition(adapter.getItemCount() - 1), 1000);
            }

            @Override
            public void onCancelled(@NotNull DatabaseError error) {
                // Failed to read value
                Log.w("mytag", "Failed to read value.", error.toException());
            }
        });
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title;
        TextView tv_user_name;
        TextView tv_time;
        CircleImageView iv_image;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_user_name = itemView.findViewById(R.id.tv_user_name);
            tv_time = itemView.findViewById(R.id.tv_time);
            iv_image = itemView.findViewById(R.id.iv_image);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initializePlayer() {
        rl_video_view.setVisibility(View.GONE);
        player = ExoPlayerFactory.newSimpleInstance(context);
        video_player.setPlayer(player);
        video_player.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        video_player.setUseController(false);
        player.setPlayWhenReady(true);
        player.seekTo(0);

        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setUsage(C.USAGE_MEDIA)
                .setContentType(C.CONTENT_TYPE_MOVIE)
                .build();
        if (player.getAudioComponent() != null)
            player.getAudioComponent().setAudioAttributes(audioAttributes, true);
//        embedded_video_link = "http:\\/\\/techslides.com\\/demos\\/sample-videos\\/small.mp4";

        embedded_video_link = Prefs.getPrefInstance().getValue(context, Const.VIDEO_LINK, "");
        try {
            if (!embedded_video_link.equals("")) {
                Uri uri = Uri.parse(embedded_video_link);
                MediaSource mediaSource = buildMediaSource(uri, null);
                player.prepare(mediaSource, true, true);
            }
        } catch (Exception e) {

        }
        player.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {
                    case Player.STATE_READY:
                        if (playWhenReady) {
//                            callVideoWatchAPI();
                        }
                        break;
                    case Player.STATE_ENDED:
                        break;
                }
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                rl_video_view.setVisibility(View.VISIBLE);
                switch (error.type) {
                    case TYPE_SOURCE:
                        try {
                            Snackbar snackbar = Snackbar.make(rootView, "This video can't be played right now. Please try again later.", Snackbar.LENGTH_SHORT);
                            snackbar.show();
                        } catch (Exception e) {

                        }
                        break;
                    case TYPE_RENDERER:
                        player.retry();
                        break;
                    case TYPE_UNEXPECTED:
                        player.retry();
                        break;
                }
            }
        });
    }

    private MediaSource buildMediaSource(Uri uri, @Nullable String overrideExtension) {
        @C.ContentType int type = Util.inferContentType(uri, overrideExtension);
        switch (type) {
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(new DefaultDataSourceFactory(context, new DefaultHttpDataSourceFactory(Util.getUserAgent(context, context.getResources().getString(R.string.app_name)))))
                        .createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(new DefaultDataSourceFactory(context, new DefaultHttpDataSourceFactory(Util.getUserAgent(context, context.getResources().getString(R.string.app_name)))))
                        .createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(new DefaultDataSourceFactory(context, new DefaultHttpDataSourceFactory(Util.getUserAgent(context, context.getResources().getString(R.string.app_name)))))
                        .createMediaSource(uri);
            case C.TYPE_OTHER:
                DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory()
                        .setConstantBitrateSeekingEnabled(true);
                return new ExtractorMediaSource.Factory(new DefaultHttpDataSourceFactory(Util.getUserAgent(context, context.getResources().getString(R.string.app_name))))
                        .setExtractorsFactory(extractorsFactory)
                        .createMediaSource(uri);
            default: {
                throw new IllegalStateException("Unsupported type: " + type);
            }
        }
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean("playWhenReady", playWhenReady);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((Activity) context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Util.SDK_INT > 23) {
            if (video_player != null) {
                video_player.onResume();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((Activity) context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        hideSystemUi();
        if ((Util.SDK_INT <= 23 || player == null)) {
            if (video_player != null) {
                video_player.onResume();
            }
        }
        if (adapter != null) {
            adapter.startListening();
        }
    }

    @Override
    protected void onStop() {
        if (adapter != null) {
            adapter.stopListening();
        }
        super.onStop();
    }

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onDestroy() {
        ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onDestroy();
    }

    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            video_player.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | SYSTEM_UI_FLAG_FULLSCREEN
                    | SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        else
            video_player.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
    }



    @SuppressLint("StaticFieldLeak")
    private void callLikeApi(String like_status) {
        if (Utils.getInstance().isConnectivity(context)) {

            new AsyncTask<Void, Void, String>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    mProgressDialog = new ProgressDialog(context);
                    mProgressDialog.setMessage("Loading");
                    mProgressDialog.setCanceledOnTouchOutside(false);
                    mProgressDialog.setIndeterminate(true);
                    mProgressDialog.setCancelable(true);
                    mProgressDialog.show();
                }

                @Override
                protected String doInBackground(Void... voids) {
                    String response = null;
                    JSONObject request = new JSONObject();
                    try {
                        String user_id = PrefsUtils.getFromPrefs(context, Const.USERID, "");

                        skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
                        if (skip_status.equals("0")) {
                            request.put("user_id", skip_user_id);
                        } else {
                            request.put("user_id", user_id);
                        }
                        request.put("embedded_video_id", video_id);
                        request.put("video_like_status", "1");
                        response = WebInterface.getInstance().doPostRequest(Const.VIDEO_lIKE, request.toString());
                    } catch (Exception e) {
                    }
                    Utils.getInstance().d("Like Video REsponse : " + response);
                    return response;
                }

                @Override
                protected void onPostExecute(String response) {
                    super.onPostExecute(response);
                    mProgressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                            String user_id = jsonObject.getString("user_id");
                            int video_id = jsonObject.getInt("embedded_video_id");
                            int like_status = jsonObject.getInt("like_status");
                            int unlike_status = jsonObject.getInt("unlike_status");
                            int like_count = jsonObject.getInt("like_count");
                            int unlike_count = jsonObject.getInt("unlike_count");
                            if(like_status ==0){
                                iv_like.setImageResource(R.drawable.video_like);
                            }else{
                                iv_like.setImageResource(R.drawable.like_red);
                            }
                            iv_dislike.setImageResource(R.drawable.video_dislike);
                            tv_like.setText(String.valueOf(like_count));
                            tv_dislike.setText(String.valueOf(unlike_count));
                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }.execute();
        } else {
            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void callDisLikeApi(String like_status) {
        if (Utils.getInstance().isConnectivity(context)) {

            new AsyncTask<Void, Void, String>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    mProgressDialog = new ProgressDialog(context);
                    mProgressDialog.setMessage("Loading");
                    mProgressDialog.setCanceledOnTouchOutside(false);
                    mProgressDialog.setIndeterminate(true);
                    mProgressDialog.setCancelable(true);
                    mProgressDialog.show();
                }

                @Override
                protected String doInBackground(Void... voids) {
                    String response = null;
                    JSONObject request = new JSONObject();
                    try {
                        String user_id = PrefsUtils.getFromPrefs(context, Const.USERID, "");
                        skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
                        if (skip_status.equals("0")) {
                            request.put("user_id", skip_user_id);
                        } else {
                            request.put("user_id", user_id);
                        }
                        request.put("embedded_video_id", video_id);
                        request.put("video_like_status", "0");
                        response = WebInterface.getInstance().doPostRequest(Const.VIDEO_lIKE, request.toString());
                    } catch (Exception e) {
                    }
                    Utils.getInstance().d("Like Video REsponse : " + response);
                    return response;
                }

                @Override
                protected void onPostExecute(String response) {
                    super.onPostExecute(response);
                    mProgressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                            String user_id = jsonObject.getString("user_id");
                            int video_id = jsonObject.getInt("embedded_video_id");
                            int like_status = jsonObject.getInt("like_status");
                            int unlike_status = jsonObject.getInt("unlike_status");
                            int like_count = jsonObject.getInt("like_count");
                            int unlike_count = jsonObject.getInt("unlike_count");
                            if(unlike_status ==0){
                                iv_dislike.setImageResource(R.drawable.video_dislike);
                            }else{
                                iv_dislike.setImageResource(R.drawable.dislike_red);
                            }
                            iv_like.setImageResource(R.drawable.video_like);
                            tv_like.setText(String.valueOf(like_count));
                            tv_dislike.setText(String.valueOf(unlike_count));
                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }.execute();
        } else {
            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
        }
    }

    private void startImageSliderTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {

                }
            }
            progressTimer = new Timer();
            progressTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (sync) {
                        runOnUIThread(new Runnable() {
                            @Override
                            public void run() {

                                int currentItem = customViewPager.getCurrentItem() + 1;
                                int totalItem = adapterViewPager.getCount();
                                if (currentItem == totalItem) {
                                    customViewPager.setCurrentItem(0);
                                } else {
                                    customViewPager.setCurrentItem(currentItem);
                                }
                            }
                        });
                    }
                }
            }, 0, 4000);
        }
    }

    private void getVideoApi() {
        if (CheckNetwork.isInternetAvailable(ApplicationDMPlayer.applicationContext)) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("loading");
            mProgressDialog.show();
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
            if (skip_status.equals("0")) {
                skip_user_id = PrefsUtils.getFromPrefs(context, Const.SKIP_USERID, "");
                call = apiInterface.GetVideoApi(skip_user_id);
            } else {
                call = apiInterface.GetVideoApi(user_id);
            }
            call.enqueue(new Callback<Video>() {

                @Override
                public void onResponse(Call<Video> call, retrofit2.Response<Video> response) {
                    if (response.isSuccessful() && response != null) {
                        if (response.body().getStatus() == 1) {
                            videoData = response.body().getData();
                            video_id = videoData.get(0).getEmbeddedVideoId();
                            Prefs.getPrefInstance().setValue(context, Const.VIDEO_ID,video_id);
                            String embedded_video_name = videoData.get(0).getEmbeddedVideoName();
//                            embedded_video_link = videoData.get(0).getEmbeddedVideoLink();
                            embedded_video_link = "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8";
                            Prefs.getPrefInstance().setValue(context, Const.VIDEO_LINK, embedded_video_link);
                            video_like_status = videoData.get(0).getLikeStatus();

                            if (Utils.getInstance().isConnectivity(context)) {
                                initializePlayer();
                            } else {
                                Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                            }

                            adapterUserComment = new AdapterUserComment(context, messageBoardData);
//                            rv_message_board.setAdapter(adapterUserComment);
                            adapterUserComment.notifyDataSetChanged();
                            getMessageDataApi(page);

                            if (video_like_status.equals("1")) {
                                iv_like.setImageResource(R.drawable.like_red);
                            } else if (video_like_status.equals("0")) {
                                iv_dislike.setImageResource(R.drawable.dislike_red);
                            }

                            like_count = videoData.get(0).getLikeCount();
                            dislike_count = videoData.get(0).getDislikeCount();
                            tv_like.setText((String.valueOf(like_count)));
                            tv_dislike.setText((String.valueOf(dislike_count)));
                        } else {
                            mProgressDialog.dismiss();
                        }
                    } else {
                        mProgressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Video> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private static void getMessageDataApi(int page) {
        if (CheckNetwork.isInternetAvailable(ApplicationDMPlayer.applicationContext)) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<MessageBoard> call = apiInterface.GetMessageFromApi(page,video_id);
            call.enqueue(new Callback<MessageBoard>() {
                @Override
                public void onResponse(Call<MessageBoard> call, retrofit2.Response<MessageBoard> response) {
                    if (response.isSuccessful() && response != null) {
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        try {
                            messageBoardData = new ArrayList<>();
                            messageBoardData.addAll(response.body().getData());
                        }catch (Exception e){

                        }
                        if (status == 1) {
                            adapterUserComment.add(response.body().getData());
                            totalPage = response.body().getLastPage();
                        } else {
                            totalPage = 0;
                        }
                    } else {
                    }
                }

                @Override
                public void onFailure(Call<MessageBoard> call, Throwable t) {
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void sendMessage(String user_comment) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    String user_id = PrefsUtils.getFromPrefs(context, Const.USERID, "");
                    JSONObject jsonObject = new JSONObject();
                    skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
                    if (skip_status.equals("0")) {
                        jsonObject.put("user_id", skip_user_id);
                    } else {
                        jsonObject.put("user_id", user_id);
                    }
                    jsonObject.put("video_id", video_id);
                    jsonObject.put("user_comment", user_comment);
                    Utils.getInstance().d("mytag put is -------" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.ADDCOMMENT, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Utils.getInstance().d("mytag" + response);
                mProgressDialog.dismiss();
                if (response == null) {
                    Toast.makeText(context, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                            String user_id = jsonObject.getString("user_id");
                            String video_id = jsonObject.getString("video_id");
                            String user_comment = jsonObject.getString("user_comment");
                            edt_msg.getText().clear();
                            page = 0;
                            messageBoardData = new ArrayList<>();
                            adapterUserComment.clear();
                            getMessageDataApi(page);
//                            adapterUserComment = new AdapterUserComment(context, messageBoardData);
//                            rv_message_board.setAdapter(adapterUserComment);
//                            adapterUserComment.notifyDataSetChanged();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void sliderData() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    response = WebInterface.getInstance().doGet(Const.SLIDER);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Utils.getInstance().d("mytag" + response);
                mProgressDialog.dismiss();
                if (response == null) {
                    Toast.makeText(context, "Response null", Toast.LENGTH_LONG).show();
                    Log.d("Slider Response is:", "null");
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            JSONArray data = jsonObject.getJSONArray("data");

                            imageArraylist1 = new ArrayList<>();
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject slider_image_data = data.optJSONObject(i);
                                String sponsor_slide_name = slider_image_data.getString("sponsor_slide_name");
                                String sponsor_slider_image = slider_image_data.getString("sponsor_slider_image");
                                String sponsor_slider_link = slider_image_data.getString("sponsor_slider_link");
                                imageArraylist1.add(new ImageSliderModel(sponsor_slide_name, sponsor_slider_image, sponsor_slider_link));
                                Log.d("mainActivity", "Size is:" + sponsor_slider_image);
                            }
                        }
                        adapterViewPager = new AdapterViewPager(imageArraylist1, context);
                        customViewPager.setAdapter(adapterViewPager);
                        startImageSliderTimer();
                        System.out.println(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}
