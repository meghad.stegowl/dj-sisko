package com.djsisko.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.djsisko.R;
import com.djsisko.Utility.WebInterface;
import com.djsisko.phonemidea.CheckNetwork;
import com.djsisko.Utility.Const;
import com.djsisko.phonemidea.Utils;

import org.json.JSONObject;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener  {

    private EditText edt_email;
    private String email;
    private Button btn_login;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    ProgressDialog mProgressDialog;
    Context context;
    LinearLayout btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        context = ForgotPasswordActivity.this;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        initViews();
        listners();

    }

    private void initViews() {
        edt_email = findViewById(R.id.edt_email);
        btn_login = findViewById(R.id.btn_login);
        btn_back = findViewById(R.id.btn_back);
    }

    private void listners() {

        edt_email.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        btn_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                validateData();
                break;

            case R.id.btn_back:
                Intent registerIntent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                startActivity(registerIntent);
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent=new Intent(ForgotPasswordActivity.this,LoginActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.exit_anim,R.anim.entry_anim);
        finish();

    }

    private void validateData() {

        email = edt_email.getText().toString().trim();

        if (edt_email.getText().toString().length() != 0) {
            if (edt_email.getText().toString().matches(emailPattern)) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    forgotPassword();
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            } else {
                edt_email.setError("Please Enter Valid Email Id");
                edt_email.requestFocus();
            }

        } else {
            edt_email.setError("Please Enter Email Id");
            edt_email.requestFocus();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void forgotPassword() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(ForgotPasswordActivity.this);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("email", email);
                    response = WebInterface.getInstance().doPostRequest(Const.FORGOT_PASSWORD, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Utils.getInstance().d("mytag" + response);
                mProgressDialog.dismiss();
                if (response == null) {
                    Toast.makeText(ForgotPasswordActivity.this, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String msg = jsonObject.getString("message");
                        Log.d("Bookings", "status is:" + status);
                        if (status == 1) {
                            clearAllEditText();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                            Intent registerIntent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                            startActivity(registerIntent);
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

    private void clearAllEditText() {
        edt_email.requestFocus();
        edt_email.getText().clear();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onDestroy() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        super.onDestroy();
    }


}
