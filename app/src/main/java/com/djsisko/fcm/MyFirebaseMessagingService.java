package com.djsisko.fcm;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;

import com.djsisko.R;
import com.djsisko.activities.EmbededVideoActivity;
import com.djsisko.activities.LoginVideoActivity;
import com.djsisko.activities.MainActivity;
import com.djsisko.activities.SplashScreenActivity;
import com.djsisko.fragment.videoFromFragment;
import com.djsisko.phonemidea.Utils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import static com.djsisko.fragment.videoFromFragment.isfromvideoFromFragment;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "mytag";
    String notiType;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData().size() > 0) {
            Utils.getInstance().d("data" + remoteMessage.getData());
            showNotification(remoteMessage.getData().get("body"));

            if (remoteMessage.getData().size() > 0) {
                Log.d("mytag", "\ndata - " + remoteMessage.getData() + "\nnotitype- " +
                        remoteMessage.getData().get("notiType")
                        + "\nbody - " + remoteMessage.getData().get("body"));
                String notiType = remoteMessage.getData().get("notiType");
                Log.d("mytag", "notitype is----" + notiType);
                String message = remoteMessage.getData().get("body");
                Log.d("mytag", "message is----" + message);
                if (notiType != null && !notiType.equals("0")) {
                    if (EmbededVideoActivity.isfromEmbededVideoActivity) {
                        Log.d("mytag", "boolean1 is----" + notiType);
                        Log.d("mytag", "in if is----" + EmbededVideoActivity.isfromEmbededVideoActivity);
                        EmbededVideoActivity.updateChat();
                    } else if (LoginVideoActivity.isfromLoginVideoActivity) {
                        Log.d("mytag", "boolean2 is----" + LoginVideoActivity.isfromLoginVideoActivity);
                        Log.d("mytag", "in if else is----" + notiType);
                        LoginVideoActivity.updateChat();
                    } else {
                        Log.d("mytag", "boolean3 is----" + isfromvideoFromFragment);
                        Log.d("mytag", "else is----" + notiType);
                        Fragment fragment = MainActivity.fm.findFragmentById(R.id.container);
                        if (fragment instanceof videoFromFragment) {
                            videoFromFragment.updateChat();
                        }
                    }
                }
                showNotification(message);
            }
        }
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            showNotification(remoteMessage.getNotification().getBody());
        }
    }

    @SuppressLint("WrongConstant")
    private void showNotification(String message) {
        NotificationCompat.Builder mBuilder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String CHANNEL_ID = getString(R.string.app_name);// The id of the channel.
            CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setShowBadge(true);
            mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setChannelId(CHANNEL_ID);
            mBuilder.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);
            mBuilder.setNumber(10);

            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        } else {
            mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);
        }
        Intent resultIntent = new Intent(getApplicationContext(), SplashScreenActivity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        resultIntent.setFlags(Notification.FLAG_AUTO_CANCEL);
        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, 0);
        mBuilder.setSmallIcon(R.drawable.barkologo);
        mBuilder.setContentTitle("DJ Sisko");
        mBuilder.setContentText(message);
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.big1logo);
        mBuilder.setLargeIcon(icon);
        mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        mBuilder.setAutoCancel(true);
        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            mBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        }
        mBuilder.setContentIntent(intent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());

    }


}
