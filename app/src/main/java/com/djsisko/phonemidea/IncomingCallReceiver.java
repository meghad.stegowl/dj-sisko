package com.djsisko.phonemidea;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;

import com.djsisko.ApplicationDMPlayer;
import com.djsisko.Utility.Const;
import com.djsisko.activities.MainActivity;
import com.djsisko.data.BufferData;
import com.djsisko.manager.MediaController;

import static com.djsisko.activities.MainActivity.liveRadioButton;
import static com.djsisko.activities.MainActivity.liveRadioButtonon1;
import static com.djsisko.activities.MainActivity.pauseRadio;
import static com.djsisko.activities.MainActivity.playRadio;


public class IncomingCallReceiver extends BroadcastReceiver {

    public String check = "";
    public int isPlaying = 0, isRadioPlaying = 0;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (TelephonyManager.EXTRA_STATE_RINGING.equals(intent.getStringExtra(TelephonyManager.EXTRA_STATE))) {
            Utils.getInstance().d("call ringing");
            if (MediaController.getInstance().getPlayingSongDetail() != null) {
                if (MediaController.getInstance().isAudioPaused()) {
                    Log.d("mytag","already paused");
                    check = "paused";
                    Prefs.getPrefInstance().setValue(ApplicationDMPlayer.applicationContext, Const.CHECK3,"paused");
                    Log.d("mytag","check is :: "+Prefs.getPrefInstance().getValue(ApplicationDMPlayer.applicationContext, Const.CHECK3,""));

                }
                else{
                    Log.d("mytag","already playing");
                    MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                }
            }
            if(MainActivity.radioMusic!= null)
            {
                Log.d("mtag", " here out ring - " + isRadioPlaying);
                if(MainActivity.radioMusic.isPlaying()) {
                    Log.d("mtag","already playing");
                    isRadioPlaying = 1;
                    Log.d("mtag", " here in ring - " + isRadioPlaying);
                    MainActivity.radioMusic.pause();
                    playRadio.setVisibility(View.VISIBLE);
                    liveRadioButton.setVisibility(View.VISIBLE);
                    pauseRadio.setVisibility(View.GONE);
                    liveRadioButtonon1.setVisibility(View.GONE);
                } else {
                    Log.d("mtag","already paused");
                    check = "paused";
                    Prefs.getPrefInstance().setValue(ApplicationDMPlayer.applicationContext, Const.CHECK,"paused");
                }
            }
            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
//            Toast.makeText(context, incomingNumber, Toast.LENGTH_LONG).show();
        }
        if (TelephonyManager.EXTRA_STATE_IDLE.equals(intent.getStringExtra(TelephonyManager.EXTRA_STATE))) {
            Utils.getInstance().d("call idle");
//            if (MediaController.getInstance().getPlayingSongDetail() != null) {
                if (Prefs.getPrefInstance().getValue(ApplicationDMPlayer.applicationContext, Const.CHECK2,"").equals("playing")) {
                    String checkplay = Prefs.getPrefInstance().getValue(ApplicationDMPlayer.applicationContext, Const.CHECK,"");
                    Log.d("mtag","check is :: "+Prefs.getPrefInstance().getValue(ApplicationDMPlayer.applicationContext, Const.CHECK,""));
                    if(checkplay.equals("paused")){
                        Prefs.getPrefInstance().setValue(ApplicationDMPlayer.applicationContext,Const.CHECK,"");
                        Log.d("mtag","check is :: "+Prefs.getPrefInstance().getValue(ApplicationDMPlayer.applicationContext, Const.CHECK,""));
                    }else{
                        MainActivity.getInstance().playAudio();

////                    MsgData.getInstance().setIsPlaying("1");
//                    MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                    }
                } else {
                    if (MediaController.getInstance().getPlayingSongDetail() != null) {
                        if (MediaController.getInstance().isAudioPaused()) {
                            String checkplay = Prefs.getPrefInstance().getValue(ApplicationDMPlayer.applicationContext, Const.CHECK3,"");
                            Log.d("mytag","check is :: "+Prefs.getPrefInstance().getValue(ApplicationDMPlayer.applicationContext, Const.CHECK3,""));
                            if(checkplay.equals("paused")){
                                Prefs.getPrefInstance().setValue(ApplicationDMPlayer.applicationContext,Const.CHECK3,"");
                                Log.d("mytag","check is :: "+Prefs.getPrefInstance().getValue(ApplicationDMPlayer.applicationContext, Const.CHECK3,""));
                            }else{
//                    MsgData.getInstance().setIsPlaying("1");
                                MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                            }
                        }
                    }

                    MediaPlayer audioplayer = BufferData.getInstance().getRadioPlayer();
                    if (audioplayer != null)
                    {
                        if (audioplayer.isPlaying())
                        {
                            audioplayer.start();
//                    audioplayer.stop();
//                    audioplayer.release();
//                    audioplayer = null;
                            BufferData.getInstance().setRadioPlayer(audioplayer);
                        }
                    }
                }
//            }
            Log.d("mtag", " here out idle - " + isRadioPlaying);

            if(isRadioPlaying == 1){
                isRadioPlaying = 0;
                Log.d("mtag", " here in idle - " + isRadioPlaying);
            }
//            MediaPlayer audioplayer = BufferData.getInstance().getRadioPlayer();
//            if (audioplayer != null)
//            {
//                if (audioplayer.isPlaying())
//                {
//                    audioplayer.start();
////                    audioplayer.stop();
////                    audioplayer.release();
////                    audioplayer = null;
//                    BufferData.getInstance().setRadioPlayer(audioplayer);
//                }
//            }

        }


    }
}
