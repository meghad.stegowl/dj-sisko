package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.adapter.DjDataAdapter;
import com.djsisko.data.Song;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class GuestDjs extends Fragment {

    Context context;
    ArrayList<Song> data = new ArrayList<>();
    String title;
    RecyclerView featuredDjRecycler;
    RecyclerView.LayoutManager layoutManager;
    DjDataAdapter adapter;
    ProgressDialog mProgressDialog;
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    ImageView backButton1,playerButton1;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    String tag = GuestDjs.class.getSimpleName();
    MediaPlayer radioPlayer;

    public GuestDjs() {
        // Required empty public constructor
    }
    @SuppressLint("ValidFragment")
    public GuestDjs(Context context,MediaPlayer radioPlayer)
    {
        this.context = context;
        this.radioPlayer = radioPlayer;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_featured_djs, container, false);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        preferences = context.getSharedPreferences("MyAndroidId",Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId","");
        isConnected = preferences.getBoolean("isConnected",false);
        Log.d("FeaturedDj","Android id: "+androidId);
        Log.d("FeaturedDj","Net is connected: "+isConnected);
        featuredDjRecycler = (RecyclerView)v.findViewById(R.id.featuredDjRecycler);
        layoutManager = new LinearLayoutManager(context);
        featuredDjRecycler.setLayoutManager(layoutManager);
        if (CheckNetwork.isInternetAvailable(context))
        {
            data.clear();
            new DjsAsyncTask().execute();
        }
         else
        {
            Toast.makeText(context,"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new MenuFragment(context);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        return v;
    }

//    http://durisimomobileapps.net/djsisko/api/djs
    private class DjsAsyncTask extends AsyncTask<String,Void,String>
    {
        public DjsAsyncTask()
        {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }


        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/djs";
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            Log.d("Featured Dj","Response"+s);
            if (s == null) {
                Log.d("mytag","Bookings Response is null");
            }
            else {
                try {

                    JSONObject jsonObject = new JSONObject(s);
                    int status = jsonObject.getInt("status");
                    if(status == 0){
                        Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
            if(jsonArray.length() == 0){
                Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
            }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject featuredIdJson = jsonArray.getJSONObject(i);
                        int featuredId = featuredIdJson.getInt("feature_dj_id");
                        Log.d("Featured Dj", "id is" + featuredId);
                        JSONObject nameJson = jsonArray.getJSONObject(i);
                        String name = nameJson.getString("name");
                        Log.d("Featured Dj", "name is" + name);
                        JSONObject imageJson = jsonArray.getJSONObject(i);
                        String image = imageJson.getString("image");
                        Log.d("Featured Dj", "image is" + image);
                        JSONObject instaJson = jsonArray.getJSONObject(i);
                        String instaUrl = instaJson.getString("insta_url");
                        Log.d("Featured Dj", "insta Url is" + instaUrl);
                        data.add(new Song(featuredId, name, image, instaUrl));
                    }
                    try {
                        adapter = new DjDataAdapter(context, data,tag,radioPlayer);
                        featuredDjRecycler.setAdapter(adapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
