package com.djsisko.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.djsisko.R;
import com.djsisko.Utility.Const;
import com.djsisko.Utility.PrefsUtils;
import com.djsisko.Utility.WebInterface;
import com.djsisko.activities.LoginActivity;
import com.djsisko.activities.MainActivity;
import com.djsisko.phonemidea.CheckNetwork;
import com.djsisko.phonemidea.Utils;

import org.json.JSONObject;

public class ChangePasswordFragment extends Fragment {

    String user_id;
    Button btn_confirm;
    LinearLayout btn_back;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    LinearLayout adView;
    private Context context;
    private ProgressDialog mProgressDialog;
    private String current_pwd, new_pwd, confirm_pwd;
    private EditText edt_current_pwd, edt_new_pwd, edt_confirm_pwd;
    private TextView tv_confirm;
    private String new_password, current_password;
    private ImageView iv_current_pwdd, iv_new_pwd, iv_confirm_pwd;
    private ImageView iv_video, iv_home;

    @SuppressLint("ValidFragment")
    public ChangePasswordFragment(Context context, LinearLayout adView) {
        this.context = context;
        this.adView = adView;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_change_password, container, false);

        initViews(v);
        listners();

        return v;
    }


    private void initViews(View rootview) {
        edt_current_pwd = rootview.findViewById(R.id.edt_current_pwd);
        edt_new_pwd = rootview.findViewById(R.id.edt_new_pwd);
        edt_confirm_pwd = rootview.findViewById(R.id.edt_confirm_pwd);
        tv_confirm = rootview.findViewById(R.id.tv_confirm);
        btn_confirm = rootview.findViewById(R.id.btn_confirm);
        btn_back = rootview.findViewById(R.id.btn_back);
        iv_current_pwdd = rootview.findViewById(R.id.iv_current_pwdd);
        iv_new_pwd = rootview.findViewById(R.id.iv_new_pwd);
        iv_confirm_pwd = rootview.findViewById(R.id.iv_confirm_pwd);
        iv_video = rootview.findViewById(R.id.iv_video);
        iv_home = rootview.findViewById(R.id.iv_home);
    }

    private void listners() {

        iv_current_pwdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iv_current_pwdd.isSelected()) {
                    iv_current_pwdd.setSelected(false);
                    Log.d("mytag", "in case 1");
                    edt_current_pwd.setInputType(InputType.TYPE_CLASS_TEXT);
                    edt_current_pwd.setTextSize(19);
                    Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/SourceSansPro-Regular.ttf");
                    edt_current_pwd.setTypeface(type);
                } else {
                    Log.d("mytag", "in case 2");
                    iv_current_pwdd.setSelected(true);
                    edt_current_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_current_pwd.setTextSize(19);
                    Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/SourceSansPro-Regular.ttf");
                    edt_current_pwd.setTypeface(type);
                }
            }
        });

        iv_new_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iv_new_pwd.isSelected()) {
                    iv_new_pwd.setSelected(false);
                    Log.d("mytag", "in case 1");
                    edt_new_pwd.setInputType(InputType.TYPE_CLASS_TEXT);
                    edt_new_pwd.setTextSize(19);
                    Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/SourceSansPro-Regular.ttf");
                    edt_new_pwd.setTypeface(type);
                } else {
                    Log.d("mytag", "in case 2");
                    iv_new_pwd.setSelected(true);
                    edt_new_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_new_pwd.setTextSize(19);
                    Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/SourceSansPro-Regular.ttf");
                    edt_new_pwd.setTypeface(type);
                }
            }
        });

        iv_confirm_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iv_confirm_pwd.isSelected()) {
                    iv_confirm_pwd.setSelected(false);
                    Log.d("mytag", "in case 1");
                    edt_confirm_pwd.setInputType(InputType.TYPE_CLASS_TEXT);
                    edt_confirm_pwd.setTextSize(19);
                    Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/SourceSansPro-Regular.ttf");
                    edt_confirm_pwd.setTypeface(type);
                } else {
                    Log.d("mytag", "in case 2");
                    iv_confirm_pwd.setSelected(true);
                    edt_confirm_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_confirm_pwd.setTextSize(19);
                    Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/SourceSansPro-Regular.ttf");
                    edt_confirm_pwd.setTypeface(type);
                }
            }
        });

        iv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.setPlayLayout();
            }
        });

        iv_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag = new videoFromFragment(context);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateData();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag = new EditProfileFragment(context, adView);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

    }

    private void validateData() {
        current_pwd = edt_current_pwd.getText().toString().trim();
        new_pwd = edt_new_pwd.getText().toString().trim();
        confirm_pwd = edt_confirm_pwd.getText().toString().trim();
        if (!current_pwd.isEmpty() && !new_pwd.isEmpty() && !confirm_pwd.isEmpty()) {
            if (CheckNetwork.isInternetAvailable(context)) {
                if (edt_new_pwd.getText().toString().equals(edt_confirm_pwd.getText().toString())) {
                    if (CheckNetwork.isInternetAvailable(context)) {
                        changePassword(current_pwd, new_pwd, confirm_pwd);
                    } else {
                        Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("Password do not match");
                    builder1.setCancelable(true);

                    builder1.setNegativeButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            } else {
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (current_pwd.isEmpty()) {
                edt_current_pwd.setError("Please Enter Current Password");
                edt_current_pwd.requestFocus();
            } else if (new_pwd.isEmpty()) {
                edt_new_pwd.setError("Please Enter New Password");
                edt_new_pwd.requestFocus();
            } else if (confirm_pwd.isEmpty()) {
                edt_confirm_pwd.setError("Please Enter Confirm Password");
                edt_confirm_pwd.requestFocus();
            }
        }

    }

    private void clearAllEdditText() {
        edt_current_pwd.requestFocus();
        edt_current_pwd.getText().clear();
        edt_new_pwd.getText().clear();
        edt_confirm_pwd.getText().clear();

    }

    @SuppressLint("StaticFieldLeak")
    private void changePassword(String current_password, String new_password, String confirm_pwd) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    user_id = PrefsUtils.getFromPrefs(context, Const.USERID, "");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("user_id", user_id);
                    jsonObject.put("current_password", current_password);
                    jsonObject.put("new_password", new_password);
                    jsonObject.put("confirm_password", confirm_pwd);
                    Utils.getInstance().d("mytag put is -------" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.CHANGE_PASSWORD, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Utils.getInstance().d("mytag" + response);
                mProgressDialog.dismiss();
                if (response == null) {
                    Toast.makeText(context, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String msg = jsonObject.getString("message");
                        if (status == 1) {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                            clearAllEdditText();
                            Intent registerIntent = new Intent(context, LoginActivity.class);
                            startActivity(registerIntent);
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

}
