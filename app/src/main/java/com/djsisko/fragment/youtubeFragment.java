package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djsisko.R;
import com.djsisko.activities.MainActivity;
import com.djsisko.manager.MediaController;
import com.djsisko.phonemidea.CheckNetwork;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class youtubeFragment extends Fragment {

    Context context;
    String title,link;
    TextView menuListHeading;
    WebView mWebView;
    Fragment frag;
    FragmentManager fm;
    FragmentTransaction ft;
    ImageView backButton1,playerButton1;
    MediaPlayer radioPlayer;
    LinearLayout adView;



    @SuppressLint("ValidFragment")
    public youtubeFragment(Context context, String title, String link,MediaPlayer radioPlayer,LinearLayout adView)
    {
        this.context = context;
        this.title = title;
        this.link = link;
        this.radioPlayer = radioPlayer;
        this.adView = adView;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.removeAd();
        View v = inflater.inflate(R.layout.fragment_youtube, container, false);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        menuListHeading = v.findViewById(R.id.menuListHeading);
        mWebView = (WebView) v.findViewById(R.id.webView);
        if (CheckNetwork.isInternetAvailable(context))
        {
            getwebView(link);
            stopSong();
        }
        else
        {
            Toast.makeText(context,"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
//        mWebView.loadUrl(link);
        // Enable Javascript
//        WebSettings webSettings = mWebView.getSettings();
//        webSettings.setJavaScriptEnabled(true);
//        mWebView.setWebViewClient(new WebViewClient());
        menuListHeading.setText(title);
        menuListHeading.setSelected(true);
        menuListHeading.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        menuListHeading.setSingleLine(true);
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.getAd();
                frag = new SocialMedia(context,radioPlayer,adView);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWebView.destroy();
                MainActivity.getAd();
                MainActivity.setPlayLayout();
            }
        });
        return v;

    }

    public void stopSong()
    {
        mWebView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (MediaController.getInstance().getPlayingSongDetail() != null)
                {
                    if (!MediaController.getInstance().isAudioPaused())
                    {
                        MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                    }
                }
                if (radioPlayer != null && radioPlayer.isPlaying())
                {
                    radioPlayer.pause();
                    MainActivity.checkRadio();
                }
                return false;
            }
        });
    }

    public void getwebView(String link)
    {
        mWebView.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        mWebView.getSettings().setLoadsImagesAutomatically(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(link);
    }

}
