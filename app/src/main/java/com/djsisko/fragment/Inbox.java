package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.djsisko.R;
import com.djsisko.activities.MainActivity;
import com.djsisko.adapter.NotificationAdapter;
import com.djsisko.data.Song;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Inbox extends Fragment {

    Context context;
    String title;
    ProgressDialog mProgressDialog;
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    ArrayList<Song> data = new ArrayList<>();
    NotificationAdapter notificationAdapter;
    RecyclerView notificationRecycler;
    RecyclerView.LayoutManager layoutManager;
    ImageView backButton1, playerButton1;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;

    public Inbox() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Inbox(Context context) {
        this.context = context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_notification, container, false);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);

        notificationRecycler = v.findViewById(R.id.notificationRecycler);
        layoutManager = new LinearLayoutManager(context);
        notificationRecycler.setLayoutManager(layoutManager);

        preferences = context.getSharedPreferences("MyAndroidId", Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId", "");
        isConnected = preferences.getBoolean("isConnected", false);
        Log.d("Inbox", "Android id: " + androidId);
        Log.d("Inbox", "Net is connected: " + isConnected);
        if (CheckNetwork.isInternetAvailable(context)) {
            data.clear();
            new notificationList().execute();
        } else {
            // mProgressDialog.dismiss();
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new MenuFragment(context);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        return v;
    }

    //    http://durisimomobileapps.net/djsisko/api/notifications
    private class notificationList extends AsyncTask<String, Void, String> {
        private JSONObject jsonObject;

        public notificationList() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/notifications";
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (Exception e) {
                return null;
            }
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            Log.d("Inbox", "Response is:" + s);
            if (s == null) {
                Log.d("mytag", "Inbox Response is null");
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    int status = jsonObject.getInt("status");
                    if (status == 0) {
                        Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    if (jsonArray.length() == 0) {
                        Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
                    }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject notificationIdJson = jsonArray.getJSONObject(i);
                        int notificationId = notificationIdJson.getInt("notification_id");
                        Log.d("Inbox", "Inbox Id is:" + notificationId);
                        JSONObject msgJson = jsonArray.getJSONObject(i);
                        String msg = msgJson.getString("message");
                        Log.d("Inbox", "message:" + msg);
                        JSONObject createdAtJson = jsonArray.getJSONObject(i);
                        String createdAt = createdAtJson.getString("created_at");
                        Log.d("Inbox", "createdAt:" + createdAt);
                        JSONObject updatedAtJson = jsonArray.getJSONObject(i);
                        String updatedAt = updatedAtJson.getString("updated_at");
                        Log.d("Inbox", "updatedAt:" + updatedAt);
                        data.add(new Song(notificationId, msg, createdAt));
                        notificationAdapter = new NotificationAdapter(context, data);
                        notificationRecycler.setAdapter(notificationAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(s);
        }
    }
}
