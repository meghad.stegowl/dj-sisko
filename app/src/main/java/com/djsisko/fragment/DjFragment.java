package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.adapter.SongDataAdapter;
import com.djsisko.data.PojoSongForPlayer;
import com.djsisko.data.Song;
import com.djsisko.manager.MediaController;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DjFragment extends Fragment {

    int featuredId;
    Context context;
    ArrayList<Song> data = new ArrayList<>();
    RecyclerView djRecycler;
    RecyclerView.LayoutManager layoutManager;
    SongDataAdapter dataAdapter;
    LinearLayout header;
    String title,instaUrl;
    TextView artistTitle;
    ProgressDialog mProgressDialog;
    ImageView instaLink;
    SharedPreferences preferences;
    String androidId;
    boolean isConnected;
    ImageView backButton1,playerButton1;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    String mainTag;
    TextView menuListHeading;
    String tag = DjFragment.class.getSimpleName();
    MediaPlayer radioPlayer;


    public DjFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public DjFragment(Context context, int featuredId,String title,String instaUrl,String mainTag,MediaPlayer radioPlayer)
    {
        this.context = context;
        this.featuredId = featuredId;
        this.title = title;
        this.instaUrl = instaUrl;
        this.mainTag = mainTag;
        this.radioPlayer = radioPlayer;
        Log.d("DjFragment","Insta Url is:"+instaUrl);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_dj, container, false);
        djRecycler = (RecyclerView)v.findViewById(R.id.djRecycler);
        menuListHeading = v.findViewById(R.id.menuListHeading);
//        menuListHeading.setSelected(true);
//        menuListHeading.setEllipsize(TextUtils.TruncateAt.MARQUEE);
//        menuListHeading.setSingleLine(true);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
//        menuListHeading = v.findViewById(R.id.menuListHeading);
        preferences = context.getSharedPreferences("MyAndroidId",Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId","");
        isConnected = preferences.getBoolean("isConnected",false);
        Log.d("DjFragment","Android id: "+androidId);
        Log.d("DjFragment","Net is connected: "+isConnected);
        layoutManager = new LinearLayoutManager(context);
        djRecycler.setLayoutManager(layoutManager);
        header = (LinearLayout)v.findViewById(R.id.header);
        artistTitle = (TextView)v.findViewById(R.id.artistTitle);
        instaLink = (ImageView)v.findViewById(R.id.instaLink);
        instaLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(instaUrl);
                Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
                likeIng.setPackage("com.instagram.android");
                try {
                    startActivity(likeIng);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(instaUrl)));
                }
                //for user
//                public static Intent newInstagramProfileIntent(PackageManager pm, String url) {
//                    final Intent intent = new Intent(Intent.ACTION_VIEW);
//                    try {
//                        if (pm.getPackageInfo("com.instagram.android", 0) != null) {
//                            if (url.endsWith("/")) {
//                                url = url.substring(0, url.length() - 1);
//                            }
//                            final String username = url.substring(url.lastIndexOf("/") + 1);
//                            // http://stackoverflow.com/questions/21505941/intent-to-open-instagram-user-profile-on-android
//                            intent.setData(Uri.parse("http://instagram.com/_u/" + username));
//                            intent.setPackage("com.instagram.android");
//                            return intent;
//                        }
//                    } catch (NameNotFoundException ignored) {
//                    }
//                    intent.setData(Uri.parse(url));
//                    return intent;
//                }
            }
        });
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new GuestDjs(context,radioPlayer);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(mainTag);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        JSONObject object = new JSONObject();
        try {
            object.put("u_id",androidId);
            object.put("feature_dj_id",featuredId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (CheckNetwork.isInternetAvailable(context))
        {
            data.clear();
        new DjSongAsyncTask(object).execute();
        }
        else
        {
            Toast.makeText(context,"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
        menuListHeading.setText(title);
        menuListHeading.setSelected(true);
        menuListHeading.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        menuListHeading.setSingleLine(true);
        artistTitle.setText(title);
        return v;
    }



    public void songLoaded(String url)
    {
        Log.d("Dj","Url is:"+url);
        for (Song s1: data)
        {
            if (s1.getSongUrl().equals(url))
            {
                s1.setNowPlaying(1);
            }
            else
            {
                s1.setNowPlaying(0);
            }
            SongDataAdapter dataAdapter = new SongDataAdapter(context,data,radioPlayer);
            djRecycler.setAdapter(dataAdapter);
        }


    }

//    http://durisimomobileapps.net/djsisko/api/djs/songs
public class DjSongAsyncTask extends AsyncTask<String, Void, String> {

    final PojoSongForPlayer playingSong = MediaController.getInstance().getPlayingSongDetail();
    private JSONObject jsonObject;
    public DjSongAsyncTask(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Loading");
        mProgressDialog.setCanceledOnTouchOutside(true);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        String result = null;
        try {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            String url = "http://durisimomobileapps.net/djsisko/api/djs/songs";
            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(JSON, String.valueOf(jsonObject));
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            result = response.body().string();
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        mProgressDialog.dismiss();
        Log.d("Dj Songs", "Response is:" + s);
        if (s == null) {
            Log.d("mytag", "Bookings Response is null");
        } else {
            try {
            JSONObject jsonObject = new JSONObject(s);
                int status = jsonObject.getInt("status");
                if(status == 0){
                    Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                }
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            if(jsonArray.length() == 0){
                Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
            }
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject songIdJson = jsonArray.getJSONObject(i);
                int songId = songIdJson.getInt("song_id");
                Log.d("Dj Songs", "Song Id is:" + songId);
                JSONObject songNameJson = jsonArray.getJSONObject(i);
                String songName = songNameJson.getString("song_name");
                Log.d("Dj Songs", "Song Name is:" + songName);
                JSONObject artistJson = jsonArray.getJSONObject(i);
                String artist = artistJson.getString("artist_name");
                Log.d("Dj songs", "Song Artist is: " + artist);
                JSONObject songUrlJson = jsonArray.getJSONObject(i);
                String songUrl = songUrlJson.getString("song_url");
                Log.d("Dj songs", "Song Url is: " + songUrl);
                JSONObject songImageJson = jsonArray.getJSONObject(i);
                String songImage = songImageJson.getString("image");
                Log.d("Dj songs", "Song image is: " + songImage);
                JSONObject likeStatusJson = jsonArray.getJSONObject(i);
                int likeStatus = likeStatusJson.getInt("like_status");
                Log.d("Dj songs", "Song like status is: " + likeStatus);
                JSONObject totalLikes = jsonArray.getJSONObject(i);
                int total = totalLikes.getInt("total_likes");
                Log.d("Total Likes :",""+total);
                JSONObject songDurationJson = jsonArray.getJSONObject(i);
                String songDuration = songDurationJson.getString("song_time");
                Log.d("Dj songs", "Song Duration is: " + songDuration);
//                    dataArtist1.add(new Song(id,image,song,artist,songUrl,duration,likes,0,0,0,0));
                int nowPlaying;
                if (playingSong != null)
                {
                    if (playingSong.getS_url().equals(songUrl))
                    {
                        nowPlaying = 1;
                    }
                    else
                    {
                        nowPlaying = 0;
                    }
                }
                else
                {
                    nowPlaying = 0;
                }
                Log.d("Dj","NowPlaying: "+nowPlaying);
                data.add(new Song(songId, songImage, songName, artist, songUrl, songDuration, likeStatus,total, 0, 0, 0, 0,nowPlaying));
//                data.add(new Song(1,"http:\\/\\/durisimomobileapps.net\\/djsisko\\/core\\/public\\/storage\\/uploads\\/img3_1513072333.png","abc","abc","xyz","00:00:00",1,0,0,0,0));

            }
            try {
                dataAdapter = new SongDataAdapter(context, data,radioPlayer);
                djRecycler.setAdapter(dataAdapter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch(JSONException e){
            e.printStackTrace();
        }
    }

        System.out.println(s);
    }
}



}
