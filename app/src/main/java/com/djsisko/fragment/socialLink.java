package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.djsisko.R;
import com.djsisko.activities.MainActivity;
import com.djsisko.phonemidea.CheckNetwork;


/**
 * A simple {@link Fragment} subclass.
 */
public class socialLink extends Fragment {

    Context context;
    String title,link;
    TextView menuListHeading;
    WebView mWebView;
    Fragment frag;
    FragmentManager fm;
    FragmentTransaction ft;
    ImageView backButton1,playerButton1;

    public socialLink() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public socialLink(Context context, String title, String link)
    {
        this.context = context;
        this.title = title;
        this.link = link;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_social_link, container, false);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        menuListHeading = v.findViewById(R.id.menuListHeading);
        mWebView = (WebView) v.findViewById(R.id.webView);
        if (CheckNetwork.isInternetAvailable(context)) {
            mWebView.loadUrl(link);
        }
        else
        {
            Toast.makeText(context,"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        // Force links and redirects to open in the WebView instead of in a browser
        mWebView.setWebViewClient(new WebViewClient());
        menuListHeading.setText(title);
        menuListHeading.setSelected(true);
        menuListHeading.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        menuListHeading.setSingleLine(true);
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(title.equals("Live Video") || title.equals("Live Chat")){
                    ((MainActivity)context).getSupportFragmentManager().popBackStack();
                } else {
                    frag = new SocialMedia(context);
                    fm = ((MainActivity) context).getSupportFragmentManager();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.container, frag);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        return v;
    }

}
