package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.djsisko.R;
import com.djsisko.activities.MainActivity;
import com.djsisko.adapter.SongDataAdapter;
import com.djsisko.data.PojoSongForPlayer;
import com.djsisko.data.Song;
import com.djsisko.manager.MediaController;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HitSingles extends Fragment {


    Context context;
    ArrayList<Song> data = new ArrayList<>();
    SongDataAdapter dataAdapter;
    String title;
    RecyclerView hitSinglesRecycler;
    RecyclerView.LayoutManager layoutManager;
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    ProgressDialog mProgressDialog;
    int nowPlaying;
    ImageView backButton1, playerButton1;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    MediaPlayer radioPlayer;

    public HitSingles() {
    }

    @SuppressLint("ValidFragment")
    public HitSingles(Context context, MediaPlayer radioPlayer) {
        this.context = context;
        this.radioPlayer = radioPlayer;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_hit_singles, container, false);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        preferences = context.getSharedPreferences("MyAndroidId", Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId", "");
        isConnected = preferences.getBoolean("isConnected", false);
        Log.d("HitSingles", "Android id: " + androidId);
        Log.d("HitSingles", "Net is connected: " + isConnected);
        hitSinglesRecycler = v.findViewById(R.id.hitSinglesRecycler);
        layoutManager = new LinearLayoutManager(context);
        hitSinglesRecycler.setLayoutManager(layoutManager);
        JSONObject object = new JSONObject();
        try {
            object.put("u_id", androidId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (CheckNetwork.isInternetAvailable(context)) {
            data.clear();
            new HitsinglesAsyncTask(object).execute();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new MenuFragment(context);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        return v;
    }

    public void songLoaded(String url) {
        Log.d("Hitsingles", "Url is:" + url);
        for (Song s1 : data) {
            if (s1.getSongUrl().equals(url)) {
                s1.setNowPlaying(1);
            } else {
                s1.setNowPlaying(0);
            }
            SongDataAdapter dataAdapter = new SongDataAdapter(context, data, radioPlayer);
            hitSinglesRecycler.setAdapter(dataAdapter);
        }

    }

    //    http://durisimomobileapps.net/djsisko/api/hitsingles
    public class HitsinglesAsyncTask extends AsyncTask<String, Void, String> {

        final PojoSongForPlayer playing = MediaController.getInstance().getPlayingSongDetail();
        private JSONObject jsonObject;

        public HitsinglesAsyncTask(JSONObject jsonObject) {
            this.jsonObject = jsonObject;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/hitsingles";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(jsonObject));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (Exception e) {
                return null;
            }
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            Log.d("Hit Singles", "Response is:" + s);
            if (s == null) {
                Log.d("mytag", "Hit Singles Response is null");
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    int status = jsonObject.getInt("status");
                    if (status == 0) {
                        Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    if (jsonArray.length() == 0) {
                        Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
                    }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject songIdJson = jsonArray.getJSONObject(i);
                        int songId = songIdJson.getInt("song_id");
                        Log.d("Hit songles Songs", "Song Id is:" + songId);
                        JSONObject songNameJson = jsonArray.getJSONObject(i);
                        String songName = songNameJson.getString("song_name");
                        Log.d("Hit songles Songs", "Song Name is:" + songName);
                        JSONObject artistJson = jsonArray.getJSONObject(i);
                        String artist = artistJson.getString("artist_name");
                        Log.d("Hit songles songs", "Song Artist is: " + artist);
                        JSONObject songUrlJson = jsonArray.getJSONObject(i);
                        String songUrl = songUrlJson.getString("song_url");
                        Log.d("Hit songles songs", "Song Url is: " + songUrl);
                        JSONObject songImageJson = jsonArray.getJSONObject(i);
                        String songImage = songImageJson.getString("image");
                        Log.d("Hit songles songs", "Song image is: " + songImage);
                        JSONObject likeStatusJson = jsonArray.getJSONObject(i);
                        int likeStatus = likeStatusJson.getInt("like_status");
                        Log.d("Hit songles songs", "Song like status is: " + likeStatus);
                        JSONObject songDurationJson = jsonArray.getJSONObject(i);
                        String songDuration = songDurationJson.getString("song_time");
                        Log.d("Hit songles songs", "Song Duration is: " + songDuration);
                        JSONObject totalLikes = jsonArray.getJSONObject(i);
                        int total = totalLikes.getInt("total_likes");
                        Log.d("Total Likes :", "" + total);
                        int nowPlaying;
                        if (playing != null) {
                            if (playing.getS_url().equals(songUrl)) {
                                nowPlaying = 1;
                            } else {
                                nowPlaying = 0;
                            }
                        } else {
                            nowPlaying = 0;
                        }
                        Log.d("MyPlaylist", "NowPlaying: " + nowPlaying);
                        data.add(new Song(songId, songImage, songName, artist, songUrl, songDuration, likeStatus, total, 0, 0, 0, 0, nowPlaying));
                        Log.d("NowPlaying value is:", "" + nowPlaying);
                    }
                    try {
                        dataAdapter = new SongDataAdapter(context, data, radioPlayer);
                        hitSinglesRecycler.setAdapter(dataAdapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(s);
        }

    }

}
