package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.djsisko.R;
import com.djsisko.activities.MainActivity;
import com.djsisko.adapter.SongDataAdapter;
import com.djsisko.data.PojoSongForPlayer;
import com.djsisko.data.Song;
import com.djsisko.manager.MediaController;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyFavorites extends Fragment {


    ArrayList<Song> data = new ArrayList<>();
    RecyclerView myFavRecycler;
    RecyclerView.LayoutManager layoutManager;
    Context context;
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    SongDataAdapter dataAdapter;
    int fromMenu;
    ProgressDialog mProgressDialog;
    ImageView backButton1, playerButton1;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    String tag = MyFavorites.class.getSimpleName();
    MediaPlayer radioPlayer;

    public MyFavorites() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public MyFavorites(Context context, MediaPlayer radioPlayer) {
        this.context = context;
        this.radioPlayer = radioPlayer;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my_favourites, container, false);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
//        android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        preferences = context.getSharedPreferences("MyAndroidId", Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId", "");
        isConnected = preferences.getBoolean("isConnected", false);
        Log.d("MyFav", "Android id: " + androidId);
        Log.d("MyFav", "Net is connected: " + isConnected);
        myFavRecycler = v.findViewById(R.id.myFavRecycler);
        layoutManager = new LinearLayoutManager(context);
        myFavRecycler.setLayoutManager(layoutManager);
        JSONObject object = new JSONObject();
        try {
            object.put("u_id", androidId);
            Log.d("mytag", "Json Object : " + object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (CheckNetwork.isInternetAvailable(context)) {
            data.clear();
            new MyFavouriteAsyncTask(object).execute();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("mytag", " tag - " + getTag());

                if (Objects.equals(getTag(), "outer")) {
                    MainActivity.setPlayLayout();
                } else {
                    frag = new MenuFragment(context);
                    fm = ((MainActivity) context).getSupportFragmentManager();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.container, frag);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        return v;
    }

    public void songLoaded(String url) {
        Log.d("MyFav", "Url is:" + url);
        for (Song s1 : data) {
            if (s1.getSongUrl().equals(url)) {
                s1.setNowPlaying(1);
            } else {
                s1.setNowPlaying(0);
            }
            SongDataAdapter dataAdapter = new SongDataAdapter(context, data, radioPlayer);
            myFavRecycler.setAdapter(dataAdapter);
        }

    }

    //http://durisimomobileapps.net/djsisko/api/favs/getsongs
    private class MyFavouriteAsyncTask extends AsyncTask<String, Void, String> {
        final PojoSongForPlayer playing = MediaController.getInstance().getPlayingSongDetail();
        JSONObject object;

        private MyFavouriteAsyncTask(JSONObject object) {
            this.object = object;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/favs/getsongs";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(object));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (
                    Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            Log.d("mytag", "My Favorite songs Response : " + s);
            if (s == null) {
                Log.d("mytag", "My Favorite Response is null");
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    int status = jsonObject.getInt("status");
                    if (status == 0) {
                        Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                    Log.d("Favourites song", " status is:" + status);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    if (jsonArray.length() == 0) {
                        Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
                    }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject favIdJson = jsonArray.getJSONObject(i);
                        int favId = favIdJson.getInt("fav_id");
                        Log.d("Favourites", "Fav Id is" + favId);
                        JSONObject idJson = jsonArray.getJSONObject(i);
                        int id = idJson.getInt("song_id");
                        JSONObject songNameJson = jsonArray.getJSONObject(i);
                        String song = songNameJson.getString("song_name");
                        JSONObject artistJson = jsonArray.getJSONObject(i);
                        String artist = artistJson.getString("artist_name");
                        JSONObject imageJson = jsonArray.getJSONObject(i);
                        String image = imageJson.getString("image");
                        JSONObject durationJson = jsonArray.getJSONObject(i);
                        String duration = durationJson.getString("song_time");
                        JSONObject songUrlJson = jsonArray.getJSONObject(i);
                        String songUrl = songUrlJson.getString("song_url");
                        JSONObject likeNoJson = jsonArray.getJSONObject(i);
                        int likes = likeNoJson.getInt("like_status");
                        JSONObject totalLikes = jsonArray.getJSONObject(i);
                        int total = totalLikes.getInt("total_likes");
                        int nowPlaying;
                        if (playing != null) {
                            if (playing.getS_url().equals(songUrl)) {
                                nowPlaying = 1;
                            } else {
                                nowPlaying = 0;
                            }
                        } else {
                            nowPlaying = 0;
                        }
                        Log.d("MyFav", "NowPlaying: " + nowPlaying);
                        data.add(new Song(id, image, song, artist, songUrl, duration, likes, total, 0, status, 0, favId, nowPlaying));
                    }
                    try {
                        dataAdapter = new SongDataAdapter(getContext(), data, radioPlayer);
                        myFavRecycler.setAdapter(dataAdapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(s);
        }
    }


}
