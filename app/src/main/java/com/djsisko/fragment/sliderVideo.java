package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.djsisko.R;
import com.djsisko.activities.MainActivity;
import com.djsisko.adapter.SongDataAdapter;
import com.djsisko.data.Song;
import com.djsisko.manager.MediaController;
import com.djsisko.phonemidea.CheckNetwork;

import java.util.ArrayList;

public class sliderVideo extends Fragment {

    Context context;
    String title, videoLink;
    SongDataAdapter dataAdapter;
    ArrayList<Song> data = new ArrayList<>();
    WebView mWebView;
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    String mainTag;
    TextView menuListHeading;
    ImageView backButton1, playerButton1;
    MediaPlayer radioPlayer;
    LinearLayout adView;


    public sliderVideo() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public sliderVideo(Context context, String title, String videoLink, String mainTag, MediaPlayer radioPlayer, LinearLayout adView) {
        this.context = context;
        this.title = title;
        this.videoLink = videoLink;
        this.mainTag = mainTag;
        this.radioPlayer = radioPlayer;
        this.adView = adView;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        adView.setVisibility(View.GONE);
        View v = inflater.inflate(R.layout.fragment_slider_video, container, false);
        menuListHeading = v.findViewById(R.id.menuListHeading);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        preferences = context.getSharedPreferences("MyAndroidId", Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId", "");
        isConnected = preferences.getBoolean("isConnected", false);
        Log.d("Videos2", "Android id: " + androidId);
        Log.d("Videos2", "Net is connected: " + isConnected);
        mWebView = v.findViewById(R.id.webView);
        if (CheckNetwork.isInternetAvailable(context)) {
            getwebView(videoLink);
            stopSong();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
                adView.setVisibility(View.VISIBLE);
            }
        });
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
                mWebView.destroy();
                adView.setVisibility(View.VISIBLE);
            }
        });
        menuListHeading.setText(title);
        menuListHeading.setSelected(true);
        menuListHeading.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        menuListHeading.setSingleLine(true);
        return v;
    }

    @SuppressLint("ClickableViewAccessibility")
    public void stopSong() {
        mWebView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    if (!MediaController.getInstance().isAudioPaused()) {
                        MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                    }
                } else if (radioPlayer != null && radioPlayer.isPlaying()) {
                    radioPlayer.pause();
                    MainActivity.checkRadio();
                }
                return false;
            }
        });
    }

    public void getwebView(String link) {
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        mWebView.getSettings().setLoadsImagesAutomatically(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(link);
    }


}
