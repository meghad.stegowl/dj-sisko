package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.adapter.SongDataAdapter;
import com.djsisko.data.PojoSongForPlayer;
import com.djsisko.data.Song;
import com.djsisko.manager.MediaController;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class MusicCategoryInner extends Fragment {


    Context context;
    ArrayList<Song> data = new ArrayList<>();
    int categoryId;
    String categoryTitle;
    RecyclerView categoryRecycler;
    RecyclerView.LayoutManager layoutManager;
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    SongDataAdapter dataAdapter;
    ProgressDialog mProgressDialog;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    TextView menuListHeading;
    ImageView backButton1,playerButton1;
    String tag;
    MediaPlayer radioPlayer;

    public MusicCategoryInner() {
        // Required empty public constructor
    }


    @SuppressLint("ValidFragment")
    public MusicCategoryInner(Context context, int categoryId, String categoryTitle, String tag, MediaPlayer radioPlayer)
    {
        this.context = context;
        this.categoryId = categoryId;
        this.categoryTitle = categoryTitle;
        this.tag = tag;
        this.radioPlayer = radioPlayer;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_music_category_inside, container, false);
        menuListHeading = v.findViewById(R.id.menuListHeading);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        preferences = context.getSharedPreferences("MyAndroidId",Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId","");
        isConnected = preferences.getBoolean("isConnected",false);
        Log.d("MusicCategoryinside","Android id: "+androidId);
        Log.d("MusicCategoryinside","Net is connected: "+isConnected);
        JSONObject object = new JSONObject();
        try {
            object.put("u_id", androidId);
            object.put("category_id", categoryId);

            Log.d("mytag", "Json Object : " + object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        categoryRecycler = (RecyclerView)v.findViewById(R.id.categoryRecycler);
        layoutManager = new LinearLayoutManager(context);
        categoryRecycler.setLayoutManager(layoutManager);

        if (CheckNetwork.isInternetAvailable(context)){
            data.clear();
            new CategoryAsyncTask(object).execute();
        }
        else
        {
            Toast.makeText(context,"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
        menuListHeading.setText(categoryTitle);
        menuListHeading.setSelected(true);
        menuListHeading.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        menuListHeading.setSingleLine(true);
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new MusicCategories(context,radioPlayer);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(tag);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        return v;
    }
    public void songLoaded(String url)
    {
        Log.d("Music Category","Url is:"+url);
        for (Song s1: data)
        {
            if (s1.getSongUrl().equals(url))
            {
                s1.setNowPlaying(1);
            }
            else
            {
                s1.setNowPlaying(0);
            }
            SongDataAdapter dataAdapter = new SongDataAdapter(context,data,radioPlayer);
            categoryRecycler.setAdapter(dataAdapter);
        }

    }

    //    http://durisimomobileapps.net/djsisko/api/songsbycategory
    public class CategoryAsyncTask extends AsyncTask<String, Void, String> {

       final PojoSongForPlayer playingSong = MediaController.getInstance().getPlayingSongDetail();
        private JSONObject jsonObject;
        public CategoryAsyncTask(JSONObject jsonObject) {
            this.jsonObject = jsonObject;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/songsbycategory";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(jsonObject));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                 result = response.body().string();
                return result;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
                Log.d("Category", "Response is:" + s);
            if (!s.equals(null)) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    int status = jsonObject.getInt("status");
                    if(status == 0){
                        Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
            if(jsonArray.length() == 0){
                Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
            }
                    if(jsonArray.length() == 0){
                        Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
                    }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject songIdJson = jsonArray.getJSONObject(i);
                        int songId = songIdJson.getInt("song_id");
                        Log.d("Category Songs", "Song Id is:" + songId);
                        JSONObject songNameJson = jsonArray.getJSONObject(i);
                        String songName = songNameJson.getString("song_name");
                        Log.d("Category Songs", "Song Name is:" + songName);
                        JSONObject artistJson = jsonArray.getJSONObject(i);
                        String artist = artistJson.getString("artist_name");
                        Log.d("Category songs", "Song Artist is: " + artist);
                        JSONObject songUrlJson = jsonArray.getJSONObject(i);
                        String songUrl = songUrlJson.getString("song_url");
                        Log.d("Category songs", "Song Url is: " + songUrl);
                        JSONObject songImageJson = jsonArray.getJSONObject(i);
                        String songImage = songImageJson.getString("image");
                        Log.d("Category songs", "Song image is: " + songImage);
                        JSONObject likeStatusJson = jsonArray.getJSONObject(i);
                        int likeStatus = likeStatusJson.getInt("like_status");
                        Log.d("Category songs", "Song like status is: " + likeStatus);
                        JSONObject songDurationJson = jsonArray.getJSONObject(i);
                        String songDuration = songDurationJson.getString("song_time");
                        Log.d("Category songs", "Song Duration is: " + songDuration);
                        JSONObject totalLikes = jsonArray.getJSONObject(i);
                        int total = totalLikes.getInt("total_likes");
                        Log.d("Total Likes :",""+total);
                        int nowPlaying;
                       if (playingSong != null)
                       {
                            if (playingSong.getS_url().equals(songUrl))
                            {
                                nowPlaying = 1;
                            }
                            else
                            {
                                nowPlaying = 0;
                            }
                       }
                       else
                       {
                            nowPlaying = 0;
                       }
                        Log.d("Category","NowPlaying: "+nowPlaying);
//                    dataArtist1.add(new Song(id,image,song,artist,songUrl,duration,likes,0,0,0,0));
                        data.add(new Song(songId, songImage, songName, artist, songUrl, songDuration, likeStatus,total, 0, 0, 0, 0,nowPlaying));
                    }
                    try {
                        dataAdapter = new SongDataAdapter(context,data,radioPlayer);
                        categoryRecycler.setAdapter(dataAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else
            {

            }
            System.out.println(s);
        }


    }


}
