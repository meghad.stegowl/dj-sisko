package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.adapter.ImageGalleryDataAdapter;
import com.djsisko.data.Song;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Gallery extends Fragment {

    Context context;
    ArrayList<Song> dataModules = new ArrayList<>();
    String title;
    RecyclerView galleryRecycler;
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    ImageGalleryDataAdapter dataAdapter;
    ProgressDialog mProgressDialog;
    ImageView backButton1,playerButton1;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    String tag = Gallery.class.getSimpleName();

    public Gallery() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Gallery(Context context)
    {
        this.context = context;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_gallery, container, false);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        galleryRecycler = (RecyclerView) v.findViewById(R.id.galleryRecycler);
        preferences = context.getSharedPreferences("MyAndroidId",Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId","");
        isConnected = preferences.getBoolean("isConnected",false);
        Log.d("Gallery","Android id: "+androidId);
        Log.d("Gallery","Net is connected: "+isConnected);
        JSONObject object = new JSONObject();
        try {
            object.put("u_id", androidId);
            Log.d("mytag", "Json Object : " + object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (CheckNetwork.isInternetAvailable(context))
        {
            dataModules.clear();
            new ImageGalleryAsyncTask(object).execute();
        }
         else
        {
            Toast.makeText(context,"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new MenuFragment(context);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        return v;
    }
//    http://durisimomobileapps.net/djsisko/api/folders
public class ImageGalleryAsyncTask extends AsyncTask<String, Void, String> {

    private JSONObject jsonObject;

    public ImageGalleryAsyncTask(JSONObject object) {
        this.jsonObject = object;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Loading");
        mProgressDialog.setCanceledOnTouchOutside(true);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        String result = null;
        try {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            String url = "http://durisimomobileapps.net/djsisko/api/folders";
            OkHttpClient client = new OkHttpClient();
//                RequestBody body = RequestBody.create(JSON, String.valueOf(jsonObject));
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            result = response.body().string();
            return result;
        } catch (Exception e) {
            return null;
        }
    }


    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        mProgressDialog.dismiss();
        Log.d("mytag", "REsponse Gallery : " + s);
        if (s == null) {
            Log.d("mytag", "Bookings Response is null");
        } else {
            try
            {
            JSONObject jsonObject = new JSONObject(s);
                int status = jsonObject.getInt("status");
                if(status == 0){
                    Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                }
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            if(jsonArray.length() == 0){
                Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
            }
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject idJson = jsonArray.getJSONObject(i);
                int id = idJson.getInt("folder_id");
                Log.d("Folder id is", "" + id);
                JSONObject folderImageJson = jsonArray.getJSONObject(i);
                String image = folderImageJson.getString("folder_image");
                Log.d("Folder image is", "" + image);
                JSONObject folderTitleJson = jsonArray.getJSONObject(i);
                String folderTitle = folderTitleJson.getString("folder_name");
                Log.d("Folder title is", "" + image);
                dataModules.add(new Song(id, image, folderTitle));
//                dataModules.add(new Song(1,"http:\\/\\/durisimomobileapps.net\\/djsisko\\/core\\/public\\/storage\\/uploads\\/img3_1513072333.png","xyz"));
            }
            try {
                dataAdapter = new ImageGalleryDataAdapter(context, dataModules,tag);
                galleryRecycler.setLayoutManager(new GridLayoutManager(context, 2));
                galleryRecycler.setAdapter(dataAdapter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch(JSONException e){
            e.printStackTrace();
        }
    }
        System.out.println(s);
    }
}
}
