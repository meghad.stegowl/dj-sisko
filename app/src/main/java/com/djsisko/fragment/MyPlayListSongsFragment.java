package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.adapter.SongDataAdapter;
import com.djsisko.data.PojoSongForPlayer;
import com.djsisko.data.Song;
import com.djsisko.manager.MediaController;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyPlayListSongsFragment extends Fragment {

    Context context;
    String title;
    TextView menuListHeading;
    ImageView backButton1,playerButton1;
    int playListID;
    String playName;
    Fragment frag;
    FragmentTransaction ft;
    ProgressDialog mProgressDialog;
    FragmentManager fm;
    RecyclerView MyPlayListSongs;
    RecyclerView.LayoutManager layoutManager;
    SongDataAdapter dataAdapter;
    ArrayList<Song> data = new ArrayList<>();
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    String mainTag;
    MediaPlayer radioPlayer;

    public MyPlayListSongsFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
//    context, playListID, playName,mainLayout
    public MyPlayListSongsFragment(Context context,int playListID, String playName,String mainTag,MediaPlayer radioPlayer)
    {
        this.context = context;
      this.playListID = playListID;
      this.playName = playName;
        this.mainTag = mainTag;
        this.radioPlayer = radioPlayer;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my_play_list_songs, container, false);
        menuListHeading = v.findViewById(R.id.menuListHeading);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        preferences = context.getSharedPreferences("MyAndroidId",Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId","");
        isConnected = preferences.getBoolean("isConnected",false);
        Log.d("MyPlayListSongs","Android id: "+androidId);
        Log.d("MyPlayListSongs","Net is connected: "+isConnected);
        MyPlayListSongs = (RecyclerView)v.findViewById(R.id.MyPlayListSongs);
        layoutManager = new LinearLayoutManager(context);
        MyPlayListSongs.setLayoutManager(layoutManager);
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new MyPlayList(context,radioPlayer);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(mainTag);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        JSONObject object = new JSONObject();
        try {
            object.put("playlist_id",playListID);
            object.put("u_id",androidId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (CheckNetwork.isInternetAvailable(context))
        {
            data.clear();
            new pListDetailsAsyncTask(object).execute();
        }
         else
        {
            Toast.makeText(context,"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
        menuListHeading.setText(playName);
        menuListHeading.setSelected(true);
        menuListHeading.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        menuListHeading.setSingleLine(true);
        return v;
    }

    public void songLoaded(String url)
    {
        Log.d("MyPlaylist","Url is:"+url);
        for (Song s1: data)
        {
            if (s1.getSongUrl().equals(url))
            {
                s1.setNowPlaying(1);
            }
            else
            {
                s1.setNowPlaying(0);
            }
            SongDataAdapter dataAdapter = new SongDataAdapter(context,data,radioPlayer);
            MyPlayListSongs.setAdapter(dataAdapter);
        }

    }

    //   http://durisimomobileapps.net/djsisko/api/playlist/getsongs
    private class pListDetailsAsyncTask extends AsyncTask<String,Void,String>
    {
        final PojoSongForPlayer playing = MediaController.getInstance().getPlayingSongDetail();
        JSONObject jsonObject;
        private pListDetailsAsyncTask(JSONObject jsonObject){
            this.jsonObject = jsonObject;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }
        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/playlist/getsongs";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(jsonObject));

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                 result = response.body().string();
                return result;
            } catch (Exception e) {
                return null;
            }
        }


        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
                Log.d("mytag", "PlayList Details Response : " + s);
                if (s == null) {
                    Log.d("mytag", "Bookings Response is null");
                } else {
                    try
                    {
                    JSONObject jsonObject = new JSONObject(s);
                        int status = jsonObject.getInt("status");
                        if(status == 0){
                            Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    Log.d("status is:", "" + status);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
            if(jsonArray.length() == 0){
                Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
            }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject sipIdJson = jsonArray.getJSONObject(i);
                        int sipId = sipIdJson.getInt("sip_id");
                        Log.d("PlayList", "sip_Id is:" + sipId);
                        JSONObject idJson = jsonArray.getJSONObject(i);
                        int id = idJson.getInt("song_id");
                        JSONObject songNameJson = jsonArray.getJSONObject(i);
                        String song = songNameJson.getString("song_name");
//                    Log.d("Song name is", song);
                        JSONObject artistJson = jsonArray.getJSONObject(i);
                        String artist = artistJson.getString("artist_name");
                        JSONObject imageJson = jsonArray.getJSONObject(i);
                        String image = imageJson.getString("image");
                        JSONObject durationJson = jsonArray.getJSONObject(i);
                        String duration = durationJson.getString("song_time");
                        Log.d("Duration is", duration);
                        JSONObject songUrlJson = jsonArray.getJSONObject(i);
                        String songUrl = songUrlJson.getString("song_url");
                        Log.d("Song Url is", songUrl);
                        JSONObject likeNoJson = jsonArray.getJSONObject(i);
                        int likes = likeNoJson.getInt("like_status");
                        Log.d("like status is", "" + likes);
                        JSONObject totalLikes = jsonArray.getJSONObject(i);
                        int total = totalLikes.getInt("total_likes");
                        Log.d("Total Likes :",""+total);
                        int nowPlaying;
                        if (playing != null)
                        {
                            if (playing.getS_url().equals(songUrl))
                            {
                                nowPlaying = 1;
                            }
                            else
                            {
                                nowPlaying = 0;
                            }
                        }
                        else
                        {
                            nowPlaying = 0;
                        }
                        Log.d("MyPlaylist","NowPlaying: "+nowPlaying);
                        data.add(new Song(id, image, song, artist, songUrl, duration, likes,total, status, 0, sipId, 0,nowPlaying));
                    }
                    try {
                        dataAdapter = new SongDataAdapter(context, data,radioPlayer);
                        MyPlayListSongs.setAdapter(dataAdapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch(JSONException e){
                    e.printStackTrace();
                }
            }
            System.out.println(s);
        }
    }
}
