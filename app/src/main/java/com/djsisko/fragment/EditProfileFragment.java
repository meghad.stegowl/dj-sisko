package com.djsisko.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.djsisko.R;
import com.djsisko.Utility.Const;
import com.djsisko.Utility.FileUtils;
import com.djsisko.Utility.MultipartUtility;
import com.djsisko.Utility.NumberTextWatcher;
import com.djsisko.Utility.PrefsUtils;
import com.djsisko.Utility.WebInterface;
import com.djsisko.activities.MainActivity;
import com.djsisko.phonemidea.CheckNetwork;
import com.djsisko.phonemidea.Prefs;
import com.djsisko.phonemidea.Utils;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileFragment extends Fragment {

    private static final int PICK_FROM_CAMERA = 11;
    private static final int PICK_FROM_FILE = 13;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    int beforeLength;
    private Button btn_change_password, btn_save;
    private CircleImageView profilePic, iv_edit;
    private Context context;
    private EditText edt_name, edt_user_name, edt_email, edt_phone;
    private String name, userName, email, phone;
    private ArrayAdapter<String> adapter;
    private String image_from_pref;
    private Fragment frag;
    private FragmentTransaction ft;
    private FragmentManager fm;
    private LinearLayout adView;
    private FrameLayout fm_profile;
    private ProgressDialog mProgressDialog;
    private LinearLayout btn_back;
    private Uri mImageCaptureUri;
    private File file_additional_member;
    private String imageToString = "";
    private ImageView iv_video, iv_home;
    private String lastChar = " ";

    @SuppressLint("ValidFragment")
    public EditProfileFragment(Context context, LinearLayout adView){
        this.context = context;
        this.adView = adView;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }

    private void loadGlide(Context context, ImageView iv, String url) {
        try {
            Glide.with(context)
                    .load(url)
                    .placeholder(R.drawable.big1logo)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(iv);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        MainActivity.bottomViewPlayer.setVisibility(View.GONE);
        Prefs.getPrefInstance().setValue(context, Const.PROFILE_IMAGE, "");

        final String[] items = new String[]{"Take from camera", "Gallery", "Cancel"};
        adapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, items);

        init(v);
        listener();

        if (Utils.getInstance().isConnectivity(context)) {
            getProfileDetail();
        } else {
            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
        }
        return v;
    }

    private void init(View rootView) {
        btn_change_password = rootView.findViewById(R.id.btn_change_password);
        btn_save = rootView.findViewById(R.id.btn_save);
        profilePic = rootView.findViewById(R.id.profilePic);
        iv_edit = rootView.findViewById(R.id.iv_edit);
        edt_name = rootView.findViewById(R.id.edt_name);
        edt_user_name = rootView.findViewById(R.id.edt_user_name);
        edt_email = rootView.findViewById(R.id.edt_email);
        edt_phone = rootView.findViewById(R.id.edt_phone);
        btn_back = rootView.findViewById(R.id.btn_back);
        iv_video = rootView.findViewById(R.id.iv_video);
        iv_home = rootView.findViewById(R.id.iv_home);
        fm_profile = rootView.findViewById(R.id.fm_profile);
    }

    private void listener() {

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.bottomViewPlayer.setVisibility(View.VISIBLE);
                frag = new MenuFragment(context);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        iv_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag = new videoFromFragment(context);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        iv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateData();
            }
        });

        btn_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag = new ChangePasswordFragment(context, adView);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
//                Intent intent = new Intent(context, ChangePasswordActivity.class);
//                startActivity(intent);
            }
        });

        fm_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    Utils.getInstance().d("In check permission");
                    CheckPermission();
                } else {
                    if (Utils.getInstance().isConnectivity(context)) {
                        imageupload();
                    } else {
                        Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

//        edt_phone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());


        edt_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                int digits = edt_phone.getText().toString().length();
                if (digits > 1)
                    lastChar = edt_phone.getText().toString().substring(digits-1);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int digits = edt_phone.getText().toString().length();
                if (!lastChar.equals("-")) {
                    if (digits == 3 || digits == 7) {
                        edt_phone.append("-");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void getProfileDetail() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    String user_id = PrefsUtils.getFromPrefs(context, Const.USERID, "");
                    response = WebInterface.getInstance().doGet(Const.GETPROFILE + "?filter=" + user_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Utils.getInstance().d("mytag" + response);
                mProgressDialog.dismiss();
                if (response == null) {
                    Toast.makeText(context, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject data = jsonArray.getJSONObject(i);
                                String gcm_id = data.getString("gcm_id");
                                String name = data.getString("name");
                                Log.d("mytag", "name is----------" + name);
                                edt_name.setText(name);
                                String user_name = data.getString("user_name");
                                edt_user_name.setText(user_name);
                                String email = data.getString("email");
                                edt_email.setText(email);
                                String contact = data.getString("contact");

                                String contact1 = contact.substring(0, 3)+"-"+contact.substring(3, 6)+"-"+contact.substring(6, 10);

//                                String formattedNumber = PhoneNumberUtils.formatNumber(contact);
                                edt_phone.setText(contact1);
                                Log.d("mytag", "name is----------" + contact1);
                                String image = data.getString("image");
                                Log.d("mytag", "image is----------" + image);
                                loadGlide(context, profilePic, image);
                                Prefs.getPrefInstance().setValue(context, Const.PROFILE_IMAGE, image);
                            }

                            JSONArray jsonArray1 = jsonObject.getJSONArray("image_assets");
                            for (int j = 0; j < jsonArray1.length(); j++) {
                                JSONObject data1 = jsonArray1.getJSONObject(j);
                                String display_type = data1.getString("display_type");
                                String image_type = data1.getString("image_type");
                            }

                            JSONArray jsonArray3 = jsonObject.getJSONArray("image_size");
                            for (int k = 0; k < jsonArray3.length(); k++) {
                                JSONObject data3 = jsonArray3.getJSONObject(k);
                                String height = data3.getString("height");
                                String width = data3.getString("width");
                            }


                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

    private void validateData() {
        name = edt_name.getText().toString().trim();
        userName = edt_user_name.getText().toString().trim();
        email = edt_email.getText().toString().trim();
        phone = edt_phone.getText().toString().replace("-","");

        image_from_pref = Prefs.getPrefInstance().getValue(context, Const.PROFILE_IMAGE, "");
        Log.d("mytag", "image_from_pref in validate is ---" + image_from_pref);
        loadGlide(context, profilePic, image_from_pref);

        if (!name.isEmpty() && !userName.isEmpty() && isValidEmail(email) && isValidMobile(phone)) {
            if (CheckNetwork.isInternetAvailable(context)) {
                Log.d("mytag","phone number is---------------"+phone);
                submitProfileDetail(name, userName, email ,phone);
            } else {
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (name.isEmpty()) {
                edt_name.setError("Please Enter Name");
                edt_name.requestFocus();
            } else if (userName.isEmpty()) {
                edt_user_name.setError("Please Enter UserName");
                edt_user_name.requestFocus();
            } else if (!isValidEmail(email)) {
                edt_email.setError("Please Enter Valid Email");
                edt_email.requestFocus();
            } else if (edt_phone.getText().toString().length() == 0) {
                edt_phone.setError("Please Enter Mobile Number");
                edt_phone.requestFocus();
            }
//            else if (edt_phone.getText().toString().length() != 0 && edt_phone.getText().toString().length() < 10) {
//                edt_phone.setError("Please Enter Valid Mobile Number");
//                edt_phone.requestFocus();
//            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void submitProfileDetail(String name, String userName, String email, String phone) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    String user_id = PrefsUtils.getFromPrefs(context, Const.USERID, "");
                    String profile_img = Prefs.getPrefInstance().getValue(context, Const.PROFILE_IMAGE, "");
                    Log.d("mytag", "image is ---" + profile_img);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("user_id", user_id);
                    jsonObject.put("name", name);
                    jsonObject.put("user_name", userName);
                    jsonObject.put("email", email);
                    jsonObject.put("contact", phone);
//                    if (!profile_img.equals("")) {
//                        jsonObject.put("image", profile_img);
//                        Utils.getInstance().d("image in re -------" + profile_img);
//                    } else {
//                        image_from_pref = Prefs.getPrefInstance().getValue(context, Const.PROFILE_IMAGE, "");
//                        Log.d("mytag", "image_from_pref image is ---" + image_from_pref);
//
//                        jsonObject.put("image", image_from_pref);
//                        Utils.getInstance().d("image in else -------");
//                    }

                    image_from_pref = Prefs.getPrefInstance().getValue(context, Const.PROFILE_IMAGE, "");
                    if (!image_from_pref.equals("")) {
                        Log.d("mytag", "image_from_pref image is ---" + image_from_pref);
                        jsonObject.put("image", image_from_pref);
                    } else {
                        jsonObject.put("image", "");
                        Utils.getInstance().d("image in else -------");
                    }

                    Utils.getInstance().d("profile - " + image_from_pref);
                    Prefs.getPrefInstance().setValue(context, Const.PROFILE_IMAGE, image_from_pref);
                    response = WebInterface.getInstance().doPostRequest(Const.PROFILEUPDATE, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Utils.getInstance().d("mytag" + response);
                mProgressDialog.dismiss();
                if (response == null) {
                    Toast.makeText(context, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                            String gcm_id = jsonObject.getString("gcm_id");
                            String song_comment = jsonObject.getString("song_comment");
                            String image = jsonObject.getString("image");
                            String user_name = jsonObject.getString("user_name");
                            String name = jsonObject.getString("name");
                            String email = jsonObject.getString("email");
                            String contact = jsonObject.getString("contact");
                            Log.d("mytag","phone number is in submit profile---"+contact);
                            mProgressDialog.dismiss();
//                            frag = new MenuFragment(context);
//                            fm = ((MainActivity) context).getSupportFragmentManager();
//                            ft = fm.beginTransaction();
//                            ft.replace(R.id.container, frag);
//                            ft.addToBackStack(null);
//                            ft.commit();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }


    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    private void CheckPermission() {

        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera Access");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write Storage.");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "Sisko needs some permissions from your device.";
                for (int i = 1; i < permissionsNeeded.size(); i++)

                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                }
                            });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }

        imageupload();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                return shouldShowRequestPermissionRationale(permission);
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    imageupload();
                } else {
                    Toast.makeText(context, "Some Permission is Denied", Toast.LENGTH_SHORT).show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void imageupload() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(),
                            "djsisko" + System.currentTimeMillis() + ".jpg"));
                    Log.d("mytag", "imagetostring---------" + mImageCaptureUri);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                    try {
                        intent.putExtra("return-data", true);
                        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                        StrictMode.setVmPolicy(builder.build());
                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                if (item == 1) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != -1) return;

        switch (requestCode) {
            case PICK_FROM_CAMERA:
                Log.d("mytag", "inside class:FragmentMyProfile \t pick from camara");
                Log.d("mytag", "imagepathCamera" + FileUtils.getPath(context, mImageCaptureUri));
                docrop(mImageCaptureUri);
                break;

            case PICK_FROM_FILE:
                Log.d("mytag", "inside class:FragmentMyProfile \t pic from file");
                mImageCaptureUri = data.getData();
                Log.d("mytag", "imagepathGallary" + FileUtils.getPath(context, mImageCaptureUri));
                docrop(mImageCaptureUri);
                break;
        }
    }

    private void docrop(Uri uri) {
        final AppCompatDialog dialog1;
        dialog1 = new AppCompatDialog(context);
        dialog1.setContentView(R.layout.custom_dialog_crop);
        dialog1.setTitle("");
        dialog1.show();
        Button btn_crop = dialog1.findViewById(R.id.btn_crop);
        Button btn_rotate = dialog1.findViewById(R.id.btn_roate);
        final CropImageView cropImageView = dialog1.findViewById(R.id.cropImageView);
        cropImageView.setImageUriAsync(uri);

        btn_crop.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onClick(View v) {
                final Bitmap bitmap = cropImageView.getCroppedImage(200, 200);
                dialog1.dismiss();
                {
//                   /* String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/djsisko";
//                    File dir = new File(file_path);
//                    Log.d("mytag", "file_path in docrop---------" + file_path);
//
//                    if (!dir.exists())
//                        dir.mkdirs();
//                    String format = "djsisko" + System.currentTimeMillis();
//
//                    File file = new File(dir, format + ".png");
//                    Log.d("mytag", "file in docrop---------" + file);
//                    Log.d("mytag", "file path in docrop---------" + file_path);
//                    FileOutputStream fOut;
//                    try {
//                        fOut = new FileOutputStream(file);
//                        bitmap.compress(Bitmap.CompressFormat.PNG, 90, fOut);
//                        fOut.flush();
//                        fOut.close();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    file_additional_member = null;
//                    file_additional_member = file;
//                    Log.d("mytag", "file_additional_member in docrop---------" + file_additional_member);
//
//                    imageToString = "1";*/
                    final String[] str = {null};
                    new AsyncTask<Void, Void, String>() {

                        @Override
                        protected void onPreExecute() {
                            mProgressDialog = new ProgressDialog(context);
                            mProgressDialog.setMessage("Loading");
                            mProgressDialog.setCanceledOnTouchOutside(false);
                            mProgressDialog.setIndeterminate(true);
                            mProgressDialog.setCancelable(true);
                            mProgressDialog.show();
                            super.onPreExecute();
                        }

                        @Override
                        protected String doInBackground(Void... params) {
                            try {
                                MultipartUtility multipart = new MultipartUtility(Const.UPLOAD_IMAGE);
                                File f1 = new File(FileUtils.getPath(context, mImageCaptureUri));
                                multipart.addFilePart("image", f1);
                                Log.d("mytag", "file_additional_member in docrop---------" + file_additional_member);
                                List<String> response2 = multipart.finish();
                                StringBuilder sb = new StringBuilder();
                                for (String line : response2) {
                                    sb.append(line);
                                    sb.append("\t");
                                }
                                str[0] = sb.toString();

                                Utils.getInstance().d(" URL Upload ::" + sb.toString());
                            } catch (Exception ex) {
                                System.err.println(ex);
                            }

                            return str[0];
                        }

                        @Override
                        protected void onPostExecute(String response) {
                            super.onPostExecute(response);
                            mProgressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                imageToString = jsonObject.getString("image");
                                Log.d("mytag", "imagetostring" + imageToString);
                                if (!imageToString.equals("1") && !imageToString.equals(null)) {
                                    String image = Utils.getInstance().bitmapToString(bitmap);
                                    profilePic.setImageBitmap(bitmap);
                                    Log.d("mytag", "bitmap image is--------------" + bitmap);
                                    Log.d("mytag", "imageToString image is--------------" + imageToString);
                                    Prefs.getPrefInstance().setValue(context, Const.PROFILE_IMAGE, imageToString);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }.execute();
                }
            }
        });

        btn_rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropImageView.rotateImage(90);
            }
        });
    }


}
