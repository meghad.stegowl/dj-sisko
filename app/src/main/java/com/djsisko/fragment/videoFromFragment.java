package com.djsisko.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.djsisko.ApplicationDMPlayer;
import com.djsisko.R;
import com.djsisko.Remote.ApiClient;
import com.djsisko.Remote.ApiInterface;
import com.djsisko.Utility.Const;
import com.djsisko.Utility.PrefsUtils;
import com.djsisko.Utility.WebInterface;
import com.djsisko.activities.EmbededVideoActivity;
import com.djsisko.activities.LoginVideoActivity;
import com.djsisko.activities.MainActivity;
import com.djsisko.adapter.AdapterUserComment;
import com.djsisko.adapter.AdapterViewPager;
import com.djsisko.adapter.RecyclerViewPositionHelper;
import com.djsisko.custom.CustomViewPager;
import com.djsisko.data.ImageSliderModel;
import com.djsisko.data.MessageBoard;
import com.djsisko.data.MessageBoardData;
import com.djsisko.data.Video;
import com.djsisko.data.VideoData;
import com.djsisko.phonemidea.CheckNetwork;
import com.djsisko.phonemidea.Prefs;
import com.djsisko.phonemidea.Utils;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;

import static android.view.View.SYSTEM_UI_FLAG_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_RENDERER;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_SOURCE;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_UNEXPECTED;

public class videoFromFragment extends Fragment {

    static Context context;
    static RecyclerView rv_message_board;
    static AdapterUserComment adapterUserComment;
    static ArrayList<MessageBoardData> messageBoardData = new ArrayList<>();
    static String video_id;
    private static ProgressDialog mProgressDialog;
    private final Object progressTimerSync = new Object();
    private final Object sync = new Object();
    PlayerView video_player;
    String embedded_video_link;
    String video_like_status;
    int like_count;
    int dislike_count;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<VideoData> videoData = new ArrayList<>();
    Call<Video> call;
    String skip_user_id, skip_status;
    String value_for_back;
    private ExoPlayer player;
    private View rootView;
    private boolean playWhenReady = true;
    private CustomViewPager customViewPager;
    private ArrayList<ImageSliderModel> imageArraylist1;
    private AdapterViewPager adapterViewPager;
    private Timer progressTimer = null;
    private ImageView iv_like, iv_dislike;
    private TextView tv_like, tv_dislike;
    private RelativeLayout rl_share;
    private LinearLayout btn_back;
    private Button btn_send;
    private EditText edt_msg;
    private RecyclerViewPositionHelper recyclerViewPositionHelper;
    private boolean isLoading = false;
    private static int page = 1;
    private static int totalPage;
    public static boolean isfromvideoFromFragment = true;
    private RelativeLayout rl_video_view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_embeded_video, container, false);
        MainActivity.removeAd();
        MainActivity.bottomViewPlayer.setVisibility(View.GONE);
        isfromvideoFromFragment = true;
        EmbededVideoActivity.isfromEmbededVideoActivity = false;
        LoginVideoActivity.isfromLoginVideoActivity = false;
        skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
        skip_user_id = PrefsUtils.getFromPrefs(context, Const.SKIP_USERID, "");
        init(v);
        rl_video_view.setVisibility(View.VISIBLE);
        if (CheckNetwork.isInternetAvailable(context)) {
            getVideoApi();
            sliderData();
            listener();
            video_id = Prefs.getPrefInstance().getValue(context, Const.VIDEO_ID, "");
            Log.d("mytag", "video id is in video Login activity is---" + video_id);
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        return v;
    }

    private void init(View rootView) {
        video_player = rootView.findViewById(R.id.video_player);
        iv_like = rootView.findViewById(R.id.iv_like);
        iv_dislike = rootView.findViewById(R.id.iv_dislike);
        tv_like = rootView.findViewById(R.id.tv_like);
        tv_dislike = rootView.findViewById(R.id.tv_dislike);
        rl_share = rootView.findViewById(R.id.rl_share);
        btn_back = rootView.findViewById(R.id.btn_back);
        btn_send = rootView.findViewById(R.id.btn_send);
        edt_msg = rootView.findViewById(R.id.edt_msg);
        customViewPager = rootView.findViewById(R.id.customViewPager);
        rv_message_board = rootView.findViewById(R.id.rv_message_board);
        rl_video_view = rootView.findViewById(R.id.rl_video_view);

        layoutManager = new LinearLayoutManager(context);
        rv_message_board.setLayoutManager(layoutManager);
        recyclerViewPositionHelper = RecyclerViewPositionHelper.createHelper(rv_message_board);

//        page = 0;
//        messageBoardData = new ArrayList<>();
//        adapterUserComment = new AdapterUserComment(context, messageBoardData);
//        rv_message_board.setAdapter(adapterUserComment);
//        adapterUserComment.notifyDataSetChanged();
//        getMessageDataApi(page);
    }

    @SuppressLint("ValidFragment")
    public videoFromFragment(Context context) {
        videoFromFragment.context = context;
    }


    public static void runOnUIThread(Runnable runnable) {
        runOnUIThread(runnable, 0);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        if (delay == 0) {
            ApplicationDMPlayer.applicationHandler.post(runnable);
        } else {
            ApplicationDMPlayer.applicationHandler.postDelayed(runnable, delay);
        }
    }

    public static void updateChat() {
        runOnUIThread(() -> {
            page = 0;
            messageBoardData = new ArrayList<>();
            adapterUserComment.clear();
            getMessageDataApi(page);
        });
    }

    private void listener() {
        rv_message_board.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) rv_message_board.getLayoutManager();
                    int total = layoutManager.getItemCount();
                    int currentLastItem = recyclerViewPositionHelper.findLastCompletelyVisibleItemPosition() + 1;
                    int firstVisibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
                    Log.d("mytag", "firstVisibleItemPosition:- " + firstVisibleItemPosition);
                    Log.d("mytag", "currentLastItem :- " + currentLastItem);
                    Log.d("mytag", "total :- " + total);
                    Log.d("mytag", "totalPage :- " + totalPage);
                    Log.d("mytag", "page :- " + page);
                    if (page <= totalPage) {
                        isLoading = true;
                        page++;
                        getMessageDataApi(page);
                    }
//                    }
                }
            }
        });



        iv_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    callLikeApi(video_like_status);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        iv_dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    callDisLikeApi(video_like_status);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                releasePlayer();
                MainActivity.VisibleAd();
                MainActivity.bottomViewPlayer.setVisibility(View.VISIBLE);
                Fragment frag = new MenuFragment(context);
                FragmentManager fm = ((MainActivity) context).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();

            }
        });

        rl_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = ("Embedded Video Link : " + embedded_video_link + "\n\n" + "App Name :" + "DJ Sisko");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = edt_msg.getText().toString().trim();
                if (!TextUtils.isEmpty(msg)) {
                    if (Utils.getInstance().isConnectivity(context)) {
                        sendMessage(msg);
                    } else {
                        Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Please Enter Some Text", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void initializePlayer() {
        rl_video_view.setVisibility(View.GONE);
        player = ExoPlayerFactory.newSimpleInstance(context);
        video_player.setPlayer(player);
        video_player.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        video_player.setUseController(false);
        player.setPlayWhenReady(true);
        player.seekTo(0);

        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setUsage(C.USAGE_MEDIA)
                .setContentType(C.CONTENT_TYPE_MOVIE)
                .build();
        if (player.getAudioComponent() != null)
            player.getAudioComponent().setAudioAttributes(audioAttributes, true);
//        embedded_video_link = "http:\\/\\/techslides.com\\/demos\\/sample-videos\\/small.mp4";

        embedded_video_link = Prefs.getPrefInstance().getValue(context, Const.VIDEO_LINK, "");
        Log.d("mytag", "video link isssssssssssssss----------" + embedded_video_link);
        try {
            if (!embedded_video_link.equals("")) {
                Uri uri = Uri.parse(embedded_video_link);
                MediaSource mediaSource = buildMediaSource(uri, null);
                player.prepare(mediaSource, true, true);
            }
        } catch (Exception e) {

        }
        player.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {
                    case Player.STATE_READY:
                        if (playWhenReady) {
//                            callVideoWatchAPI();
                        }
                        break;
                    case Player.STATE_ENDED:
                        break;
                }
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                rl_video_view.setVisibility(View.VISIBLE);
                switch (error.type) {
                    case TYPE_SOURCE:
                        try {
//                            getWindow().getDecorView().findViewById(android.R.id.content);
//                            rootView = getWindow().getDecorView().getRootView();
                            Snackbar snackbar = Snackbar.make(rootView, "This video can't be played right now. Please try again later.", Snackbar.LENGTH_SHORT);
                            snackbar.show();
                        } catch (Exception e) {

                        }
                        break;
                    case TYPE_RENDERER:
                        player.retry();
                        break;
                    case TYPE_UNEXPECTED:
                        player.retry();
                        break;
                }
            }
        });
    }

    private MediaSource buildMediaSource(Uri uri, @Nullable String overrideExtension) {
        @C.ContentType int type = Util.inferContentType(uri, overrideExtension);
        switch (type) {
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(new DefaultDataSourceFactory(context, new DefaultHttpDataSourceFactory(Util.getUserAgent(context, context.getResources().getString(R.string.app_name)))))
                        .createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(new DefaultDataSourceFactory(context, new DefaultHttpDataSourceFactory(Util.getUserAgent(context, context.getResources().getString(R.string.app_name)))))
                        .createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(new DefaultDataSourceFactory(context, new DefaultHttpDataSourceFactory(Util.getUserAgent(context, context.getResources().getString(R.string.app_name)))))
                        .createMediaSource(uri);
            case C.TYPE_OTHER:
                DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory()
                        .setConstantBitrateSeekingEnabled(true);
                return new ExtractorMediaSource.Factory(new DefaultHttpDataSourceFactory(Util.getUserAgent(context, context.getResources().getString(R.string.app_name))))
                        .setExtractorsFactory(extractorsFactory)
                        .createMediaSource(uri);
            default: {
                throw new IllegalStateException("Unsupported type: " + type);
            }
        }
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean("playWhenReady", playWhenReady);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((Activity) context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Util.SDK_INT > 23) {
            if (video_player != null) {
                video_player.onResume();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((Activity) context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        hideSystemUi();
        if ((Util.SDK_INT <= 23 || player == null)) {
            if (video_player != null) {
                video_player.onResume();
            }
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onDestroy() {
        ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onDestroy();
    }

    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            video_player.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | SYSTEM_UI_FLAG_FULLSCREEN
                    | SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        else
            video_player.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
    }

    private void getVideoApi() {
        if (CheckNetwork.isInternetAvailable(ApplicationDMPlayer.applicationContext)) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("loading");
            mProgressDialog.show();
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
            Log.d("mytag", "skip status is in video activity is---" + skip_status);

            String user_id = PrefsUtils.getFromPrefs(context, Const.USERID, "");
            if (skip_status.equals("0")) {
                skip_user_id = PrefsUtils.getFromPrefs(context, Const.SKIP_USERID, "");
                call = apiInterface.GetVideoApi(skip_user_id);
                Log.d("mytag", "skip skip_user_id is in video activity is---" + skip_user_id);
            } else {
                call = apiInterface.GetVideoApi(user_id);
            }
            call.enqueue(new Callback<Video>() {

                @Override
                public void onResponse(Call<Video> call, retrofit2.Response<Video> response) {
                    if (response.isSuccessful() && response != null) {
                        if (response.body().getStatus() == 1) {
                            videoData = response.body().getData();
                            Log.d("mytag", "video responsessssssis-------" + response);
                            video_id = videoData.get(0).getEmbeddedVideoId();
                            Log.d("mytag", "video_id ssssssis-------" + video_id);
                            Prefs.getPrefInstance().setValue(context, Const.VIDEO_ID, video_id);
                            String embedded_video_name = videoData.get(0).getEmbeddedVideoName();
                            embedded_video_link = videoData.get(0).getEmbeddedVideoLink();
                            Log.d("mytag", "embedded_video_link is-------" + embedded_video_link);
                            Prefs.getPrefInstance().setValue(context, Const.VIDEO_LINK, embedded_video_link);
                            video_like_status = videoData.get(0).getLikeStatus();


                            if (Utils.getInstance().isConnectivity(context)) {
                                initializePlayer();
                            } else {
                                Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                            }


                            adapterUserComment = new AdapterUserComment(context, messageBoardData);
                            rv_message_board.setAdapter(adapterUserComment);
                            adapterUserComment.notifyDataSetChanged();
                            getMessageDataApi(page);

                            if (video_like_status.equals("1")) {
                                iv_like.setImageResource(R.drawable.like_red);
                            } else if (video_like_status.equals("0")) {
                                iv_dislike.setImageResource(R.drawable.dislike_red);
                            }

                            like_count = videoData.get(0).getLikeCount();
                            dislike_count = videoData.get(0).getDislikeCount();
                            tv_like.setText((String.valueOf(like_count)));
                            tv_dislike.setText((String.valueOf(dislike_count)));
                        } else {
                            mProgressDialog.dismiss();
                        }
                    } else {
                        mProgressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Video> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void callLikeApi(String like_status) {
        if (Utils.getInstance().isConnectivity(context)) {

            new AsyncTask<Void, Void, String>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    mProgressDialog = new ProgressDialog(context);
                    mProgressDialog.setMessage("Loading");
                    mProgressDialog.setCanceledOnTouchOutside(false);
                    mProgressDialog.setIndeterminate(true);
                    mProgressDialog.setCancelable(true);
                    mProgressDialog.show();
                }

                @Override
                protected String doInBackground(Void... voids) {
                    String response = null;
                    JSONObject request = new JSONObject();
                    try {
                        String user_id = PrefsUtils.getFromPrefs(context, Const.USERID, "");
                        skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
                        if (skip_status.equals("0")) {
                            request.put("user_id", skip_user_id);
                        } else {
                            request.put("user_id", user_id);
                        }
                        request.put("embedded_video_id", video_id);
                        request.put("video_like_status", "1");
                        response = WebInterface.getInstance().doPostRequest(Const.VIDEO_lIKE, request.toString());
                    } catch (Exception e) {
                    }
                    Utils.getInstance().d("Like Video REsponse : " + response);
                    return response;
                }

                @Override
                protected void onPostExecute(String response) {
                    super.onPostExecute(response);
                    mProgressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                            String user_id = jsonObject.getString("user_id");
                            int video_id = jsonObject.getInt("embedded_video_id");
                            int like_status = jsonObject.getInt("like_status");
                            int unlike_status = jsonObject.getInt("unlike_status");
                            int like_count = jsonObject.getInt("like_count");
                            Log.d("mytag", "total likes " + like_count);
                            int unlike_count = jsonObject.getInt("unlike_count");
                            if (like_status == 0) {
                                iv_like.setImageResource(R.drawable.video_like);
                            } else {
                                iv_like.setImageResource(R.drawable.like_red);
                            }
                            iv_dislike.setImageResource(R.drawable.video_dislike);
                            tv_like.setText(String.valueOf(like_count));
                            tv_dislike.setText(String.valueOf(unlike_count));
                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }.execute();
        } else {
            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void callDisLikeApi(String like_status) {
        if (Utils.getInstance().isConnectivity(context)) {

            new AsyncTask<Void, Void, String>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    mProgressDialog = new ProgressDialog(context);
                    mProgressDialog.setMessage("Loading");
                    mProgressDialog.setCanceledOnTouchOutside(false);
                    mProgressDialog.setIndeterminate(true);
                    mProgressDialog.setCancelable(true);
                    mProgressDialog.show();
                }

                @Override
                protected String doInBackground(Void... voids) {
                    String response = null;
                    JSONObject request = new JSONObject();
                    try {
                        String user_id = PrefsUtils.getFromPrefs(context, Const.USERID, "");
                        skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
                        if (skip_status.equals("0")) {
                            request.put("user_id", skip_user_id);
                        } else {
                            request.put("user_id", user_id);
                        }
                        request.put("embedded_video_id", video_id);
                        request.put("video_like_status", "0");
                        response = WebInterface.getInstance().doPostRequest(Const.VIDEO_lIKE, request.toString());
                    } catch (Exception e) {
                    }
                    Utils.getInstance().d("Like Video REsponse : " + response);
                    return response;
                }

                @Override
                protected void onPostExecute(String response) {
                    super.onPostExecute(response);
                    mProgressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                            String user_id = jsonObject.getString("user_id");
                            int video_id = jsonObject.getInt("embedded_video_id");
                            int like_status = jsonObject.getInt("like_status");
                            int unlike_status = jsonObject.getInt("unlike_status");
                            int like_count = jsonObject.getInt("like_count");
                            Log.d("mytag", "total likes " + like_count);
                            int unlike_count = jsonObject.getInt("unlike_count");
                            if (unlike_status == 0) {
                                iv_dislike.setImageResource(R.drawable.video_dislike);
                            } else {
                                iv_dislike.setImageResource(R.drawable.dislike_red);
                            }
                            iv_like.setImageResource(R.drawable.video_like);
                            tv_like.setText(String.valueOf(like_count));
                            tv_dislike.setText(String.valueOf(unlike_count));
                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }.execute();
        } else {
            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
        }
    }

    private void startImageSliderTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {

                }
            }
            progressTimer = new Timer();
            progressTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (sync) {
                        runOnUIThread(new Runnable() {
                            @Override
                            public void run() {

                                int currentItem = customViewPager.getCurrentItem() + 1;
                                int totalItem = adapterViewPager.getCount();
                                if (currentItem == totalItem) {
                                    customViewPager.setCurrentItem(0);
                                } else {
                                    customViewPager.setCurrentItem(currentItem);
                                }
                            }
                        });
                    }
                }
            }, 0, 4000);
        }
    }

    private static void getMessageDataApi(int page) {
        Log.d("mytag", "page number is-----" + page);
        if (CheckNetwork.isInternetAvailable(ApplicationDMPlayer.applicationContext)) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Log.d("mytag", "video id in retrofit call" + video_id);
            Call<MessageBoard> call = apiInterface.GetMessageFromApi(page, video_id);
            call.enqueue(new Callback<MessageBoard>() {
                @Override
                public void onResponse(Call<MessageBoard> call, retrofit2.Response<MessageBoard> response) {
                    if (response.isSuccessful() && response != null) {
                        int status = response.body().getStatus();
                        String msg = response.body().getMessage();
                        // messageBoardData = new ArrayList<>();
                        try {
                            messageBoardData = new ArrayList<>();
                            messageBoardData.addAll(response.body().getData());
                        }catch (Exception e){

                        }
                        if (status == 1) {
                            adapterUserComment.add(response.body().getData());
                            totalPage = response.body().getLastPage();
                        } else {
                            totalPage = 0;
                        }
                    } else {
                    }
                }

                @Override
                public void onFailure(Call<MessageBoard> call, Throwable t) {
                }
            });
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void sendMessage(String user_comment) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    String user_id = PrefsUtils.getFromPrefs(context, Const.USERID, "");
                    JSONObject jsonObject = new JSONObject();
                    skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
                    if (skip_status.equals("0")) {
                        jsonObject.put("user_id", skip_user_id);
                    } else {
                        jsonObject.put("user_id", user_id);
                    }
                    jsonObject.put("video_id", video_id);
                    jsonObject.put("user_comment", user_comment);
                    Utils.getInstance().d("mytag put is -------" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.ADDCOMMENT, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Utils.getInstance().d("mytag" + response);
                mProgressDialog.dismiss();
                if (response == null) {
                    Toast.makeText(context, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                            String user_id = jsonObject.getString("user_id");
                            String video_id = jsonObject.getString("video_id");
                            String user_comment = jsonObject.getString("user_comment");
                            edt_msg.getText().clear();
                            Log.d("mytag", "page is in send message api --------------" + page);

                            page = 0;
                            messageBoardData = new ArrayList<>();
                            adapterUserComment.clear();
                            getMessageDataApi(page);
//                            adapterUserComment = new AdapterUserComment(context, messageBoardData);
//                            rv_message_board.setAdapter(adapterUserComment);
//                            adapterUserComment.notifyDataSetChanged();
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }


    @SuppressLint("StaticFieldLeak")
    private void sliderData() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    response = WebInterface.getInstance().doGet(Const.SLIDER);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Utils.getInstance().d("mytag" + response);
                mProgressDialog.dismiss();
                if (response == null) {
                    Toast.makeText(context, "Response null", Toast.LENGTH_LONG).show();
                    Log.d("Slider Response is:", "null");
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            JSONArray data = jsonObject.getJSONArray("data");

                            imageArraylist1 = new ArrayList<>();
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject slider_image_data = data.optJSONObject(i);
                                String sponsor_slide_name = slider_image_data.getString("sponsor_slide_name");
                                String sponsor_slider_image = slider_image_data.getString("sponsor_slider_image");
                                String sponsor_slider_link = slider_image_data.getString("sponsor_slider_link");
                                imageArraylist1.add(new ImageSliderModel(sponsor_slide_name, sponsor_slider_image, sponsor_slider_link));
                                Log.d("mainActivity", "Size is:" + sponsor_slider_image);
                            }
                        }
                        adapterViewPager = new AdapterViewPager(imageArraylist1, context);
                        customViewPager.setAdapter(adapterViewPager);
                        startImageSliderTimer();
                        System.out.println(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }
}
