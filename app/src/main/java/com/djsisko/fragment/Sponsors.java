package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.adapter.SponsorAdapter;
import com.djsisko.data.sponsors;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class Sponsors extends Fragment {

    Context context;
    SponsorAdapter dataAdapter;
    ProgressDialog mProgressDialog;
    RecyclerView sponsorRecycler;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<sponsors> data = new ArrayList<>();
    ImageView backButton1,playerButton1;
    Fragment frag;
    FragmentManager fm;
    FragmentTransaction ft;
    SharedPreferences preferences;
    String androidId;
    Boolean isConnected;

    public Sponsors() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Sponsors(Context context)
    {
        this.context = context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_sponsors, container, false);
        preferences = context.getSharedPreferences("MyAndroidId",Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId","");
        isConnected = preferences.getBoolean("isConnected",false);
        Log.d("Sponsors","Android id: "+androidId);
        Log.d("Sponsors","Net is connected: "+isConnected);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        sponsorRecycler = (RecyclerView)v.findViewById(R.id.sponsorRecycler);
        layoutManager = new LinearLayoutManager(context);
        sponsorRecycler.setLayoutManager(layoutManager);
        if (CheckNetwork.isInternetAvailable(context))
        {
        data.clear();
        new sponsorsAsync().execute();}
        else
        {
            Toast.makeText(context,"No Internet Conncetion",Toast.LENGTH_SHORT).show();
        }
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new MenuFragment(context);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        return v;
    }

    //http://durisimomobileapps.net/djsisko/api/sponsors
    private class sponsorsAsync extends AsyncTask<String,Void,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/sponsors";
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            Log.d("mytag", "Sponsors Response : " + s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                int status = jsonObject.getInt("status");
                if(status == 0){
                    Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                }
                JSONArray jsonArray = jsonObject.getJSONArray("data");
            if(jsonArray.length() == 0){
                Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
            }
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject idJson = jsonArray.getJSONObject(i);
                    int id = idJson.getInt("sponsor_id");
                    Log.d("Sponsors","Id is:"+id);
                    JSONObject nameJson = jsonArray.getJSONObject(i);
                    String name = nameJson.getString("sponsor_name");
                    Log.d("Sponsors","Name is:"+name);
                    JSONObject descrJson = jsonArray.getJSONObject(i);
                    String descr = descrJson.getString("sponsor_description");
                    Log.d("Sponsors","description is:"+descr);
                    JSONObject addrJson = jsonArray.getJSONObject(i);
                    String address = addrJson.getString("sponsor_address");
                    Log.d("Sponsors","Address is:"+address);
                    JSONObject logoJson = jsonArray.getJSONObject(i);
                    String logo = logoJson.getString("sponsor_logo");
                    Log.d("Sponsors","logo is:"+logo);
                    JSONObject bannerJson = jsonArray.getJSONObject(i);
                    String banner = bannerJson.getString("sponsor_banner");
                    Log.d("Sponsors","banner is:"+banner);
                    data.add(new sponsors(id,name,descr,address,logo,banner));
                }
                try
                {
                    dataAdapter = new SponsorAdapter(context,data);
                    sponsorRecycler.setAdapter(dataAdapter);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                }
             catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



}
