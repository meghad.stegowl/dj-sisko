package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.djsisko.R;
import com.djsisko.activities.MainActivity;
import com.djsisko.data.EventsData;
import com.google.android.gms.maps.GoogleMap;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class Events2Fragment extends Fragment {

    Context context;
    EventsData current;
    String title;
    ImageView calImage;
    TextView location, place, dateTimeText, time;
    WebView description;
    double lat = 0, longt = 0;
    String latitude, longitude;
    Button map_location;
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    String tag;
    GoogleMap gMap;
    TextView menuListHeading;
    ImageView backButton1, playerButton1;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    ScrollView scrollview;
    LocationListener listener;

    public Events2Fragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Events2Fragment(Context context, String title, EventsData current, String longitude, String latitude, String tag) {
        this.context = context;
        this.current = current;
        this.title = title;
        this.longitude = longitude;
        this.latitude = latitude;
        Log.d("Longitude Constructor", longitude);
        Log.d("Latitude Constructor", latitude);
        this.tag = tag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_events2, container, false);
        scrollview = v.findViewById(R.id.scr);
        scrollview.setSmoothScrollingEnabled(true);
//        if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.M))
//        {
//            checkPermission();
//        }
        String provider = android.provider.Settings.Secure.getString(getContext().getContentResolver(), android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (!provider.contains("gps")) {
            Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(gpsOptionsIntent);
        }
        //initializeMap();
        menuListHeading = v.findViewById(R.id.menuListHeading);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        menuListHeading.setText(title);
        menuListHeading.setSelected(true);
        menuListHeading.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        menuListHeading.setSingleLine(true);
        preferences = context.getSharedPreferences("MyAndroidId", Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId", "");
        isConnected = preferences.getBoolean("isConnected", false);
        Log.d("Events2", "Android id: " + androidId);
        Log.d("Events2", "Net is connected: " + isConnected);
        calImage = v.findViewById(R.id.calImage);
        location = v.findViewById(R.id.location);
        place = v.findViewById(R.id.place);
        place.setSelected(true);
        dateTimeText = v.findViewById(R.id.dateTimeText);
        time = v.findViewById(R.id.time);
        map_location = v.findViewById(R.id.map_location);
        description = v.findViewById(R.id.description);
        Glide.with(context)
                .load(current.geteImage())
                .placeholder(R.drawable.big1logo)
                .error(R.drawable.big1logo)
                .into(calImage);
        location.setText(current.geteTitle());
        place.setText(current.geteAddress());
        try {
            Geocoder geocoder = new Geocoder(getContext());
            Log.d("mytag", current.geteAddress());
            List<Address> searchList = geocoder.getFromLocationName(current.geteAddress(), 1);
            if (searchList != null && !searchList.isEmpty()) {
                Address address = searchList.get(0);
                latitude = String.valueOf(address.getLatitude());
                longitude = String.valueOf(address.getLongitude());
//                getdirection.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri
//                                .parse("http://maps.google.com/maps?daddr=" + latt + "," + lngg));
//                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
//                        final ComponentName cn = new ComponentName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//                        intent.setComponent(cn);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                    }
//                });
            } else {
                map_location.setVisibility(View.GONE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String date = current.geteStart();
        String time = current.getTime();
        String date_time = date + " at " + time;
        dateTimeText.setText(date_time);
        String desc = current.geteDescription();
        //  time.setText(current.getTime());
        description.getSettings().setJavaScriptEnabled(true);
        description.setBackgroundColor(context.getResources().getColor(R.color.darkGrey));

        description.loadDataWithBaseURL(null, desc, "text/html", "utf-8", null);
        description.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                view.loadUrl("javascript:document.body.style.setProperty(\"color\", \"white\");"
                );
            }
        });

        // description.setText(Html.fromHtml(current.geteDescription()));
//        SupportMapFragment mapFragment1 = (SupportMapFragment) findFragmentById(R.id.map);
//        mapFragment1.getMapAsync(Events2Fragment.this);
        map_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String provider = android.provider.Settings.Secure.getString(getContext().getContentResolver(), android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                if ((Double.valueOf(longitude) == null && Double.valueOf(latitude) == 0) || !provider.contains("gps")) {
                    Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(gpsOptionsIntent);
                } else {
                    Log.d("", "ELSE");
                    float destiLat = Float.parseFloat(String.valueOf(latitude));
                    Log.d("", "lat(" + latitude + ")");
                    float destiLong = Float.parseFloat(String.valueOf(longitude));
                    Log.d("", "long(" + longitude + ")");
                    String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f", Double.valueOf(latitude), Double.valueOf(longitude), destiLat, destiLong);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                }
            }
        });
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new Events(context);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(tag);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });

        return v;
    }
}
