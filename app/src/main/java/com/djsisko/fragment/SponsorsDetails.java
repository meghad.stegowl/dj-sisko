package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.adapter.SponsorImageAdapter;
import com.djsisko.data.sponsors;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class SponsorsDetails extends Fragment {

    Context context;
    int id;
    ArrayList<sponsors> imagesData = new ArrayList<>();
    SharedPreferences preferences;
    String androidId;
    Boolean isConnected;
    String sponsorName, sponsorAdd,description,logo,sTitle;
    TextView title,address,descriptionData,menuListHeading;
    RecyclerView detailsRecycler;
    RecyclerView.LayoutManager layoutManager;
    ImageView backButton1,playerButton1,logoImage;
    Fragment frag;
    FragmentManager fm;
    FragmentTransaction ft;
    SponsorImageAdapter dataAdapter;
    NestedScrollView scrollview;
    ProgressDialog mProgressDialog;


    public SponsorsDetails() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public SponsorsDetails(Context context,int id,String sTitle)
    {
        this.context = context;
        this.id = id;
        this.sTitle = sTitle;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_sponsors_details, container, false);
        scrollview = v.findViewById(R.id.scrollview);
        scrollview.setSmoothScrollingEnabled(true);
        title = v.findViewById(R.id.title);
        address = v.findViewById(R.id.address);
        descriptionData = v.findViewById(R.id.descriptionData);
        menuListHeading = v.findViewById(R.id.menuListHeading);

        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        logoImage = v.findViewById(R.id.logo);
        detailsRecycler = v.findViewById(R.id.detailsRecycler);
        detailsRecycler.setNestedScrollingEnabled(false);
        detailsRecycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        detailsRecycler.setLayoutManager(layoutManager);
        preferences = context.getSharedPreferences("MyAndroidId",Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId","");
        isConnected = preferences.getBoolean("isConnected",false);
        Log.d("SponsorsDetails","Android id: "+androidId);
        Log.d("SponsorsDetails","Net is connected: "+isConnected);
        JSONObject object = new JSONObject();
        try {
            object.put("id",id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (CheckNetwork.isInternetAvailable(context)){
        new detailsAsync(object).execute();}
        else
        {
            Toast.makeText(context,"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
        menuListHeading.setText(sTitle);
        menuListHeading.setSelected(true);
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new Sponsors(context);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();

            }
        });
        return v;
    }

    //http://durisimomobileapps.net/djsisko/api/getSponsor
    private class detailsAsync extends AsyncTask<String,Void,String>
    {
        JSONObject jsonObject;
        public detailsAsync(JSONObject jsonObject)
        {
            this.jsonObject = jsonObject;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            try
            {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/getSponsor";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(jsonObject));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch(Exception e)
            {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            Log.d("Sponsors Details","Response is:"+s);
            if (!s.equals(null))
            {
                try {
                    JSONObject object = new JSONObject(s);
                    JSONObject data = object.getJSONObject("data");
                     sponsorName = data.getString("sponsor_name");
                    Log.d("SponsorDetails","name is:"+sponsorName);
                     sponsorAdd = data.getString("sponsor_address");
                    Log.d("SponsorDetails","address is:"+sponsorAdd);
                     description = data.getString("sponsor_description");
                    Log.d("SponsorDetails","description is:"+description);
                     logo = data.getString("sponsor_logo");
                    Log.d("SponsorDetails","logo is:"+logo);
                    title.setText(sponsorName);
                    title.setSelected(true);
                    descriptionData.setText(Html.fromHtml(description));
                    address.setText(sponsorAdd);
                    Glide.with(context)
                            .load(logo)
                            .placeholder(R.drawable.big1logo)
                            .error(R.drawable.big1logo)
                            .into(logoImage);
                    JSONArray images = object.getJSONArray("inner_images");
                    for (int i = 0; i < images.length();i++)
                    {
                        String image = images.getString(i);
                        Log.d("SponsorsDetails","Image is:"+image);
                        imagesData.add(new sponsors(image));
                    }
                    dataAdapter = new SponsorImageAdapter(context,imagesData);
                  detailsRecycler.setAdapter(dataAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

}
