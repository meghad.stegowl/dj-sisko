package com.djsisko.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.data.Song;
import com.djsisko.phonemidea.CheckNetwork;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShareMyApp extends Fragment {

    Context context;
    ProgressDialog mProgressDialog;
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    ImageButton shareApp;
    ArrayList<Song> data = new ArrayList<>();
    ImageView backButton1,playerButton1;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    String tag = ShareMyApp.class.getSimpleName();
    String msg,finalLink;

    public ShareMyApp() {
        // Required empty public constructor
    }
    @SuppressLint("ValidFragment")
    public ShareMyApp(Context context) {
        this.context = context;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View v  = inflater.inflate(R.layout.fragment_share_my_app, container, false);
        shareApp = v.findViewById(R.id.shareApp);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        preferences = context.getSharedPreferences("MyAndroidId",Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId","");
        isConnected = preferences.getBoolean("isConnected",false);
        Log.d("ShareMyApp","Android id: "+androidId);
        Log.d("ShareMyApp","Net is connected: "+isConnected);
        if (CheckNetwork.isInternetAvailable(context)){
            data.clear();
            new sharemyAsyncTask().execute();
        }
        else
        {
            Toast.makeText(context,"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
        shareApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareLink();
            }

        });
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new MenuFragment(context);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            MainActivity.setPlayLayout();
            }
        });
        return v;
    }

    public void shareLink()
    {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        share.putExtra(Intent.EXTRA_TEXT,finalLink);
        startActivity(Intent.createChooser(share, "Share link!"));
    }


//    http://durisimomobileapps.net/djbambino/api/share
    private class sharemyAsyncTask extends AsyncTask<String,Void,String>
    {
    private JSONObject jsonObject;
    public sharemyAsyncTask() {}
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }

        @Override
    protected String doInBackground(String... params) {
        String result = null;
        try {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            String url = "http://durisimomobileapps.net/djsisko/api/share";
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
             result = response.body().string();
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        mProgressDialog.dismiss();
            Log.d("Share My App", "Response is:" + s);
            if (s == null) {
                Log.d("mytag", "Share My App Response is null");
            } else {
                try
                {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                int shareId = jsonObject1.getInt("share_id");
                Log.d("Share My App", "Share Id is:" + shareId);
                String androidLink = jsonObject1.getString("android_link");
                String alinks = jsonObject1.getString("android_link");
                String iosLink = jsonObject1.getString("ios_link");
                Log.d("Share My App", "Android Link:" + androidLink);
                String text = jsonObject1.getString("text");
                data.add(new Song(finalLink));
                msg = text + "\n";
                finalLink = text + "\n\n" + alinks + "\n\n" + iosLink ;
            } catch(JSONException e){
                e.printStackTrace();
            }
        }
        System.out.println(s);
    }
}
}
