package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.djsisko.R;
import com.djsisko.activities.MainActivity;
import com.djsisko.data.Song;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class SocialMedia extends Fragment {

    Context context;
    ProgressDialog mProgressDialog;
    ArrayList<Song> data = new ArrayList<>();
    ImageView facebook,twitter,youtube,instagram;
    String facebookString,twitterString,youtubeString,instagramString;
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    ImageView backButton1,playerButton1;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    String tag = SocialMedia.class.getSimpleName();
    MediaPlayer radioPlayer;
    LinearLayout adView;


    public SocialMedia() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public SocialMedia(Context context)
    {
        this.context = context;
    }

    @SuppressLint("ValidFragment")
    public SocialMedia(Context context,MediaPlayer radioPlayer,LinearLayout adView)
    {
        this.context = context;
        this.radioPlayer= radioPlayer;
        this.adView = adView;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v  =  inflater.inflate(R.layout.fragment_social_media, container, false);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        preferences = context.getSharedPreferences("MyAndroidId",Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId","");
        isConnected = preferences.getBoolean("isConnected",false);
        Log.d("SocialMedia","Android id: "+androidId);
        Log.d("SocialMedia","Net is connected: "+isConnected);
        facebook = v.findViewById(R.id.facebook);
        twitter = v.findViewById(R.id.twitter);
        youtube = v.findViewById(R.id.youtube);
        instagram = v.findViewById(R.id.instagram);
        if (CheckNetwork.isInternetAvailable(context))
        {
            data.clear();
            new SocialAsyncTask().execute();
        }
         else
        {
            Toast.makeText(context,"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new MenuFragment(context);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        facebook.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                frag = new socialLink(context,"FACEBOOK",facebookString);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new socialLink(context,"TWITTER",twitterString);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new youtubeFragment(context,"YOUTUBE",youtubeString,radioPlayer,adView);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new socialLink(context,"INSTAGRAM",instagramString);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        return v;
    }

    //http://durisimomobileapps.net/djsisko/api/social_links
    public class SocialAsyncTask extends AsyncTask<String,Void,String>
    {
        private JSONObject jsonObject;
        public SocialAsyncTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/social_links";
                OkHttpClient client = new OkHttpClient();
//               RequestBody body = RequestBody.create(JSON, String.valueOf(jsonObject));
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (Exception e) {
                return null;
            }
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            try {
                Log.d("SocialMedia","Response is:"+s);
                JSONObject jsonObject = new JSONObject(s);
                JSONArray jsonArray = jsonObject.getJSONArray("data");
            if(jsonArray.length() == 0){
                Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
            }
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject socialIdJson =jsonArray.getJSONObject(i);
                    int socialId = socialIdJson.getInt("social_id");
                    Log.d("SocialMedia","Social Id is:"+socialId);
                    JSONObject socialLinkJson =jsonArray.getJSONObject(i);
                    String socialLink = socialLinkJson.getString("social_link");
                    Log.d("SocialMedia","Social Link is:"+socialLink);
                    data.add(new Song(socialId,socialLink));
                }

                facebookString = data.get(0).getSongTitle();
                Log.d("Social Media","Facebook: "+facebookString);
                twitterString = data.get(1).getSongTitle();
                Log.d("Social Media","Twitter: "+twitterString);
                youtubeString = data.get(2).getSongTitle();
                Log.d("Social Media","Youtube: "+youtubeString);
                instagramString = data.get(3).getSongTitle();
                Log.d("Social Media","Instagram: "+instagramString);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            System.out.println(s);
        }
    }


}
