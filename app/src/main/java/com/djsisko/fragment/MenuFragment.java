package com.djsisko.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.djsisko.R;
import com.djsisko.Utility.Const;
import com.djsisko.Utility.PrefsUtils;
import com.djsisko.Utility.WebInterface;
import com.djsisko.activities.EmbededVideoActivity;
import com.djsisko.activities.LoginActivity;
import com.djsisko.activities.MainActivity;
import com.djsisko.phonemidea.Prefs;
import com.djsisko.phonemidea.Utils;

import org.json.JSONObject;

@SuppressLint("ValidFragment")
public class MenuFragment extends Fragment implements View.OnClickListener {

    Context context;
    ImageView backButton, playerButton;
    MediaPlayer radioPlayer;
    LinearLayout adView;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    TextView tv_Logout, tv_login;
    private ImageView iv_edit_prof;
    private RelativeLayout rl_profile;
    private ProgressDialog mProgressDialog;
    private LinearLayout ll_logout_login;
    private LinearLayout ll_tv_categories, ll_hit_singles, ll__guest_djs, ll_videos, ll_gallery, ll_events, ll_live_video,
            ll_live_chat, ll_favorites, ll_my_playlist, ll_social_media, ll_inbox, ll_bookings, ll_sign_up, ll_share, ll_sponsered_by;


    public MenuFragment() {
    }


    @SuppressLint("ValidFragment")
    public MenuFragment(Context context, MediaPlayer radioPlayer, LinearLayout adView) {
        this.context = context;
        this.radioPlayer = radioPlayer;
        this.adView = adView;
    }

    @SuppressLint("ValidFragment")
    public MenuFragment(Context context) {
        this.context = context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menu, container, false);

        backButton = v.findViewById(R.id.backButton);
        playerButton = v.findViewById(R.id.playerButton);
        iv_edit_prof = v.findViewById(R.id.iv_edit_prof);
        rl_profile = v.findViewById(R.id.rl_profile);
        tv_Logout = v.findViewById(R.id.tv_Logout);
        tv_login = v.findViewById(R.id.tv_login);
        ll_logout_login = v.findViewById(R.id.ll_logout_login);

        ll_tv_categories = v.findViewById(R.id.ll_tv_categories);
        ll_hit_singles = v.findViewById(R.id.ll_hit_singles);
        ll__guest_djs = v.findViewById(R.id.ll__guest_djs);
        ll_videos = v.findViewById(R.id.ll_videos);
        ll_gallery = v.findViewById(R.id.ll_gallery);
        ll_events = v.findViewById(R.id.ll_events);
        ll_live_video = v.findViewById(R.id.ll_live_video);
        ll_live_chat = v.findViewById(R.id.ll_live_chat);
        ll_favorites = v.findViewById(R.id.ll_favorites);
        ll_my_playlist = v.findViewById(R.id.ll_my_playlist);
        ll_social_media = v.findViewById(R.id.ll_social_media);
        ll_inbox = v.findViewById(R.id.ll_inbox);
        ll_bookings = v.findViewById(R.id.ll_bookings);
        ll_sign_up = v.findViewById(R.id.ll_sign_up);
        ll_share = v.findViewById(R.id.ll_share);
        ll_sponsered_by = v.findViewById(R.id.ll_sponsered_by);


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        playerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        getListener();
        listner();
        return v;

    }

    public void getListener() {
        ll_tv_categories.setOnClickListener(this);
        ll_hit_singles.setOnClickListener(this);
        ll__guest_djs.setOnClickListener(this);
        ll_videos.setOnClickListener(this);
        ll_gallery.setOnClickListener(this);
        ll_events.setOnClickListener(this);
        ll_live_video.setOnClickListener(this);
        ll_live_chat.setOnClickListener(this);
        ll_favorites.setOnClickListener(this);
        ll_my_playlist.setOnClickListener(this);
        ll_social_media.setOnClickListener(this);
        ll_inbox.setOnClickListener(this);
        ll_bookings.setOnClickListener(this);
        ll_sign_up.setOnClickListener(this);
        ll_share.setOnClickListener(this);
        ll_sponsered_by.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ll_tv_categories:
                frag = new MusicCategories(context, radioPlayer);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
                break;

            case R.id.ll_hit_singles:
                frag = new HitSingles(context, radioPlayer);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack("HitSingles");
                ft.commit();
                break;

            case R.id.ll__guest_djs:
                frag = new GuestDjs(context, radioPlayer);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack("FeaturedDj");
                ft.commit();
                break;

            case R.id.ll_videos:
                frag = new Videos(context, radioPlayer, adView);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack("Videos");
                ft.commit();
                break;

            case R.id.ll_gallery:
                frag = new Gallery(context);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack("Gallery");
                ft.commit();
                break;

            case R.id.ll_events:
                frag = new Events(context);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack("Events");
                ft.commit();
                break;

            case R.id.ll_live_video:
//                frag = new videoFromFragment(context);
//                fm = ((MainActivity) context).getSupportFragmentManager();
//                ft = fm.beginTransaction();
//                ft.replace(R.id.container, frag);
//                ft.addToBackStack(null);
//                ft.commit();
                Intent i = new Intent(context, EmbededVideoActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                break;

            case R.id.ll_live_chat:
                frag = new socialLink(context, "Live Chat", ((MainActivity) context).live_chat);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack("Live Chat");
                ft.commit();
                break;

            case R.id.ll_favorites:
                frag = new MyFavorites(context, radioPlayer);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack("MyFav");
                ft.commit();
                break;

            case R.id.ll_my_playlist:
                frag = new MyPlayList(context, radioPlayer);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack("MyPlayList");
                ft.commit();
                break;

            case R.id.ll_social_media:
                frag = new SocialMedia(context, radioPlayer, adView);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack("SocialMedia");
                ft.commit();
                break;

            case R.id.ll_inbox:
                frag = new Inbox(context);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack("Inbox");
                ft.commit();
                break;

            case R.id.ll_bookings:
                frag = new Bookings(context);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
                break;

            case R.id.ll_sign_up:
                frag = new SignUp(context);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
                break;

            case R.id.ll_share:
                frag = new ShareMyApp(context);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
                break;

            case R.id.ll_sponsered_by:
                frag = new Sponsors(context);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
                break;
            default:
                frag = new MenuFragment(context, radioPlayer, adView);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
                break;
        }
    }

    private void listner() {
        rl_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String skip_status = Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, "");
                Log.d("mytag", "skip status is in menu list frgmnet is---" + skip_status);

                if (skip_status.equals("0")) {
                    android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
                    alertDialog.setTitle(context.getResources().getString(R.string.app_name));
                    alertDialog.setMessage("Please login to continue.");
                    alertDialog.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface d, int which) {
                                    d.dismiss();
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("isUser", true);
                                    startActivity(intent);
                                }
                            });
                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                } else {
                    frag = new EditProfileFragment(context, adView);
                    fm = ((MainActivity) context).getSupportFragmentManager();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.container, frag);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });


        String skip_status = String.valueOf(Prefs.getPrefInstance().getValue(context, Const.SKIP_STATUS, ""));
        Log.d("mytag", "skip status is----" + skip_status);
        if (skip_status.equals("0")) {
            tv_Logout.setVisibility(View.GONE);
            tv_login.setVisibility(View.VISIBLE);
        }else{
            tv_Logout.setVisibility(View.VISIBLE);
            tv_login.setVisibility(View.GONE);
        }

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, LoginActivity.class);
                startActivity(i);
            }
        });

        tv_Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("Are you sure you want to Logout? ");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                PrefsUtils.saveToPrefs(context, Const.PREFS_LOGIN_USERNAME_KEY, "");
                                PrefsUtils.saveToPrefs(context, Const.PREFS_LOGIN_PASSWORD_KEY, "");
                                getLogoutApi();
                                Intent i = new Intent(context, LoginActivity.class);
                                startActivity(i);
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
    }


    @SuppressLint("StaticFieldLeak")
    private void getLogoutApi() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    String user_id = PrefsUtils.getFromPrefs(context, Const.USERID, "");
                    response = WebInterface.getInstance().doGet(Const.LOGOUT + "?logout=" + user_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Utils.getInstance().d("mytag" + response);
                mProgressDialog.dismiss();
                if (response == null) {
                    Toast.makeText(context, "Response is null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            Log.d("mytag", "response is--------" + response);
                            Prefs.getPrefInstance().setValue(context, Const.SKIP_STATUS, "0");
                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }


}
