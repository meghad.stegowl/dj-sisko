package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.adapter.PlayListAdapter;
import com.djsisko.data.Song;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.djsisko.activities.MainActivity.setPlayLayout;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyPlayList extends Fragment {

    Context context;
    String title;
    ImageView backButton1, playerButton1;
    RecyclerView myPlayListRecycler;
    RecyclerView.LayoutManager layoutManager;
    PlayListAdapter dataAdapter;
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    ArrayList<Song> data = new ArrayList<>();
    int fromMenu;
    int fromPlayer;
    ProgressDialog mProgressDialog;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    String tag = MyPlayList.class.getSimpleName();
    MediaPlayer radioPlayer;


    public MyPlayList() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public MyPlayList(Context context, MediaPlayer radioPlayer) {
        this.context = context;
        this.radioPlayer = radioPlayer;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my_play_list, container, false);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        myPlayListRecycler = (RecyclerView) v.findViewById(R.id.myPlayListRecycler);
        layoutManager = new LinearLayoutManager(context);
        myPlayListRecycler.setLayoutManager(layoutManager);
        preferences = context.getSharedPreferences("MyAndroidId", Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId", "");
        isConnected = preferences.getBoolean("isConnected", false);
        Log.d("MyPlayList", "Android id: " + androidId);
        Log.d("MyPlayList", "Net is connected: " + isConnected);
        JSONObject object = new JSONObject();
        try {
            object.put("u_id", androidId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (CheckNetwork.isInternetAvailable(context)) {
            data.clear();
            new MyPlayListAsyncTask(object).execute();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("mytag", " tag - " + getTag());
                if (Objects.equals(getTag(), "outer")) {
                    MainActivity.setPlayLayout();
                } else {
                    frag = new MenuFragment(context);
                    fm = ((MainActivity) context).getSupportFragmentManager();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.container, frag);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPlayLayout();
            }
        });
        return v;
    }

    //    http://durisimomobileapps.net/djsisko/api/playlist/user
    public class MyPlayListAsyncTask extends AsyncTask<String, Void, String> {

        private JSONObject jsonObject;

        public MyPlayListAsyncTask(JSONObject object) {
            this.jsonObject = object;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/playlist/user";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(jsonObject));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (Exception e) {
                return null;
            }
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            Log.d("mytag", "MyPlayList Response : " + s);
            if (s == null) {
                Log.d("mytag", "MyPlaylist Response is null");
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    int status = jsonObject.getInt("status");
                    if (status == 0) {
                        mProgressDialog.dismiss();
                        Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    if (jsonArray.length() == 0) {
                        mProgressDialog.dismiss();
                        Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
                    }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject idJson = jsonArray.getJSONObject(i);
                        int playListId = idJson.getInt("playlist_id");
                        Log.d("MyPlayList id is:", "" + playListId);
                        JSONObject userID = jsonArray.getJSONObject(i);
                        String userId = userID.getString("u_id");
                        Log.d("MyPlayList userId is:", userId);
                        JSONObject nameJson = jsonArray.getJSONObject(i);
                        String playListName = nameJson.getString("name");
                        Log.d("MyPlayList Name is:", playListName);
                        JSONObject totalCountJson = jsonArray.getJSONObject(i);
                        int totalCount = totalCountJson.getInt("total_count");
                        Log.d("MyPlayList totalCount", "" + totalCount);
//                int songId,String songTitle,int totalCount
                        data.add(new Song(playListId, playListName, totalCount));
                        dataAdapter = new PlayListAdapter(context, data, playListId, tag, radioPlayer);
                        myPlayListRecycler.setAdapter(dataAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            System.out.println(s);
        }
    }

}
