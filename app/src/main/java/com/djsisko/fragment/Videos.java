package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.djsisko.R;
import com.djsisko.activities.MainActivity;
import com.djsisko.adapter.VideoDataAdapter;
import com.djsisko.data.Song;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class Videos extends Fragment {

    Context context;
    ArrayList<Song> videoData = new ArrayList<>();
    VideoDataAdapter dataAdapter;
    RecyclerView videosRecycler;
    RecyclerView.LayoutManager layoutManager;
    ProgressDialog mProgressDialog;
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    ImageView backButton1, playerButton1;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    String tag = Videos.class.getSimpleName();
    MediaPlayer radioPlayer;
    LinearLayout adView;

    public Videos() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Videos(Context context, MediaPlayer radioPlayer, LinearLayout adView) {
        this.context = context;
        this.radioPlayer = radioPlayer;
        this.adView = adView;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        adView.setVisibility(View.GONE);
        View v = inflater.inflate(R.layout.fragment_videos, container, false);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        preferences = context.getSharedPreferences("MyAndroidId", Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId", "");
        isConnected = preferences.getBoolean("isConnected", false);
        Log.d("Videos", "Android id: " + androidId);
        Log.d("Videos", "Net is connected: " + isConnected);
        videosRecycler = v.findViewById(R.id.videosRecycler);
        layoutManager = new LinearLayoutManager(context);
        videosRecycler.setLayoutManager(layoutManager);
        JSONObject object = new JSONObject();
        try {
            object.put("u_id", androidId);

            Log.d("mytag", "Json Object : " + object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (CheckNetwork.isInternetAvailable(context)) {
            videoData.clear();
            new videosAsyncTask(object).execute();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                frag = new MenuFragment(context);
                fm = ((MainActivity) context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container, frag);
                ft.addToBackStack(null);
                ft.commit();
//                adView.setVisibility(View.VISIBLE);
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
//                adView.setVisibility(View.VISIBLE);
            }
        });
        return v;
    }

    //http://durisimomobileapps.net/djsisko/api/videos
    private class videosAsyncTask extends AsyncTask<String, Void, String> {
        JSONObject object;

        private videosAsyncTask(JSONObject object) {
            this.object = object;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/videos";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(object));
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (Exception e) {
                return null;
            }
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            try {
                Log.d("mytag", "Videos Response : " + s);
                JSONObject jsonObject = new JSONObject(s);
                int status = jsonObject.getInt("status");
                if (status == 0) {
                    Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                }
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                if (jsonArray.length() == 0) {
                    Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
                }
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject idJson = jsonArray.getJSONObject(i);
                    int id = idJson.getInt("video_id");
                    Log.d("Video id is", "" + id);
                    JSONObject videoNameJson = jsonArray.getJSONObject(i);
                    String vName = videoNameJson.getString("name");
                    Log.d("mytag", "Video Name IS " + vName);
                    JSONObject imageJson = jsonArray.getJSONObject(i);
                    String image = imageJson.getString("image");
                    Log.d("Video Image is", image);
                    JSONObject dateJson = jsonArray.getJSONObject(i);
                    String date = dateJson.getString("date");
                    Log.d("Duration is", date);
                    JSONObject videoLinkJson = jsonArray.getJSONObject(i);
                    String videoLink = videoLinkJson.getString("video_link");
                    Log.d("Video Fragment", videoLink);
                    videoData.add(new Song(id, image, vName, date, videoLink));
                }
                try {
                    dataAdapter = new VideoDataAdapter(context, videoData, tag, radioPlayer, adView);
                    videosRecycler.setAdapter(dataAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            System.out.println(s);
        }
    }

}
