package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.adapter.MusicCategoryAdapter;
import com.djsisko.data.DataSisko;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class MusicCategories extends Fragment {


    Context context;
    String title;
    ArrayList<DataSisko> data = new ArrayList<>();
    RecyclerView musicCatRecycler;
    RecyclerView.LayoutManager layoutManager;
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    MusicCategoryAdapter musicCategoryAdapter;
    ProgressDialog mProgressDialog;
    ImageView backButton1,playerButton1;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    String tag = MusicCategories.class.getSimpleName();
    MediaPlayer radioPlayer;
//    TextView menuListHeading;

    public MusicCategories() {
    }
    @SuppressLint("ValidFragment")
    public MusicCategories(Context context,MediaPlayer radioPlayer)
    {
        this.context = context;
        this.radioPlayer = radioPlayer;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        container.removeAllViews();
        View v = inflater.inflate(R.layout.fragment_music_categories, container, false);


        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
       musicCatRecycler = (RecyclerView)v.findViewById(R.id.musicCatRecycler);
        preferences = context.getSharedPreferences("MyAndroidId",Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId","");
        isConnected = preferences.getBoolean("isConnected",false);
        Log.d("MusicCategory","Android id: "+androidId);
        Log.d("MusicCategory","Net is connected: "+isConnected);
        if (CheckNetwork.isInternetAvailable(context))
        {
            data.clear();
            new MusicCategoryAsyncTask().execute();
        }
         else
        {
            Toast.makeText(context,"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
        layoutManager = new LinearLayoutManager(context);
        musicCatRecycler.setLayoutManager(layoutManager);
        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new MenuFragment(context);
                fm = ((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        return v;
    }

   //http://durisimomobileapps.net/djsisko/api/categories
   public class MusicCategoryAsyncTask extends AsyncTask<String, Void, String> {

       private JSONObject jsonObject;
       public MusicCategoryAsyncTask() {

       }

       @Override
       protected void onPreExecute() {
           super.onPreExecute();
           mProgressDialog = new ProgressDialog(context);
           mProgressDialog.setMessage("Loading");
           mProgressDialog.setCanceledOnTouchOutside(true);
           mProgressDialog.setIndeterminate(true);
           mProgressDialog.setCancelable(true);
           mProgressDialog.show();
       }

       @Override
       protected String doInBackground(String... params) {
           String result = null;
           try {
               MediaType JSON = MediaType.parse("application/json; charset=utf-8");
               String url = "http://durisimomobileapps.net/djsisko/api/categories";
               OkHttpClient client = new OkHttpClient();
//               RequestBody body = RequestBody.create(JSON, String.valueOf(jsonObject));
               Request request = new Request.Builder()
                       .url(url)
                       .build();
               Response response = client.newCall(request).execute();
               result = response.body().string();
               return result;
           } catch (Exception e) {
               return null;
           }
       }


       @Override
       protected void onPostExecute(String s) {
           super.onPostExecute(s);
           mProgressDialog.dismiss();
           Log.d("MusicCategory", "Response is:" + s);
           if (s == null) {
               Log.d("mytag", "Music Category Response is null");
           } else {
               try
               {
               JSONObject jsonObject = new JSONObject(s);
                   int status = jsonObject.getInt("status");
                   if(status == 0){
                       Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                   }
               JSONArray jsonArray = jsonObject.getJSONArray("data");
            if(jsonArray.length() == 0){
                Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
            }
               for (int i = 0; i < jsonArray.length(); i++) {
                   JSONObject categoryIdJson = jsonArray.getJSONObject(i);
                   int categoryId = categoryIdJson.getInt("category_id");
                   Log.d("Music Category", "Category Id is:" + categoryId);
                   JSONObject categoryNameJson = jsonArray.getJSONObject(i);
                   String categoryName = categoryNameJson.getString("category_name");
                   Log.d("Music Category", "Category Name is:" + categoryName);
                   JSONObject categoryImageJson = jsonArray.getJSONObject(i);
                   String categoryImage = categoryImageJson.getString("image");
                   Log.d("Music Category", "Category Image is:" + categoryImage);
                   data.add(new DataSisko(categoryId, categoryImage, categoryName));
               }
               try {
                   musicCategoryAdapter = new MusicCategoryAdapter(context, data,tag,radioPlayer);
                   musicCatRecycler.setAdapter(musicCategoryAdapter);
               } catch (Exception e) {
                   e.printStackTrace();
               }
           } catch(JSONException e){
               e.printStackTrace();
           }
       }

           System.out.println(s);
       }

   }
}
