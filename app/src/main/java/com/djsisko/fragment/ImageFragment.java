package com.djsisko.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.djsisko.activities.MainActivity;
import com.djsisko.R;
import com.djsisko.adapter.ImageDataAdapter;
import com.djsisko.data.Song;
import com.djsisko.phonemidea.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ImageFragment extends Fragment {


    ArrayList<Song> dataModules = new ArrayList<>();
    Context context;
    int id;
    String title;
    String androidId;
    boolean isConnected;
    SharedPreferences preferences;
    Fragment frag;
    FragmentTransaction ft;
    FragmentManager fm;
    RecyclerView imageRecycler;
    ProgressDialog mProgressDialog;
    ImageDataAdapter imageDataAdapter;
    TextView menuListHeading;
    String mainTag;
    ImageView backButton1,playerButton1;

    public ImageFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public ImageFragment(Context context, int id, String title, String mainTag)
    {
        this.context = context;
        this.id = id;
        this.title = title;
        this.mainTag = mainTag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_image, container, false);
        menuListHeading = v.findViewById(R.id.menuListHeading);
        backButton1 = v.findViewById(R.id.backButton1);
        playerButton1 = v.findViewById(R.id.playerButton1);
        preferences = context.getSharedPreferences("MyAndroidId",Context.MODE_PRIVATE);
        androidId = preferences.getString("androidId","");
        isConnected = preferences.getBoolean("isConnected",false);
        Log.d("ImageFragment","Android id: "+androidId);
        Log.d("ImageFragment","Net is connected: "+isConnected);
        imageRecycler = v.findViewById(R.id.imageRecycler);

        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frag = new Gallery(context);
                fm =((MainActivity)context).getSupportFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.container,frag);
                ft.addToBackStack(mainTag);
                ft.commit();
            }
        });
        playerButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.setPlayLayout();
            }
        });
        menuListHeading.setText(title);
        menuListHeading.setSelected(true);
        menuListHeading.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        menuListHeading.setSingleLine(true);
        JSONObject object = new JSONObject();
        try {
            object.put("folder_id",id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (CheckNetwork.isInternetAvailable(context))
        {
            dataModules.clear();
            new imageGalleryAsyncTask(object).execute();
        }
         else
        {
            Toast.makeText(context,"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
        return v;
    }


    //    http://durisimomobileapps.net/dreamteam/api/folders/images
    private class imageGalleryAsyncTask extends AsyncTask<String,Void,String> {

        JSONObject object;

        private imageGalleryAsyncTask(JSONObject object)
        {
            this.object = object;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }


        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            try
            {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = "http://durisimomobileapps.net/djsisko/api/folders/images";
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(object));
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch(
                    Exception e)

            {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
                Log.d("mytag", "Images inside gallery Response : " + s);
                if (s == null) {
                    Log.d("mytag", "Bookings Response is null");
                } else {
                    try {
                    JSONObject jsonObject = new JSONObject(s);
                        int status = jsonObject.getInt("status");
                        if(status == 0){
                            Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
            if(jsonArray.length() == 0){
                Toast.makeText(context, "No Data Found.", Toast.LENGTH_SHORT).show();
            }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject galleryIdJson = jsonArray.getJSONObject(i);
                        int galleryId = galleryIdJson.getInt("gallery_id");
                        Log.d("Gallery Id is", "" + galleryId);
                        JSONObject imageJson = jsonArray.getJSONObject(i);
                        String image = imageJson.getString("image");
                        Log.d("Image is:", image);
                        dataModules.add(new Song(galleryId, image));
//                    dataModules.add(new Song(1,"http:\\/\\/durisimomobileapps.net\\/djsisko\\/core\\/public\\/storage\\/uploads\\/img3_1513072333.png"));
                    }
                    try {
                        imageDataAdapter = new ImageDataAdapter(context, dataModules);
                        imageRecycler.setLayoutManager(new GridLayoutManager(context,3));
                        imageRecycler.setAdapter(imageDataAdapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch(JSONException e){
                    e.printStackTrace();
                }
            }
            System.out.println(s);
        }

    }

}
